class Features():
	def __init__(self):
		pass

	def workerId(self):
		return 1
	def workerPrice(self):
		return 10
	def workerHitpoints(self):
		return 10
	def workerMovement(self):
		return 2
	def workerAttack(self):
		return 3
	def workerDefence(self):
		return 0
	def workerRange(self):
		return 1
	def workerSpecial(self):
		return 1
	def workerCarry(self):
		return 10
	def workerGatheringSpeed(self):
		return 2
	def workerBuilderSpeed(self):
		return 20
	def workerRepairerSpeed(self):
		return 10
	def workerChoppingSpeed(self):
		return 3
	def workerTrainingSpeed(self):
		return 0.2

	def swordmanId(self):
		return 2
	def swordmanPrice(self):
		return 15
	def swordmanHitpoints(self):
		return 30
	def swordmanMovement(self):
		return 2
	def swordmanAttack(self):
		return 5
	def swordmanDefence(self):
		return 1
	def swordmanRange(self):
		return 1
	def swordmanSpecial(self):
		return 2
	def swordmanTrainingSpeed(self):
		return 0.25
	def swordmanABvsN(self):
		return 4

	def eliteswordmanId(self):
		return 3
	def eliteswordmanPrice(self):
		return 30
	def eliteswordmanHitpoints(self):
		return 50
	def eliteswordmanMovement(self):
		return 3
	def eliteswordmanAttack(self):
		return 7
	def eliteswordmanDefence(self):
		return 2
	def eliteswordmanRange(self):
		return 1
	def eliteswordmanSpecial(self):
		return 3
	def eliteswordmanTrainingSpeed(self):
		return 0.18
	def eliteswordmanABvsN(self):
		return 5

	def archerId(self):
		return 4
	def archerPrice(self):
		return 20
	def archerHitpoints(self):
		return 20
	def archerMovement(self):
		return 2
	def archerAttack(self):
		return 3
	def archerDefence(self):
		return 0
	def archerRange(self):
		return 4
	def archerSpecial(self):
		return 4
	def archerABr3(self):
		return 1
	def archerABr2(self):
		return 2
	def archerABslow(self):
		return 2
	def archerAvsB(self):
		return 1
	def archerTrainingSpeed(self):
		return 0.18

	def knightId(self):
		return 5
	def knightPrice(self):
		return 50
	def knightHitpoints(self):
		return 70
	def knightMovement(self):
		return 4
	def knightAttack(self):
		return 7
	def knightDefence(self):
		return 2
	def knightRange(self):
		return 1
	def knightSpecial(self):
		return 5
	def knightTrainingSpeed(self):
		return 0.1

	def catapultId(self):
		return 6
	def catapultPrice(self):
		return 50
	def catapultHitpoints(self):
		return 20
	def catapultMovement(self):
		return 1
	def catapultAttack(self):
		return 20
	def catapultDefence(self):
		return 1
	def catapultRange(self):
		return 5 #(2 -) 5
	def catapultSpecial(self):
		return 6
	def catapultTrainingSpeed(self):
		return 0.1
	def catapultAoE(self):
		return 0.25

	def HQId(self):
		return 7
	def HQPrice(self):
		return 75
	def HQHitpoints(self):
		return 100
	def HQDefence(self):
		return 3
	def HQSpecial(self):
		return 7

	def resourcecampId(self):
		return 8
	def resourcecampPrice(self):
		return 30
	def resourcecampHitpoints(self):
		return 50
	def resourcecampDefence(self):
		return 1
	def resourcecampSpecial(self):
		return 8

	def barracksId(self):
		return 9
	def barracksPrice(self):
		return 50
	def barracksHitpoints(self):
		return 75
	def barracksDefence(self):
		return 1
	def barracksSpecial(self):
		return 9

	def archeryrangeId(self):
		return 10
	def archeryrangePrice(self):
		return 50
	def archeryrangeHitpoints(self):
		return 75
	def archeryrangeDefence(self):
		return 1
	def archeryrangeSpecial(self):
		return 10

	def stableId(self):
		return 11
	def stablePrice(self):
		return 50
	def stableHitpoints(self):
		return 75
	def stableDefence(self):
		return 1
	def stableSpecial(self):
		return 11

	def siegeworkshopId(self):
		return 17
	def siegeworkshopPrice(self):
		return 75
	def siegeworkshopHitpoints(self):
		return 75
	def siegeworkshopDefence(self):
		return 1
	def siegeworkshopSpecial(self):
		return 17

	def watchtowerId(self):
		return 13
	def watchtowerPrice(self):
		return 50
	def watchtowerHitpoints(self):
		return 100
	def watchtowerAttack(self):
		return 3
	def watchtowerDefence(self):
		return 3
	def watchtowerRange(self):
		return 4
	def watchtowerSpecial(self):
		return 12

	def castleId(self):
		return 14
	def castlePrice(self):
		return 120
	def castleHitpoints(self):
		return 200
	def castleAttack(self):
		return 7
	def castleDefence(self):
		return 3
	def castleRange(self):
		return 4
	def castleSpecial(self):
		return 13

	def resourceplantId(self):
		return 14
	def resourceplantContain(self):
		return 300

	def hugeresourceplantId(self):
		return 15
	def hugeresourceplantContain(self):
		return 500

	def blockId(self):
		return 16
	def blockHitpoints(self):
		return 10
	def blockFallingDamage(self):
		return 10

	def cheatScroogeMcDuckAmount(self):
		return 1000

	'''
	def Price(self):
		return
	def Hitpoints(self):
		return
	def Movement(self):
		return
	def Attack(self):
		return
	def Defence(self):
		return
	def Range(self):
		return
	def Special(self):
		return
	'''
