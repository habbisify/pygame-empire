from random import random,shuffle

from commands import Commands #for Commands().getUnitsList()
from messages import *
from pygameactivity import *

class PathGenerator2():
	def __init__(self):
		pass
	def pathfinding(self, A,C,M,D,ghost_mode):
		'''Tama funktio selvittaa lyhimman reitin aloituspisteen A ja lopetuspisteen C valilla kartalla M. Aloituspisteen ja lopetuspisteen on oltava listoja
		   muotoa
		   
		   A,C = [x,y],
		   
		   ja niiden on sijaittava annetulla kartalla. Kartan on oltava kaksidimensionaalinen ja suorakulmainen muotoa
		   
		   M = [y1=[x1,...,xn],y2,...,yn=[x1,...,xn]].
		   
		   Kartalla 0-alkio merkkaa ruutua, johon voi liikkua; jos ruudun arvo on jotain muuta, funktio toteaa sen esteeksi. Reitin loydyttya funktio palauttaa
		   reitin muodossa
		   
		   [[[x1,y1],[x2,y2],...,[x(n-1),y(n-1)],[xn,yn]],
		   missa [x1,y1]=A, [x2,y2],...,[x(n-1),y(n-1)] ovat reitin pisteet valilla A - C jarjestyksessa ja [xn,yn]=C.
		   
		   Mikali reittia ei loydy syvyydella D (esim. jos lopetuspiste-alkio ei ole 0), funktio palauttaa arvon -1. Jos aloituspiste on sama kuin lopetuspiste, funktio
		   palauttaa arvon -2.
		   
		   ghost_mode sallii reitin lavitse liikkuvista objekteista UNITS
		'''
		
		'''1. Maariteellaan aloituspiste A=[x,y], mielivaltainen piste B=[x,y] ja lopetuspiste C=[x,y] kartalla M = [y1=[x1,...,xn],...,yn=[x1,...,xn]].
		      Maaritellaan oikea reitti P0=-1, tarkisteltavat reitit paths = [] ja lukitut ruudut L = [].
		'''
		P0 = -1
		paths = []
		L = []
		UNITS = Commands().getUnitsList()
		N1 = [[1,0],[0,-1],[-1,0],[-1,0],[0,1],[0,1],[1,0],[1,0]]
		
		'''1...Jos A=C -> P0=-2, return P0
		'''
		if A == C:
			P0 = -2
			return P0
		
		'''2. Luodaan nelja reittia P1, P2, P3, P4. Jokainen saa pisteen A arvon ensimmaiseksi alkiokseen. P1:lle luodaan toinen alkio B1, B1=A+[1,0].
		      P2 <- B2, B2=A+[0,1]. P3 <- B3, B3=A+[-1,0]. P4 <- B4, B4=A+[0,-1]. Luodaan siis reitti jokaista siirtymaruutua kohden, kun voidaan liikkua
			  pysty- ja vaakasuoraan.
		'''
		B1 = [1,0]
		paths.append([[A[0],A[1]],[A[0]+B1[0],A[1]+B1[1]]])
		B2 = [0,1]
		paths.append([[A[0],A[1]],[A[0]+B2[0],A[1]+B2[1]]])
		B3 = [-1,0]
		paths.append([[A[0],A[1]],[A[0]+B3[0],A[1]+B3[1]]])
		B4 = [0,-1]
		paths.append([[A[0],A[1]],[A[0]+B4[0],A[1]+B4[1]]])
		#print "2. paths",paths
		
		i=0
		for path in paths:
			for coordinates in path:
				if not 0 <= coordinates[0] < len(M[0]) or not 0 <= coordinates[1] < len(M):
					paths[i] = "trash"
			i+=1
		
		while paths.count("trash") > 0: #ilman trash-ratkaisua .pop(i) ja .remove(x) poistaisivat vaaria listoja
			paths.remove("trash")
		
		'''3. while n<k (k = raja, kuinka monta pathia kasitellaan maksimissaan):
		'''
		n=0
		while n<=D: #D = kuinka monta pathia kasitellaan maksimissaan
			paths_new = list(paths)
			i=0
			for path in paths:
				#print "current path",path
				
				'''3.2. Tarkistetaan P, onko viimeisen alkion arvo kartalla nolla, (M(B(n)))=0. Jos ei ole, P tormaa esteeseen ja tuhoutuu.
				        Jos etsitaan ghost_modella, niin P ei tormaa liikkuviin objekteihin.
				'''
				if ghost_mode:
					if M[path[-1][1]][path[-1][0]] != 0:
						if abs(M[path[-1][1]][path[-1][0]][0]) not in UNITS:
							paths_new[i] = "trash"
				else:		
					if M[path[-1][1]][path[-1][0]] != 0:
						paths_new[i] = "trash"
						#print "3.2. trash",paths_new
				
				'''3.3. Tarkistetaan P, onko viimeinen alkio lukittu, (B(n) in L)!=0. Jos on, P tormaa optimaalisempaan reittiin ja tuhoutuu.
				'''
				if L.count(path[-1]) != 0:
					paths_new[i] = "trash"
					#print "3.3 trash",paths_new
				
				'''3.4. Tarkistetaan P, onko viimeisella alkiolla B(n) olemassa duplikaattia, (B(n) in [any]P)>=2. Jos on, arvo niista yksi.
						Tuhoa muut P kuin valittu. Duplikaatit ovat keskenaan yhta optimaalisia.
				'''
				duplicate_list = []
				a=0
				for j in paths_new:
					if path[-1] == j[-1]:
						duplicate_list.append(j)
						a+=1
						
				shuffle(duplicate_list)
				
				while a >= 2:
					index = paths_new.index(duplicate_list[a-1])
					paths_new.remove(duplicate_list[a-1])
					paths_new.insert(index, "trash")
					a-=1
					#print "3.4. trash",paths_new
				
				i+=1
				
			while paths_new.count("trash") > 0: #ilman trash-ratkaisua .pop(i) ja .remove(x) poistaisivat vaaria listoja
				paths_new.remove("trash")
			
			#print "paths after trashing",paths_new
			
			'''3.1. Tarkistetaan P, onko viimeinen alkio B(n)=C. Jos on, kyseinen P=P(min) -> P=P0, return P0
			'''
			for path in paths_new:
				if path[-1] == C:
					P0 = path
					return P0
			
			'''3.5. Merkitaan kaikkien P kayttamat B lukituiksi, L <- [all]P(B).
			'''
			for path in paths_new:
				for i in path:
					if L.count(i) == 0:
						L.append(i)
			#print "3.5. locked squares",L
			
			'''3.6. Luodaan jokaista P kohti nelja uutta reittia P(n+1), P(n+2), P(n+3), P(n+4), missa n= 4*(taman kohdan toistojen maara + 1) Jokaiselle
			        uudelle pathille annetaan vanhan alkiot,
					P(n+k) <- P(n).
			   3.7. P(n+1):lle luodaan uusi alkio B(n+1), B(n+1)=B(n)+[1,0]. P(n+2) <- B(n+2), B(n+2)=B(n)+[0,1]. P(n+3) <- B(n+3), B(n+3)=B(n)+[-1,0].
					P(n+4) <- B(n+4), B(n+4)=B(n)+[0,-1].
			'''
			paths_newest = []
			for path in paths_new:
				paths_newer = []
				i = 0
				while i < 4:
					paths_newer.append(list(path))
					i += 1
				
				paths_newer[0].append(list([path[-1][0]+B1[0],path[-1][1]+B1[1]]))
				paths_newer[1].append(list([path[-1][0]+B2[0],path[-1][1]+B2[1]]))
				paths_newer[2].append(list([path[-1][0]+B3[0],path[-1][1]+B3[1]]))
				paths_newer[3].append(list([path[-1][0]+B4[0],path[-1][1]+B4[1]]))
				#print paths_newer
				
				for path_newer in paths_newer:
					paths_newest.append(list(path_newer))
				
			#print "newest paths:",paths_newest
			
			i=0
			for path in paths_newest:
				if path[-1][0] < 0 or path[-1][1] < 0 or path[-1][0] > 18 or path[-1][1] > 9:
					paths_newest[i] = "trash"
				i+=1
			
			while paths_newest.count("trash") > 0: #ilman trash-ratkaisua .pop(i) ja .remove(x) poistaisivat vaaria listoja
				paths_newest.remove("trash")
			
			paths = list(paths_newest) #merkitaan valmistunut edellista yhden kerroksen syvempi uusi reittien lista paths-listaksi
			#print paths
			n+=1
		return P0
		
	def findPath(self, movement_list, position):
		N1 = [[1,0],[0,-1],[-1,0],[-1,0],[0,1],[0,1],[1,0],[1,0]]														#C:n variaatio edellisesta tutkinnasta
		N2 = [[1,0],[0,-1],[0,-1],[0,-1],[-1,0],[-1,0],[-1,0],[-1,0],[0,1],[0,1],[0,1],[0,1],[1,0],[1,0],[1,0],[1,0]]	#
		
		A = list(movement_list[1])
		C = list(movement_list[2])
		M = position
		D = 200 #depth of search
		
		P0 = -1
		
		
		
		if M[C[1]][C[0]] == 0:
			P0 = self.pathfinding(A,C,M,D,False)
		
		if P0 != -1:
			return P0
		
		N1_distances = [[[0,0],1000]] #node = [[C(x),C(y)], distance from unit to the node]
		N2_distances = [[[0,0],1000]]
		Ns_distances = [N1_distances, N2_distances]
		
		for n in N1:
			C[0]+=n[0]
			C[1]+=n[1]
			if 0 <= C[0] < len(position[0]) and 0 <= C[1] < len(position):
				if M[C[1]][C[0]] == 0 or A==C:
					A_C_length = int(abs(A[0]-C[0])+abs(A[1]-C[1]))
					a=0
					for i in N1_distances:
						if A_C_length < i[1]:
							N1_distances.insert(int(a), [list(C), int(A_C_length)])
							break
						a+=1
		
		N2_check = False
		j=0
		while P0 == -1:

			if Ns_distances[j][0][1] != 1000:
				
				Ns_closest_node = Ns_distances[j].pop(0)
				C = Ns_closest_node[0]
				P0 = self.pathfinding(A,C,M,D,False)
				if P0 != -1:
					return P0
			
			elif (movement_list[0] == 1 or movement_list[0] == 5) and not N2_check:
				N2_check = True
				j+=1
				
				C = list(movement_list[2])
				C[0]+=1
				C[1]+=1
				for n in N2:
					C[0]+=n[0]
					C[1]+=n[1]
					if 0 <= C[0] < len(position[0]) and 0 <= C[1] < len(position):
						if M[C[1]][C[0]] == 0 or A==C:
							A_C_length = int(abs(A[0]-C[0])+abs(A[1]-C[1]))
							a=0
							for i in N2_distances:
								if A_C_length < i[1]:
									N2_distances.insert(int(a), [list(C), int(A_C_length)])
									break
								a+=1
			
			else:
				break
		
		
		
		#ghost_mode
		Pg = -1
		C = list(movement_list[2])
		
		N1_distances = [[[0,0],1000]] #node = [[C(x),C(y)], distance from unit to the node]
		N2_distances = [[[0,0],1000]]
		Ns_distances = [N1_distances, N2_distances]
		
		for n in N1:
			C[0]+=n[0]
			C[1]+=n[1]
			if 0 <= C[0] < len(position[0]) and 0 <= C[1] < len(position):
				if M[C[1]][C[0]] == 0 or A==C:
					A_C_length = int(abs(A[0]-C[0])+abs(A[1]-C[1]))
					a=0
					for i in N1_distances:
						if A_C_length < i[1]:
							N1_distances.insert(int(a), [list(C), int(A_C_length)])
							break
						a+=1
				elif M[C[1]][C[0]] != 0:
					if abs(M[C[1]][C[0]][0]) in [1,2,3,4,5,6]:
						A_C_length = int(abs(A[0]-C[0])+abs(A[1]-C[1]))
						a=0
						for i in N1_distances:
							if A_C_length < i[1]:
								N1_distances.insert(int(a), [list(C), int(A_C_length)])
								break
							a+=1
		
		N2_check = False
		j=0
		while Pg == -1:

			if Ns_distances[j][0][1] != 1000:
				
				Ns_closest_node = Ns_distances[j].pop(0)
				C = Ns_closest_node[0]
				Pg = self.pathfinding(A,C,M,D,True)
				
				if Pg != -1:
					P0_temp = []
					i=0
					for step in Pg:
						if (i == 0 or position[step[1]][step[0]] == 0):
							P0_temp.append(list(step))
						else:
							break
						i+=1
					P0 = list(P0_temp)
					return P0
				
			elif (movement_list[0] == 1 or movement_list[0] == 5) and not N2_check:
				N2_check = True
				j+=1
				
				C = list(movement_list[2])
				C[0]+=1
				C[1]+=1
				for n in N2:
					C[0]+=n[0]
					C[1]+=n[1]
					if 0 <= C[0] < len(position[0]) and 0 <= C[1] < len(position):
						if M[C[1]][C[0]] == 0 or A==C:
							A_C_length = int(abs(A[0]-C[0])+abs(A[1]-C[1]))
							a=0
							for i in N2_distances:
								if A_C_length < i[1]:
									N2_distances.insert(int(a), [list(C), int(A_C_length)])
									break
								a+=1
						elif M[C[1]][C[0]] != 0:
							if M[C[1]][C[0]][0] in [1,2,3,4,5,6]:
								A_C_length = int(abs(A[0]-C[0])+abs(A[1]-C[1]))
								a=0
								for i in N2_distances:
									if A_C_length < i[1]:
										N2_distances.insert(int(a), [list(C), int(A_C_length)])
										break
									a+=1
			
			else:
				break
		
		
		
		return P0
	
	def createMovement(self, screen, pre_coordinates, new_coordinates, position):
		
		unit = position[pre_coordinates[1]].pop(pre_coordinates[0])
		unit[1] = [new_coordinates[0],new_coordinates[1]]
		
		PygAct().drawAction(screen,1,unit,pre_coordinates,[0],new_coordinates)
		
		position[pre_coordinates[1]].insert(pre_coordinates[0], 0)
		position[new_coordinates[1]][new_coordinates[0]] = unit
		
		#Toisen yksikon jahtaaminen
		for line in position:
			for node in line:
				if node != 0:
					if abs(node[0]) in [1,2,3,4,5,6]:
						if node[2] != 0:
							if node[2][0] == 5 and node[2][1] == pre_coordinates:
								node[2][1] = list(new_coordinates)
		
		return position
	
	def movementModule(self, screen, movement_list, position):
		event_list = []
		event = ""
		
		#Toisen yksikon jahtaaminen
		if position[movement_list[1][1]][movement_list[1][0]] != 0:
			if abs(position[movement_list[1][1]][movement_list[1][0]][0]) in [1,2,3,4,5,6,7,8,9,10,11,12,13,17]:
				if position[movement_list[1][1]][movement_list[1][0]][2] != 0:
					movement_list[2] = list(position[movement_list[1][1]][movement_list[1][0]][2][1])
				else:
					return "trash", event, position
		
		path = self.findPath(movement_list, position)
		
		if type(path) is list:
			
			m = position[path[0][1]][path[0][0]][5] #movement
			
			#Rangedit, jotka jahtaavat kohdetta ja ovat ampumaetaisyydella eivat lahesty kohdetta
			i=0
			while i<=m:
				if i < len(path)-1:
					if position[movement_list[1][1]][movement_list[1][0]][2] != 0:
						if position[movement_list[1][1]][movement_list[1][0]][2][0] == 5 and abs(path[i][0]-movement_list[2][0])+abs(path[i][1]-movement_list[2][1]) <= position[movement_list[1][1]][movement_list[1][0]][8]:
							m = int(i)
							break
				else:
					break
				i+=1
			
			if m < len(path)-1:
				
				new_coordinates = path[m]
				movement_list = [movement_list[0],new_coordinates,movement_list[2]]
				
				#unit = Messages().numberToUnit(position[path[0][1]][path[0][0]][0])
				#event = Messages().createMovementInProgressEvent(movement_list,path,new_coordinates,unit)
				#Messages().printMovementEvent(event)

			else:
				
				new_coordinates = path[-1]
				
				#unit = Messages().numberToUnit(position[path[0][1]][path[0][0]][0])
				#event = Messages().createMovementCompleteEvent(movement_list,path,new_coordinates,unit)
				#Messages().printMovementEvent(event)
				
				if new_coordinates == movement_list[2] or position[movement_list[1][1]][movement_list[1][0]][2][0] in [2,3,4,6]:
					if new_coordinates == movement_list[2] and position[movement_list[1][1]][movement_list[1][0]][2][0] == 1:
						position[movement_list[1][1]][movement_list[1][0]][2] = 0
					movement_list = "trash"
				else:
					movement_list = [movement_list[0],new_coordinates,movement_list[2]]
			
			'''Tekee itse mekaanisen aseman muutoksen kartalla ja delegoi animoimisen.
			'''
			position = self.createMovement(screen,path[0],new_coordinates, position)
		
		else:
			if path == -1:
				print "Couldn't find a path with the given depth."
			elif path == -2:
				if movement_list[1] == movement_list[2] or position[movement_list[1][1]][movement_list[1][0]][2][0] in [2,3,4,6]:
					if movement_list[1] == movement_list[2] and position[movement_list[1][1]][movement_list[1][0]][2][0] == 1:
						position[movement_list[1][1]][movement_list[1][0]][2] = 0
					movement_list = "trash"
					print "Unit had already reached it's destination."
			else:
				print "Something went wrong :("
				movement_list = "trash"
		
		return movement_list, event, position
	
	def runPaths(self, screen, all_movement_list, position):
		all_list_movement = []
		event_list = []
		
		for movement_list in all_movement_list:
			if type(position[movement_list[1][1]][movement_list[1][0]]) is list:
				list_movement, event, position = self.movementModule(screen, movement_list, position)
			
				all_list_movement.append(list_movement) #just movement_list upside down to maintain the functionality
				event_list.append(event)

		
		while all_list_movement.count("trash") > 0:
			all_list_movement.remove("trash")

		all_movement_list = list(all_list_movement)
		return all_movement_list, event_list, position