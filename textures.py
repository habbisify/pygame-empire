from messages import *
from audio import playSound

class Textures:
	def __init__(self):
		pass
	def printLogo2(self):
		print "                                                 _,..---''-.,"
		print "                                              ,-`,-.         `,"
		print "                                             /  / _/           \\"
		print "                                            /___//_____,--.     ,"
		print "                                           /     __,..--'||     |"
		print "                                           `T`7 //,-',  //      |"
		print "    ______                _                 )/_//  `'  //       |"
		print "   / ____/___ ___  ____  (_)_______         |`-`      <<,       /"
		print "  / ___// __ `__ \/ __ \/ / ___/ _ \        \\ _        `,\\     /"
		print " / /___/ / / / / / /_/ / / /  /  __/         |)`',       \\\\___/"
		print "/_____/_/ /_/ /_/ /__ /_/_/  / ___/          \\`~~       , `--'"
		print "               /_/                            |     ,.-'     |"
		print "                                              `--,`         \\  _"
		print "   Made by Joni Seppala                   ,-'`)T(            >` `--..,"
		print "                                        ,'`   //\\_\\        ,/`         `-,"
		print "                                       (  ___/   /`-....--<               `,"
		print "                                        /`  /\\__/\\__,/     >._              )"
		print "                                       /   |__/\\__/  \\____/\\  `-,-.____,.--'\\"
		print ""
	def printPosition(self, position, name1, name2, resources1, resources2, points1, points2):
		k = -1
		processed_position = []
		for i in position:
			k += 1
			processed_position.append([])
			for j in i:
				c = self.intToChar(j)
				processed_position[k].append(c)
		self.printPosition2(processed_position, name1, name2, resources1, resources2, points1, points2)
	def intToChar(self, i):
		c = ""
		if i == 0:
			c = " "
		elif type(i) is list:
			if i[0] == 1:
				c = "W"
			elif i[0] == -1:
				c = "w"
			elif i[0] == 2:
				c = "S"
			elif i[0] == -2:
				c = "s"
			elif i[0] == 3:
				c = "E"
			elif i[0] == -3:
				c = "e"
			elif i[0] == 4:
				c = "A"
			elif i[0] == -4:
				c = "a"
			elif i[0] == 5:
				c = "K"
			elif i[0] == -5:
				c = "k"
			elif i[0] == 6:
				c = "C"
			elif i[0] == -6:
				c = "c"
			elif i[0] == 7:
				c = "H"
			elif i[0] == -7:
				c = "h"
			elif i[0] == 8:
				c = "^"
			elif i[0] == -8:
				c = "*"
			elif i[0] == 9:
				c = "B"
			elif i[0] == -9:
				c = "b"
			elif i[0] == 10:
				c = "@"
			elif i[0] == -10:
				c = "%"
			elif i[0] == 11:
				c = "S"
			elif i[0] == -11:
				c = "s"
			elif i[0] == 12:
				c = "T"
			elif i[0] == -12:
				c = "t"
			elif i[0] == 13:
				c = "C"
			elif i[0] == -13:
				c = "c"
			elif i[0] == 14:
				c = "r"
			elif i[0] == 15:
				c = "R"
			elif i[0] == 17:
				c = "I"
			elif i[0] == -17:
				c = "i"
			elif i[0] == 16:
				c = "#"
		return c
	def printPosition2(self, position, name1, name2, resources1, resources2, points1, points2):
		print "  -----------------------------------------------------------------------------"
		print "j | " + position[0][0] + " | " + position[0][1] + " | " + position[0][2] + " | " + position[0][3] + " | " + position[0][4] + " | " + position[0][5] + " | " + position[0][6] + " | " + position[0][7] + " | " + position[0][8] + " | " + position[0][9] + " | " + position[0][10] + " | " + position[0][11] + " | " + position[0][12] + " | " + position[0][13] + " | " + position[0][14] + " | " + position[0][15] + " | " + position[0][16] + " | " + position[0][17] + " | " + position[0][18] + " |"
		print "  -----------------------------------------------------------------------------"
		print "i | " + position[1][0] + " | " + position[1][1] + " | " + position[1][2] + " | " + position[1][3] + " | " + position[1][4] + " | " + position[1][5] + " | " + position[1][6] + " | " + position[1][7] + " | " + position[1][8] + " | " + position[1][9] + " | " + position[1][10] + " | " + position[1][11] + " | " + position[1][12] + " | " + position[1][13] + " | " + position[1][14] + " | " + position[1][15] + " | " + position[1][16] + " | " + position[1][17] + " | " + position[1][18] + " |"
		print "  -----------------------------------------------------------------------------"
		print "h | " + position[2][0] + " | " + position[2][1] + " | " + position[2][2] + " | " + position[2][3] + " | " + position[2][4] + " | " + position[2][5] + " | " + position[2][6] + " | " + position[2][7] + " | " + position[2][8] + " | " + position[2][9] + " | " + position[2][10] + " | " + position[2][11] + " | " + position[2][12] + " | " + position[2][13] + " | " + position[2][14] + " | " + position[2][15] + " | " + position[2][16] + " | " + position[2][17] + " | " + position[2][18] + " |"
		print "  -----------------------------------------------------------------------------"
		print "g | " + position[3][0] + " | " + position[3][1] + " | " + position[3][2] + " | " + position[3][3] + " | " + position[3][4] + " | " + position[3][5] + " | " + position[3][6] + " | " + position[3][7] + " | " + position[3][8] + " | " + position[3][9] + " | " + position[3][10] + " | " + position[3][11] + " | " + position[3][12] + " | " + position[3][13] + " | " + position[3][14] + " | " + position[3][15] + " | " + position[3][16] + " | " + position[3][17] + " | " + position[3][18] + " |"
		print "  -----------------------------------------------------------------------------"
		print "f | " + position[4][0] + " | " + position[4][1] + " | " + position[4][2] + " | " + position[4][3] + " | " + position[4][4] + " | " + position[4][5] + " | " + position[4][6] + " | " + position[4][7] + " | " + position[4][8] + " | " + position[4][9] + " | " + position[4][10] + " | " + position[4][11] + " | " + position[4][12] + " | " + position[4][13] + " | " + position[4][14] + " | " + position[4][15] + " | " + position[4][16] + " | " + position[4][17] + " | " + position[4][18] + " |"
		print "  -----------------------------------------------------------------------------"
		print "e | " + position[5][0] + " | " + position[5][1] + " | " + position[5][2] + " | " + position[5][3] + " | " + position[5][4] + " | " + position[5][5] + " | " + position[5][6] + " | " + position[5][7] + " | " + position[5][8] + " | " + position[5][9] + " | " + position[5][10] + " | " + position[5][11] + " | " + position[5][12] + " | " + position[5][13] + " | " + position[5][14] + " | " + position[5][15] + " | " + position[5][16] + " | " + position[5][17] + " | " + position[5][18] + " |"
		print "  -----------------------------------------------------------------------------"
		print "d | " + position[6][0] + " | " + position[6][1] + " | " + position[6][2] + " | " + position[6][3] + " | " + position[6][4] + " | " + position[6][5] + " | " + position[6][6] + " | " + position[6][7] + " | " + position[6][8] + " | " + position[6][9] + " | " + position[6][10] + " | " + position[6][11] + " | " + position[6][12] + " | " + position[6][13] + " | " + position[6][14] + " | " + position[6][15] + " | " + position[6][16] + " | " + position[6][17] + " | " + position[6][18] + " |"
		print "  -----------------------------------------------------------------------------"
		print "c | " + position[7][0] + " | " + position[7][1] + " | " + position[7][2] + " | " + position[7][3] + " | " + position[7][4] + " | " + position[7][5] + " | " + position[7][6] + " | " + position[7][7] + " | " + position[7][8] + " | " + position[7][9] + " | " + position[7][10] + " | " + position[7][11] + " | " + position[7][12] + " | " + position[7][13] + " | " + position[7][14] + " | " + position[7][15] + " | " + position[7][16] + " | " + position[7][17] + " | " + position[7][18] + " |"
		print "  -----------------------------------------------------------------------------"
		print "b | " + position[8][0] + " | " + position[8][1] + " | " + position[8][2] + " | " + position[8][3] + " | " + position[8][4] + " | " + position[8][5] + " | " + position[8][6] + " | " + position[8][7] + " | " + position[8][8] + " | " + position[8][9] + " | " + position[8][10] + " | " + position[8][11] + " | " + position[8][12] + " | " + position[8][13] + " | " + position[8][14] + " | " + position[8][15] + " | " + position[8][16] + " | " + position[8][17] + " | " + position[8][18] + " |"
		print "  -----------------------------------------------------------------------------"
		print "a | " + position[9][0] + " | " + position[9][1] + " | " + position[9][2] + " | " + position[9][3] + " | " + position[9][4] + " | " + position[9][5] + " | " + position[9][6] + " | " + position[9][7] + " | " + position[9][8] + " | " + position[9][9] + " | " + position[9][10] + " | " + position[9][11] + " | " + position[9][12] + " | " + position[9][13] + " | " + position[9][14] + " | " + position[9][15] + " | " + position[9][16] + " | " + position[9][17] + " | " + position[9][18] + " |"
		print "  -----------------------------------------------------------------------------"
		print "    1   2   3   4   5   6   7   8   9   10  11  12  13  14  15  16  17  18  19"
		space = (54-(len(name1) + len(str(resources1)) + len(str(points1)) + len(name2) + len(str(resources2)) + len(str(points2))))*" "
		print name1, " R: ", str(resources1), " P: ", str(points1), space, name2, " R: ", str(resources2), " P: ", str(points2)
	
	def questionUnit(self, unit, player1_name, player2_name):
		if unit == 0:
			print "\n\tBlank square\n"
			print "             _"
			print "           (`  ).                   _"
			print "          (     ).              .:(`  )`.     You may occupy this square by"
			print ")       _(       '`.          :(   .    )     placing a unit on it or"
			print "     .=(`(      .   )     .--  `.  (    ) )   constructing a building on it."
			print "    ((    (..__.:'-'   .+(   )   ` _`  ) )"
			print "`.     `(       ) )       (   .  )     (   )  ._"
			print "  )      ` __.:'   )     (   (   ))     `-'.-(`  )"
			print ")  )  ( )       --'       `- __.'         :(      ))"
			print ".-'  (_.'          .')                    `(    )  ))"
			print "                 (_  )                     ` __.:'"
			print ""
			print "--..,___.--,--'`,---..-.--+--.,,-,,..._.--..-._.------."
			print ""
		elif type(unit) is list:
			if abs(unit[0]) == 1:
				playSound(0)
				print "\n\tWorker\n"
				print"          ___"
				print"        .\"   \"."
				print"        |     )"
				print"        ).' -("
				print"         )  _/"
				print"       .'_`( "
				print"      / ( ,/;"
				print"     /   \\ ) \\\\."
				print"    /'-./ \\ '.\\\\)"
				print"    \\   \\  '---;\\"
				print"    |`\\  \\      \\\\"
				print"   / / \\  \\      \\\\"
				print" _/ /   / /      _\\\\/"
				print"( \\/   /_/       \   |"
				print" \\_)  (___)       '._/"
				print "Side: " + self.returnPlayerName(unit[0], player1_name, player2_name)
				print "Location: " + Messages().coordinatesToReadable(unit[1])
				print "Current order: " + Messages().order(unit[2])
				print ""
				print "Price: " + str(unit[3])
				print "Hitpoints: " + str(unit[4][0]) + "/" + str(unit[4][1])
				print "Movement: " + str(unit[5])
				print "Attack: " + str(unit[6])
				print "Defence: " + str(unit[7])
				print "Range: " + str(unit[8])
				print "Special: " + Messages().special(unit[9])
				print "Carry: " + str(unit[10])
			elif abs(unit[0]) == 2:
				playSound(1)
				print "\n\tSwordman\n"
				print "|\             //"
				print " \\\\           _!_"
				print "  \\\\         /___\\"
				print "   \\\\        [+++]"
				print "    \\\\    _ _\^^^/_ _"
				print "     \\\\/ (    '-'  ( )"
				print "     /( \/ | <&>   /\ \\"
				print "    (* \  / \     /  > )"
				print "        \"`   >::::\ / /"
				print "            /:::' |/_)"
				print "           /  /|  |"
				print "          (  / (  /"
				print "          / /   \ \\"
				print "         / /     \ \\"
				print "       /___|    /___|"
				print ""
				print "Side: " + self.returnPlayerName(unit[0], player1_name, player2_name)
				print "Location: " + Messages().coordinatesToReadable(unit[1])
				print "Current order: " + Messages().order(unit[2])
				print ""
				print "Price: " + str(unit[3])
				print "Hitpoints: " + str(unit[4][0]) + "/" + str(unit[4][1])
				print "Movement: " + str(unit[5])
				print "Attack: " + str(unit[6])
				print "Defence: " + str(unit[7])
				print "Range: " + str(unit[8])
				print "Special: " + Messages().special(unit[9])
			elif abs(unit[0]) == 3:
				playSound(3)
				print "\n\tElite swordman\n"
				print"                   {}"
				print"                  .--."
				print"                 /.--.\\"
				print"                 |====|"
				print"                 |`::`|"
				print"             .-;`\..../`;_.-^-._"
				print"      /\\   /  |...::..|`   :   `|"
				print"      |:'\ |   /'''::''|   .:.   |"
				print"     @|\ /\;-,/\   ::  |..:::::..|"
				print"     `||\ <` >  >._::_.| ':::::' |"
				print"      || `""`  /   ^^  |   ':'   |"
				print"      ||       |       \    :    /"
				print"      ||       |        \   :   /"
				print"      ||       |___/\___|`-.:.-`"
				print"      ||        \_ || _/    `"
				print"      ||        <_ >< _>"
				print"      ||        |  ||  |"
				print"      ||        |  ||  |"
				print"      ||       _\.:||:./_"
				print"      \/      /____/\____\\"
				print""
				print "Side: " + self.returnPlayerName(unit[0], player1_name, player2_name)
				print "Location: " + Messages().coordinatesToReadable(unit[1])
				print "Current order: " + Messages().order(unit[2])
				print ""
				print "Price: " + str(unit[3])
				print "Hitpoints: " + str(unit[4][0]) + "/" + str(unit[4][1])
				print "Movement: " + str(unit[5])
				print "Attack: " + str(unit[6])
				print "Defence: " + str(unit[7])
				print "Range: " + str(unit[8])
				print "Special: " + Messages().special(unit[9])
			elif abs(unit[0]) == 4:
				playSound(1)
				print "\n\tArcher\n"
				print "Side: " + self.returnPlayerName(unit[0], player1_name, player2_name)
				print "Location: " + Messages().coordinatesToReadable(unit[1])
				print "Current order: " + Messages().order(unit[2])
				print ""
				print "Price: " + str(unit[3])
				print "Hitpoints: " + str(unit[4][0]) + "/" + str(unit[4][1])
				print "Movement: " + str(unit[5])
				print "Attack: " + str(unit[6])
				print "Defence: " + str(unit[7])
				print "Range: " + str(unit[8])
				print "Special: " + Messages().special(unit[9])
			elif abs(unit[0]) == 5:
				playSound(2)
				print "\n\tKnight\n"
				print "             =."
				print "            (| \\"
				print "            (__/        ,\\"
				print "            /) \ (\    '(``,"
				print "           /,_ | |.`. '/'  .\\"
				print "      ====(___(0===================================-"
				print "           /   \\-|./'/   \\ ,/ \\,"
				print "          /____> (/'/    //-\\,')"
				print "    _----_(   /\"_-''\___//   ``"
				print "   /      |(  \\         /"
				print " //| (    |`\\  \\       /"
				print "'/'|      |=/  /      /"
				print "/' \\    )  \\\\/'     '/"
				print "    \\   /   `='(     )"
				print "    |  /-___::--\\  //"
				print "Side: " + self.returnPlayerName(unit[0], player1_name, player2_name)
				print "Location: " + Messages().coordinatesToReadable(unit[1])
				print "Current order: " + Messages().order(unit[2])
				print ""
				print "Price: " + str(unit[3])
				print "Hitpoints: " + str(unit[4][0]) + "/" + str(unit[4][1])
				print "Movement: " + str(unit[5])
				print "Attack: " + str(unit[6])
				print "Defence: " + str(unit[7])
				print "Range: " + str(unit[8])
				print "Special: " + Messages().special(unit[9])
			elif abs(unit[0]) == 6:
				playSound(4)
				print "\n\tCatapult\n"
				print"              ---- 4SSEO"
				print"              \    \SSQ'"
				print"               \ \Y \Sp"
				print"                \;\\_ \\"
				print"              .;'  \\\\"
				print"            .;'     \\\\"
				print"          .;'        \\\\"
				print"  ____  .;'____       \\\\   ____     ____"
				print" / / _\_L / /  \ ______\\\\_/_/__\__ / /  \\"
				print"| | |____| | ++ |_________________| | ++ |"
				print" \_\__/   \_\__/          \_\__/   \_\__/"
				print""
				print "Side: " + self.returnPlayerName(unit[0], player1_name, player2_name)
				print "Location: " + Messages().coordinatesToReadable(unit[1])
				print "Current order: " + Messages().order(unit[2])
				print ""
				print "Price: " + str(unit[3])
				print "Hitpoints: " + str(unit[4][0]) + "/" + str(unit[4][1])
				print "Movement: " + str(unit[5])
				print "Attack: " + str(unit[6])
				print "Defence: " + str(unit[7])
				print "Range: " + str(unit[8])
				print "Special: " + Messages().special(unit[9])
			elif abs(unit[0]) == 7:
				playSound(5)
				print "\n\tHQ\n"
				print "Side: " + self.returnPlayerName(unit[0], player1_name, player2_name)
				print "Location: " + Messages().coordinatesToReadable(unit[1])
				print "Current order: " + Messages().order(unit[2])
				print ""
				print "Price: " + str(unit[3])
				print "Hitpoints: " + str(unit[4][0]) + "/" + str(unit[4][1])
				print "Defence: " + str(unit[7])
				print "Special: " + Messages().special(unit[9])
			elif abs(unit[0]) == 8:
				playSound(6)
				print "\n\tResource camp\n"
				print "Side: " + self.returnPlayerName(unit[0], player1_name, player2_name)
				print "Location: " + Messages().coordinatesToReadable(unit[1])
				print "Current order: " + Messages().order(unit[2])
				print ""
				print "Price: " + str(unit[3])
				print "Hitpoints: " + str(unit[4][0]) + "/" + str(unit[4][1])
				print "Defence: " + str(unit[7])
				print "Special: " + Messages().special(unit[9])
			elif abs(unit[0]) == 9:
				playSound(7)
				print "\n\tBarracks\n"
				print"  _._._                       _._._"
				print" _|   |_                     _|   |_"
				print" | ... |_._._._._._._._._._._| ... |"
				print" | \"\"\" |  \"\"\"    \"\"\"    \"\"\"  | \"\"\" |"
				print" |     |---------------------|     |"
				print" | \"\"\" |  \"\"\"    \"\"\"    \"\"\"  | \"\"\" |"
				print" |[-|-]|  :::   .-\"-.   :::  |[-|-]|"
				print" |     | |~|~|  |_|_|  |~|~| |     |"
				print" |_____|_|_|_|__|_|_|__|_|_|_|_____|"
				print"@@@@@@@@@@@@@@/CcCcCcC\@@@@@@@@@@@@@@"
				print""
				print "Side: " + self.returnPlayerName(unit[0], player1_name, player2_name)
				print "Location: " + Messages().coordinatesToReadable(unit[1])
				print "Current order: " + Messages().order(unit[2])
				print ""
				print "Price: " + str(unit[3])
				print "Hitpoints: " + str(unit[4][0]) + "/" + str(unit[4][1])
				print "Defence: " + str(unit[7])
				print "Special: " + Messages().special(unit[9])
			elif abs(unit[0]) == 10:
				playSound(8)
				print "\n\tArchery range\n"
				print "Side: " + self.returnPlayerName(unit[0], player1_name, player2_name)
				print "Location: " + Messages().coordinatesToReadable(unit[1])
				print "Current order: " + Messages().order(unit[2])
				print ""
				print "Price: " + str(unit[3])
				print "Hitpoints: " + str(unit[4][0]) + "/" + str(unit[4][1])
				print "Defence: " + str(unit[7])
				print "Special: " + Messages().special(unit[9])
			elif abs(unit[0]) == 11:
				playSound(9)
				print "\n\tStable\n"
				print "Side: " + self.returnPlayerName(unit[0], player1_name, player2_name)
				print "Location: " + Messages().coordinatesToReadable(unit[1])
				print "Current order: " + Messages().order(unit[2])
				print ""
				print "Price: " + str(unit[3])
				print "Hitpoints: " + str(unit[4][0]) + "/" + str(unit[4][1])
				print "Defence: " + str(unit[7])
				print "Special: " + Messages().special(unit[9])
			elif abs(unit[0]) == 17:
				playSound(10)
				print "\n\tSiege workshop\n"
				print "Side: " + self.returnPlayerName(unit[0], player1_name, player2_name)
				print "Location: " + Messages().coordinatesToReadable(unit[1])
				print "Current order: " + Messages().order(unit[2])
				print ""
				print "Price: " + str(unit[3])
				print "Hitpoints: " + str(unit[4][0]) + "/" + str(unit[4][1])
				print "Defence: " + str(unit[7])
				print "Special: " + Messages().special(unit[9])
			elif abs(unit[0]) == 12:
				playSound(11)
				print "\n\tWatch tower\n"
				print"                       ._"
				print"                       |~"
				print"                     uuuuu"
				print"                     |_#-|"
				print"                     | _#|"
				print"                     |_ -|"
				print"________ .$$. ______ | - | _____________"
				print"        .#$$$. __    |-  | ....__"
				print"  _.--' $$$$$$    ` -[__N]        `-----"
				print"        $$$$$$    -."
				print"   -.    `:/'    _.))        .--."
				print"          ||   .'.-'     _..-.. _.-."
				print"   ._.-.  '\"  /  (     .'      `."
				print" -'     `.   .    `. -'"
				print"          `. .      `--.."
				print"              `."
				print""
				print "Side: " + self.returnPlayerName(unit[0], player1_name, player2_name)
				print "Location: " + Messages().coordinatesToReadable(unit[1])
				print "Current order: " + Messages().order(unit[2])
				print ""
				print "Price: " + str(unit[3])
				print "Hitpoints: " + str(unit[4][0]) + "/" + str(unit[4][1])
				print "Attack: " + str(unit[6])
				print "Defence: " + str(unit[7])
				print "Range: " + str(unit[8])
				print "Special: " + Messages().special(unit[9])
			elif abs(unit[0]) == 13:
				playSound(12)
				print "\n\tCastle\n"
				print "                      |>>>"
				print "                      |"
				print "        |>>>      _  _|_  _         |>>>"
				print "        |        |;| |;| |;|        |"
				print "    _  _|_  _    \\.    .   /    _  _|_  _"
				print "   |;|_|;|_|;|    \\: +    /    |;|_|;|_|;|"
				print "   \\..       /    ||:+++. |    \\.    .   /"
				print "    \\.  ,   /     ||:+++  |     \\:  .   /"
				print "     ||:+  |_   _ ||_ . _ | _   _||:+  |"
				print "     ||+++.|||_|;|_|;|_|;|_|;|_|;||+++ |"
				print "     ||+++ ||.    .     .      . ||+++.|"
				print "     ||: . ||:.     . .   .  ,   ||:   |"
				print "     ||:   ||:  ,     +       .  ||: , |"
				print "     ||:   ||:.     +++++      . ||:   |"
				print "     ||:   ||.     +++++++  .    ||: . |"
				print "     ||: . ||: ,   +++++++ .  .  ||:   |"
				print "     ||: . ||: ,   +++++++ .  .  ||:   |"
				print "     ||: . ||: ,   +++++++ .  .  ||:   |"
				print ""
				print "Side: " + self.returnPlayerName(unit[0], player1_name, player2_name)
				print "Location: " + Messages().coordinatesToReadable(unit[1])
				print "Current order: " + Messages().order(unit[2])
				print ""
				print "Price: " + str(unit[3])
				print "Hitpoints: " + str(unit[4][0]) + "/" + str(unit[4][1])
				print "Attack: " + str(unit[6])
				print "Defence: " + str(unit[7])
				print "Range: " + str(unit[8])
				print "Special: " + Messages().special(unit[9])
			elif unit[0] == 14:
				print "\n\tResource plant\n"
				print"                  |"
				print"                 |.|"+17*" "+"Resources: "+str(unit[2][0])+"/"+str(unit[2][1])
				print"                 |.|"
				print"                |\./|"
				print"                |\./|"
				print".               |\./|               ."
				print" \^.\          |\\.//|          /.^/"
				print"  \--.|\       |\\.//|       /|.--/"
				print"    \--.| \    |\\.//|    / |.--/"
				print"     \---.|\    |\./|    /|.---/"
				print"        \--.|\  |\./|  /|.--/"
				print"           \ .\  |.|  /. /"
				print" _ -_^_^_^_-  \ \\ // /  -_^_^_^_- _"
				print"   - -/_/_/- ^ ^  |  ^ ^ -\_\_\- -"
				print""
			elif unit[0] == 15:
				print "\n\tHuge resource plant\n"
				print"                  |"
				print"                 |.|"+17*" "+"Resources: "+str(unit[2][0])+"/"+str(unit[2][1])
				print"                 |.|"
				print"                |\./|"
				print"                |\./|"
				print".               |\./|               ."
				print" \^.\          |\\.//|          /.^/"
				print"  \--.|\       |\\.//|       /|.--/"
				print"    \--.| \    |\\.//|    / |.--/"
				print"     \---.|\    |\./|    /|.---/"
				print"        \--.|\  |\./|  /|.--/"
				print"           \ .\  |.|  /. /"
				print" _ -_^_^_^_-  \ \\ // /  -_^_^_^_- _"
				print"   - -/_/_/- ^_^/| |\^_^ -\_\_\- -"
				print"             /_ / | \ _\\"
				print"                  |"
				print""
		elif unit == 16:
			print "\n\tBlock\n"
		
		'''
		print "\n\tUnit\n"
		print "Side: " + self.returnPlayerName(unit[0], player1_name, player2_name)
		print "Location: " + Messages().coordinatesToReadable(unit[1])
		print ""
		print "Price: " + str(unit[2])
		print "Hitpoints: " + str(unit[3][0]) + "/" + str(unit[3][1])
		print "Current order: " + Messages().order(unit[4])
		print "Movement: " + str(unit[5])
		print "Attack: " + str(unit[6])
		print "Defence: " + str(unit[7])
		print "Range: " + str(unit[8])
		print "Special: " + Messages().special(unit[9])
		'''
	
	def returnPlayerName(self,side, player1_name, player2_name):
		if side > 0:
			return player1_name
		else:
			return player2_name