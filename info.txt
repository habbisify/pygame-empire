﻿komennot



	 - näytä pelitilanne



	i - introduction



	c - commands



	x,y - kysy koordinaattien detailit



	x,y,... (coordinates)
		x,y (coordinates) - käske koordinaattien1 yksikköä siirtymään, keräämään resursseja, rakentamaan tai korjaamaan koordinaatteihin2
		
		e (economy) - käske koordinaattien työläistä keräämään annettavaa resurssia lähimmältä resurssikentältä
		
		b,... (build) - rakenna koordinaatteihin rakennus... (tulee olla tarpeeksi resursseja, ruudun tulee olla tyhjä; luo rakennuksen niin, että HP=1(/x) ja komentaa lähimmän workerin rakentamaan, mikäli pelaajalla on workereita; rakennus alkaa toimia vasta kun HP=max)
			h (headquarters) - headquarters
			r (resource camp) - resource camp
			b (barracks) - barracks
			a (archery range) - archery range
			s (stable) - stable
			w (siege workshop) - siege workshop
			t (watch tower) - watch tower
			c (castle) - castle
		
		t,... (train) - kouluta koordinaattien rakennuksesta...
			s (swordman) - swordman
			e (elite swordman) - elite swordman
			a (archer) - archer
			k (knight) - knight
			c (catapult) - catapult
			w (workers) - worker
		
		d (delete) - kierrätä koordinaattien rakennus (saat siitä 2/3 * HP(nyt) / HP(max) resurssia takaisin ja menetät vuorosi)



	e (economy) - käske kaikkia työläisiä keräämään resursseja lähimmältä resurssikentältä



	t... (train) - kouluta yksikkö per sitä valmistava rakennus (tulee olla tarpeeksi resursseja ja tilaa rakennuksen vieressä; jos ei tarpeeksi kaikkiin mutta ainakin yhteen, niin arpoo, minne joukot valmistuvat)
		n (int)... (n=1 -> n ei pakollinen)
			s (swordman) - swordman
			e (elite swordman) - elite swordman
			a (archer) - archer
			k (knight) - knight
			c (catapult) - catapult
			w (workers) - worker
			C (clear) - tyhjentää koulutuslistan



	au,... (all units) - valitse kaikki yksiköt
	w,... (workers) - valitse kaikki workerit
	m,... (military) - valitse kaikki sotilaat
	s,... (swordman) - valitse kaikki swordmanit
	e,... (elite swordman) - valitse kaikki elite swordmanit
	a,... (archer) - valitse kaikki archerit
	k,... (knight) - valitse kaikki knightit
	c,... (catapult) - valitse kaikki catapultit
		m,... (movement) - käske kaikkia [yksikkötyyppi] siirtymään koordinaatteihin
			x,y - määränpää
		s,... (strategy) - muuta kaikkien [sotilastyyppi] strategiaa (EI worker)
			a (agressive) - kaikki käskyttömät [sotilastyypit] marssivat omalle taisteluetäisyydelle vihollisesta, mikäli näkevät tämän viiden ruudun päässä
			d (defensive) - kaikki käskyttömät [sotilastyypit] marssivat omalle taisteluetäisyydelle vihollisesta, joka on taisteluetäisyydellään heihin tai kahden ruudun päässä
			h (hold) - [sotilastyypit] pitävät asemansa
				
			p,... (primary target) - [sotilastyyppi] priorisoi kohteekseen...
				r / [¬,] (random) - random
				m,... (military) - ...sotilaat tyypiltään...
					r / [¬,] (random) - random
					s (swordman) - swordman
					e (elite swordman) - elite swordman
					a (archer) - archer
					k (knight) - knight
					c (catapult) - catapult
				w (workers) - workerit
				b,... (buildings) - ...rakennukset tyypiltään...
					r / [¬,] (random) - random
					h (headquarters) - headquarters
					r (resource camp) - resource camp
					b (barracks) - barracks
					a (archery range) - archery range
					s (stable) - stable
					w (siege workshop) - siege workshop
					t (watch tower) - watch tower
					c (castle) - castle



	[SPACEBAR] - ohita vuoro



	r - luovuta
		- takaisin
		y (yes) - juu
		n (no) - ei
_________________________________________________________________________________________________________________________________________________________________

ominaisuudet
	positiiviset luvut = player1; negatiiviset luvut = player2
	1 - worker P=10 HP=10 M=2 A=3 D=0 R=1 S="Able to collect resources and build structures" C=10
	2 - swordman P=15 HP=30 M=2 A=5 D=1 R=1 S="Attack +4 vs knight"
	3 - elite swordman P=30 HP=50 M=3 A=7 D=2 R=1 S="Attack +5 vs knight"
	4 - archer P=20 HP=20 M=2 A=3 D=0 R=4 S="Attack +2 vs targets R=2, +1 if R=3. Attack +2 vs targets M<=2. Bonuses do stack. Damages buildings only with A=1"
	5 - knight P=50 HP=70 M=4 A=7 D=2 R=1 S="-"
	6 - catapult P=50 HP=20 M=1 A=20 D=1 R=(2-)5 S="Must reload every other turn. Penetrates building's armour (D=0 when hit). Area of effective: all the squares adjacent vertically or horizontally to the target will be hit with 1/4 of A. NOTE: Has a minimum range"(, F=1)
	
	7 - headquarters P=75 HP=100 D=3 S="May produce workers. May store resources. Game is lost if all headquarters of a player are destroyed"
	8 - recource camp P=30 HP=50 D=1 S="May store resources"
	9 - barracks P=50 HP=75 D=1 S="May produce swordmen. If player owns at least one castle, he may produce elite swordmen."
	10 - archery range P=50 HP=75 D=1 S="May produce archers"
	11 - stable P=50 HP=75 D=1 S="May produce knights"
	(17?) - siege workshop P=75 HP=75 D=1 S="May produce catapults"
	12 - watch tower P=50 HP=100 A=3 D=3 R=4 S="-"
	13 - castle P=120 HP=200 A=7 D=3 R=4 S="Unlocks elite swordman"
	
	neutraaleja itemejä:
	14 - resource plant N=300
	15 - huge resource plant N=500
	16 - block HP=10

olemassa olevan yksikön ominaisuudet taulukossa (kaikki mahdolliset listattu järjestykseen)
																		All		Units	Buildings	Neutral items |	Worker	Watch tower	Castle	Resource plants
	nimi - 13 (castle)													#
	puoli - 1 (player1), -1 (player2) EI OMAA PARAMETRIA						#		#
	sijainti - [3,5] (x=4,y="f")										#
	
	nykyinen käsky - 															#
		0 (ei käskyä), 1 (kerää resursseja), [2,[5,7]] (liiku, minne),
		[3, 8, [13,2]] (rakenna, mitä, minne)
	
	hinta																		#		#
	elämät - [5, 10] (5/10 elämää)												#		#
	liikkuvuus																	#
	hyökkäys																	#											#			#
	puolustus																	#		#
	hyökkäysetäisyys															#											#			#
	erikoisominaisuudet - 3 (3. special)										#		#
	yksikön kantamat resurssit																						#
	yksiköstä louhittavat resurssit - [150, 200] (150/200 resurssia)																			#

pisteytys
	NP=1/20*pelaajan resurssit + 1/5*pelaajan yksiköiden ja rakennusten kokonaishinta + 2 per sotilas + 5 pelaajalle, jolla on eniten työläisiä + 10 pelaajalle, jolla on suurin armeija + 5 joka HQsta ja linnasta

aloitusresurssit P1:N=30,P2:N=33
_________________________________________________________________________________________________________________________________________________________________

Cheats:
	olenjuutalainen - Gain 1000 resourses.
	ferrari - All of your knights replace their horses with Ferraris. Their movement speed is increased +4 and they have new fancy sprites.
	juodaanviinaaa - All of your units will gain +1 to their attack. Once per movement, they move 1 square to random direction. In addition, your strategy is aggressive for the rest of the game.
	rutto[∅∨1∨2] - All units and buildings of the indexed player go to HP=1. If you type "rutto" without any index, all troops and buildings suffer from this. This even affects to objects made after typing the cheatcode.
	vastustajanionpaskanaama - Immediately destroys all enemy objects except one HQ.
	olenviineri - Wins the game. Quite boring.
_________________________________________________________________________________________________________________________________________________________________

pathfinding v.2
	def pathfinding(A,C,M)
		1. Määriteellään aloituspiste A=[x,y], mielivaltainen piste B=[x,y] ja lopetuspiste C=[x,y] kartalla M. Määritellään P0=-1, paths = [] ja L = []. Jos A=C -> P0=-2, return P0
		2. Luodaan neljä pathia P1, P2, P3, P4. Jokainen P saa pisteen A arvon ensimmäiseksi alkiokseen. P1:lle luodaan toinen alkio B1, B1=A+[1,0]. P2 <- B2, B2=A+[0,1]. P3 <- B3, B3=A+[-1,0]. P4 <- B4, B4=A+[0,-1].
		3. while n<k (k = syvyyden raja):
			1. Tarkistetaan ∀P, onko viimeinen alkio B(n)=C. Jos on, kyseinen P=P(min) -> P=P0, return P0
			2. Tarkistetaan ∀P, onko viimeisen alkion arvo kartalla nolla, (M(B(n)))=0. Jos ei ole, P törmää esteeseen ja tuhoutuu.
			3. Tarkistetaan ∀P, onko viimeinen alkio lukitsematon, (B(n) in L)=0. Jos ei ole, P törmää optimaalisempaan reittiin ja tuhoutuu.
			4. Tarkistetaan ∀P, onko viimeisellä alkiolla B(n) olemassa duplikaattia, (B(n) in ∃P)>=2. Jos on, arvo niistä yksi. Tuhoa muut P kuin valittu. Duplikaatit ovat keskenään yhtä optimaalisia.
			5. Merkitään kaikkien P käyttämät B lukituiksi, L <- ∀P(B).
			6. Luodaan ∀P neljä uutta pathia P(n+1), P(n+2), P(n+3), P(n+4) Jokaiselle uudelle pathille annetaan vanhan alkiot, P(n+k) <- P(n).
			7. P(n+1):lle luodaan uusi alkio B(n+1), B(n+1)=B(n)+[1,0]. P(n+2) <- B(n+2), B(n+2)=B(n)+[0,1]. P(n+3) <- B(n+3), B(n+3)=B(n)+[-1,0]. P(n+4) <- B(n+4), B(n+4)=B(n)+[0,-1].
			n+=1
	
	N = [[1,0],[0,-1],[-1,0],[-1,0],[0,1],[0,1],[1,0],[1,0]...] C:n variaatio edellisesta tutkinnasta
	
	while n<k (k = tutkittavien ruutujen määrän raja, jos reittiä ei löydy tutkittavaan ruutuun):
		P0 = pathfinding(A,C,M)
		Jos P0≠-1 -> return P0
		C+=N[n]
		n+=1

pathfinding v.1
	1. Tarkista, että ainakin yksi objektin viereinen ruutu on tyhjä ja ainakin kaksi viereisen tyhjän ruudun viereistä ruutua ovat tyhjiä
	2. Tarkista, että kohde on tyhjä ja ainakin yksi sen viereinen ruutu on tyhjä ja ainakin kaksi edellä mainitun ruudun viereisistä ruuduista ovat tyhjiä
	3. Jos kohdan 2 tarkistukset palauttivat epätoden, tee uusi tarkistus kohteen viereisille ruuduille. Tee kunnes löytyy saavutettava kohde, ja aseta se uudeksi määränpääksi.
	4. Toista kunnes reitti1 tai umpikuja tai n > 1000:
		n += 1
		1. Laske objektin x- ja y-etäisyys kohteeseen
		2. Tarkista objektin pidempää etäisyyttä lyhentävä suuntaruutu
		3. Jos ruutu on tyhjä eikä siinä olla vielä käyty, siirry siihen
		Luo kaksi reittiä:
			4. Jos ehdot eivät toteutuneet, tarkista objektin lyhyempää etäisyyttä lyhentävä suuntaruutu
			5. -3
			6. Jos ehdot eivät toteutuneet, tarkista objektin lyhyempää etäisyyttä pidentävä suuntaruutu
			7. -3
			8. Jos ehdot eivät toteutuneet, tarkista objektin pidempää etäisyyttä pidentävä suuntaruutu
			9. Jos ruutu on tyhjä, siirry siihen
			10. Jos ehdot eivät toteutuneet, tarkista objektin lyhyempää etäisyyttä pidentävä suuntaruutu
			11. -9
			14. Jos ehdot eivät toteutuneet, tarkista objektin lyhyempää etäisyyttä lyhentävä suntaruutu
			15. -9
			16. Jos ehdot eivät toteutuneet, tarkista objektin pidempää etäisyyttä lyhentävä suuntaruutu
			17. -9
			18. Jos objekti on kohteessa, niin reitti1. Jos ehdot eivät toteutuneet, niin umpikuja
			
			4. Jos ehdot eivät toteutuneet, tarkista objektin lyhyempää etäisyyttä pidentävä suuntaruutu
			5. -3
			6. Jos ehdot eivät toteutuneet, tarkista objektin lyhyempää etäisyyttä lyhentävä suuntaruutu
			7. -3
			8. Jos ehdot eivät toteutuneet, tarkista objektin pidempää etäisyyttä pidentävä suuntaruutu
			9. Jos ruutu on tyhjä, siirry siihen
			10. Jos ehdot eivät toteutuneet, tarkista objektin lyhyempää etäisyyttä pidentävä suuntaruutu
			11. -9
			14. Jos ehdot eivät toteutuneet, tarkista objektin lyhyempää etäisyyttä lyhentävä suntaruutu
			15. -9
			16. Jos ehdot eivät toteutuneet, tarkista objektin pidempää etäisyyttä lyhentävä suuntaruutu
			17. -9
			18. Jos objekti on kohteessa, niin reitti1
	
	5. Tee kaikille löytyneille reiteille:
		1. Jos reitti löytyi, etsi samat koordinaatit ja poista reitit niiden välistä, esim. a1, a2, -b2, -a2,- a3, b3, c3, c2, b2,- b1 -> a1, a2, b2, b1
		2. Muodosta kaikki mahdolliset reitit rekursiolla
		- tarkistaa, mitkä ovat mahdollisia suuntaruutuja koottujen koordinaattien perusteella
		- luo jokaisesta mahdollisesta permutaatiosta uuden reitin
		- yhtä koordinaattia voidaan käyttää vain kerran -> jos reitti joutuu umpikujaan, se tuhoaa itsensä
		- saadaan kaikki mahdolliset reitit
	6. Valitse lyhin reitti
	7. Palauta n-askelta reittiä edenneen objektin koordinaatit
	8. Jos reittiä ei löytynyt, palauta n-askelta mahdollisimman lähelle kohdetta edenneen objektin koordinaatit