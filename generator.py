from random import random,shuffle

from startgenerator import *
from commands import *
from features import *
from messages import *
from pygameactivity import *
from audio import *

class ScoreGenerator():
	def __init__(self):
		pass
	def generateScore(self,side,player_resources,position):
		score = 0
		army1 = 0
		army2 = 0
		eco1 = 0
		eco2 = 0
		for line in position:
			for node in line:
				if node != 0:
					if abs(node[0]) in [1,2,3,4,5,6,7,8,9,10,11,12,13,17]:
						if ((side == 1 and node[0] > 0) or (side == 2 and node[0] < 0)) and node[9] != -1:
							score += int(Commands().getObjectPriceByInt(abs(node[0]))*0.2)
							if abs(node[0]) in [2,3,4,5,6]:
								army1 += 1
								score += 2
							elif abs(node[0]) in [7,13]:
								score += 5
							elif abs(node[0]) == 1:
								eco1 += 1
						elif (side == 1 and node[0] < 0) or (side == 2 and node[0] > 0) and node[9] != -1:
							if abs(node[0]) in [2,3,4,5,6]:
								army2 += 1
							elif abs(node[0]) == 1:
								eco2 += 1

		if army1 > army2:
			score += 10
		if eco1 > eco2:
			score += 5
		score += int(player_resources*0.05)
		return score



class TrainingGenerator():
	def __init(self):
		pass
	def trainUnits(self,screen,training_queue,position):

		training_event_list = []
		N = [[1,0],[1,-1],[0,-1],[-1,-1],[-1,0],[-1,1],[0,1],[1,1]]

		player_index = 0
		for player_queue in training_queue:
			order_index = 0
			for order in player_queue:

				building_c = order[2]

				#Ensin tarkistetaan, ettei rakennus ole tuhoutunut
				if type(position[building_c[1]][building_c[0]]) is list:
					if abs(position[building_c[1]][building_c[0]][0]) not in [7,9,10,11,17]:
						training_queue[player_index][order_index] = "trash" #Ilman trash-ratkaisua overflow-error

					else:
						empty_square_list = []
						for n in N:
							if 0 <= building_c[1]+n[1] < len(position) and 0 <= building_c[0]+n[0] < len(position[0]):
								if position[building_c[1]+n[1]][building_c[0]+n[0]] == 0:
									empty_square_list.append([int(building_c[0]+n[0]),int(building_c[1]+n[1])]) #x,y

						empty_squares_n = len(empty_square_list)

						unit_trained_portion = order.pop(3)
						if int(unit_trained_portion + float(Commands().getUnitTrainingSpeedByInt(order[0]))) <= empty_squares_n or unit_trained_portion + float(Commands().getUnitTrainingSpeedByInt(order[0])) < 1:
							unit_trained_portion += float(Commands().getUnitTrainingSpeedByInt(order[0]))
						else:
							unit_trained_portion = 0.99 + empty_squares_n


						train_amount = int(unit_trained_portion)
						if train_amount >= order[1]:
							train_amount = int(order[1])
							training_queue[player_index][order_index] = "trash" #Ilman trash-ratkaisua overflow-error
						else:
							new_unit_trained_portion = unit_trained_portion - train_amount
							order.insert(3, new_unit_trained_portion)

						space_exists = True
						while train_amount > 0 and space_exists:
							if len(empty_square_list) > 0: #Onko rakennuksen vieressa tilaa uudelle unitille
								i = int(random()*len(empty_square_list))

								position[empty_square_list[i][1]][empty_square_list[i][0]] = ObjectGenerator().generateByInt(player_index*-2+1,list(empty_square_list[int(i)]),order[0]) #worker: (1,empty_square))

								if abs(order[0]) == 1:
									playSound("workerspawn")
								elif abs(order[0]) == 6:
									playSound("catapult")
								else:
									playSound("warriorspawn")

								PygAct().drawAction(screen,7,position[empty_square_list[i][1]][empty_square_list[i][0]],position[empty_square_list[i][1]][empty_square_list[i][0]][1],0,0)

								empty_square_list.pop(i)
								train_amount -= 1
								old_amount = order.pop(1)
								order.insert(1,old_amount-1)

				order_index +=1
			player_index +=1

		for player_queue in training_queue:
			while player_queue.count("trash") > 0:
				player_queue.remove("trash")

		return training_event_list,training_queue,position






class CombatGenerator():
	def __init__(self):
		pass

	def prioritizeTarget(self,position,attacker,possible_targets_c):



		def calcPriority(target,the_target_c,i,wound_priority,distance_priority,corner_priority,target_priority):
			new_priority = 1000*i + 900 + wound_priority + distance_priority + corner_priority
			if new_priority < target_priority:
				return list(target[1]),int(new_priority)
			else:
				return the_target_c,target_priority



		attacker_id = attacker[0]
		target_priority = 99999 #Random big number
		the_target_c = []

		for target_c in possible_targets_c:

			target = position[target_c[1]][target_c[0]]
			target_id = target[0]



			#Haavoittuneisuuden vaikutus priorisointiin
			wound_priority = int(target[4][0]*1.0/target[4][1]*60)

			distance = abs(target_c[0]-attacker[1][0])+abs(target_c[1]-attacker[1][1])
			#Kulmikkain olevat sotilaat priorisoivat toisensa, jos kahden ruudun paassa on ei-kulmikkainen vastustajan yksikko
			#Jos seka pysty-vaaka -akselilla etta kulmittain on vieressa vihollinen, priorisoidaan pysty-vaaka
			corner_priority = 0
			if distance == 2 and abs(target_c[0]-attacker[1][0]) == 1 and abs(target_c[1]-attacker[1][1]) == 1:
				distance = 1
				corner_priority = 1

			#Vieressa olevaa yksikkoa vastaan tappeleminen on tarkeinta
			if distance == 1:
				distance_priority = -120
			#Archerin priorisoinnit
			elif abs(attacker_id) == 4:
				if distance == 2:
					distance_priority = -60
				elif distance == 3:
					distance_priority = -30
				else:
					distance_priority = 40
			#Rangen vaikutus priorisointiin
			else:
				distance_priority = (distance-1)*10 #Max range to function = 5



			#Swordmanit ja elite swordmanit priorisoivat knightit
			if abs(attacker_id) in [2,3] and abs(target_id) == 5:

				general_priority = 0

			#Knightit priorisoivat catapultit
			elif abs(attacker_id) == 5 and abs(target_id) == 6:

				general_priority = 0

			#Catapultit priorisoivat toiset catapultit
			elif abs(attacker_id) == 6 and abs(target_id) == 6:

				general_priority = 0

			#Defensiiviset struktuurit priorisoivat aloitettavat rakennukset niiden LOSilla, jos HP = 1
			elif abs(attacker_id) in [12,13] and abs(target_id) in [7,8,9,10,11,12,13,17] and target[4][0] == 1 and target[4][0] == 1:

				general_priority = 0

			#Knightit priorisoivat archerit
			elif abs(attacker_id) == 5 and abs(target_id) == 4:

				general_priority = 1

			#Vastustajan militaryn priorisointi
			elif abs(target_id) in [2,3,4,5,6] and target_priority > 1000:

				general_priority = 2

			#Vastustajan ekonomian priorisointi (Catapult: defensiiviset struktuurit)
			elif ((abs(attacker_id) != 6 and abs(target_id) == 1) or (abs(attacker_id) == 6 and abs(target_id) in [12,13])) and target_priority > 2000:

				general_priority = 3

			#Vastustajan defensiivisten struktuurien priorisointi (Catapult: ekonomia)
			elif ((abs(attacker_id) != 6 and abs(target_id) in [12,13]) or ((abs(attacker_id) == 6 and abs(target_id) == 1))) and target_priority > 3000:

				general_priority = 4

			#Vastustajan HQ:n ja military struktuurien priorisointi
			elif abs(target_id) in [7,9,10,11,17] and target_priority > 4000:

				general_priority = 5

			#Vastustajan Resource campin priorisointi
			elif abs(target_id) == 8 and target_priority > 5000:

				general_priority = 6

			the_target_c, target_priority = calcPriority(target,the_target_c,general_priority,wound_priority,distance_priority,corner_priority,target_priority)

		return the_target_c

	def rangedAttacks(self,screen,p1,p2,position):
		events = []
		ranged = []
		i=0
		for row in position:
			j=0
			for node in row:
				if node != 0:
					possible_targets = []
					#P1
					if node[0] == 4 or (node[0] == 6 and node[10] == 1) or (node[0] in [12,13] and node[9] != -1):
						n=0
						for y in position:
							m=0
							for x in y:
								if position[n][m] != 0:
									if abs(node[0]) != 6:
										if position[n][m][0] < 0 and abs(i-n) + abs(j-m) <= node[8]:
											possible_targets.append([m,n])
									else:
										if position[n][m][0] < 0 and (2 <= abs(i-n) + abs(j-m) <= node[8] and (abs(i-n) > 1 or abs(j-m) > 1)):
											possible_targets.append([m,n])
								m+=1
							n+=1
					#P2
					elif node[0] == -4 or (node[0] == -6 and node[10] == 1) or (node[0] in [-12,-13] and node[9] != -1):
						n=0
						for y in position:
							m=0
							for x in y:
								if type(position[n][m]) is list:
									if abs(node[0]) != 6:
										if (0 < position[n][m][0] < 14 or position[n][m][0] == 17) and abs(i-n) + abs(j-m) <= node[8]:
											possible_targets.append([m,n])
									else:
										if (0 < position[n][m][0] < 14 or position[n][m][0] == 17) and (2 <= abs(i-n) + abs(j-m) <= node[8] and (abs(i-n) > 1 or abs(j-m) > 1)):
											possible_targets.append([m,n])
								m+=1
							n+=1

					if abs(node[0]) == 6 and node[10] == 0:
						node[10] = 1

					if len(possible_targets) > 0:
						shuffle(possible_targets)
						target_defined = False
						if node[2] != 0:
							if position[node[2][1][1]][node[2][1][0]] != 0:
								#Ensimmainen ehto on varmistus, etteivat ala ampumaan omia
								if ((position[node[2][1][1]][node[2][1][0]][0] < 0 and node[0] > 0) or (position[node[2][1][1]][node[2][1][0]][0] > 0 and node[0] < 0)) and node[2][0] == 5 and abs(node[1][0]-node[2][1][0])+abs(node[1][1]-node[2][1][1]) <= node[8]:
									target = list(node[2][1])
									target_defined = True
						if not target_defined:
							target = self.prioritizeTarget(position,node,possible_targets)
						ranged.append([[int(j),int(i)],target]) #[attacker, defender]
				j+=1
			i+=1

		while len(ranged) > 0:

			i = int(random()*len(ranged))
			attacker = position[ranged[i][0][1]][ranged[i][0][0]]

			N = [[1,0],[0,-1],[-1,0],[0,1]]

			defenders = [position[ranged[i][1][1]][ranged[i][1][0]]]
			#Katapultin aoe
			if attacker != 0:
				if abs(attacker[0]) == 6:
					for n in N:
						if 0 <= ranged[i][1][0]+n[0] < len(position[0]) and 0 <= ranged[i][1][1]+n[1] < len(position):
							if position[ranged[i][1][1]+n[1]][ranged[i][1][0]+n[0]] != 0:
								if abs(position[ranged[i][1][1]+n[1]][ranged[i][1][0]+n[0]][0]) in Commands().getHPUnits():
									defenders.append(position[ranged[i][1][1]+n[1]][ranged[i][1][0]+n[0]])

			nth_defender = 0
			for defender in defenders:

				#Hyokkaaja voi olla kuollut saman vuoron taisteluissa
				if defender != 0 and attacker != 0:

					if abs(defender[0]) in [1,2,3,4,5,6,7,8,9,10,11,12,13,17]:

						attacker_id = attacker[0]
						defender_id = defender[0]

						#Archer specialities
						if abs(attacker_id) == 4:

							distance = abs(attacker[1][0]-defender[1][0]) + abs(attacker[1][1]-defender[1][1])
							if abs(attacker[1][0]-defender[1][0]) == 1 and abs(attacker[1][1]-defender[1][1]) == 1:
								distance = 1
							if distance == 2:
								attack = attacker[6]+Features().archerABr2()
							elif distance == 3:
								attack = attacker[6]+Features().archerABr3()
							else:
								attack = attacker[6]

							#Slow target
							if defender[5] <= 2:
								attack += Features().archerABslow()

							#Building target
							if abs(defender_id) in [7,8,9,10,11,12,13,17]:
								attack = Features().archerAvsB()

						else:
							attack = attacker[6]

						if nth_defender:
							attack = int(attacker[6]*Features().catapultAoE())

						HP = defender[4].pop(0)

						#Rakennus, joka ei ole valmis, ei ole yhta vahva
						if defender[9] != -1:
							armour = defender[7]
						else:
							armour = 0

						if attacker_id != 6 or abs(defender_id) not in [7,8,9,10,11,12,13,17]:
							if armour >= attack:
								HP -= 1
							else:
								HP -= int(attack-armour)
						else:
							HP -= int(attack)

						#Make catapult fire only every other turn
						if abs(attacker_id) == 6:
							attacker[10] = 0

						if abs(attacker_id) in [4,12]:
							playSound("archershoot")
						elif abs(attacker_id) == 6:
							if not nth_defender:
								playSound("catapultshoot")
						else:
							playSound("castleshoot")

						if HP > 0:
							defender[4].insert(0,HP)
							Messages().printAttackInfo("ranged",int(attacker[6]),attacker,defender,p1,p2)
							events.append("343 Damage.")
						else:

							Commands().playDyingSound(abs(defender_id))

							position = Commands().objectDiedReactions(position,defender)

							Messages().printAttackInfo("ranged",-1,attacker,defender,p1,p2)
							position[defender[1][1]][defender[1][0]] = 0

						PygAct().drawAction(screen,5,attacker,attacker[1],defender,defender[1])

				nth_defender += 1

			ranged.pop(i)

		return events, position

	def handtohandAttacks(self,screen,p1,p2,position):
		N = [[1,0],[1,-1],[0,-1],[-1,-1],[-1,0],[-1,1],[0,1],[1,1]]
		events = []
		combats = []
		i=0
		for row in position:
			j=0
			for node in row:
				if type(node) is list:
					possible_targets = []
					#P1
					if 1 <= node[0] <= 3 or node[0] == 5:
						for n in N:
							if (0 <= i+n[1] < len(position)) and (0 <= j+n[0] < len(row)):
								if type(position[i+n[1]][j+n[0]]) is list:
									if position[i+n[1]][j+n[0]][0] < 0:
										possible_targets.append([int(j)+int(n[0]),int(i)+int(n[1])])
					#P2
					elif -1 >= node[0] >= -3 or node[0] == -5:
						for n in N:
							if (0 <= i+n[1] < len(position)) and (0 <= j+n[0] < len(row)):
								if type(position[i+n[1]][j+n[0]]) is list:
									if 0 < position[i+n[1]][j+n[0]][0] < 14 or position[i+n[1]][j+n[0]][0] == 17:
										possible_targets.append([int(j)+int(n[0]),int(i)+int(n[1])])

					if len(possible_targets) > 0:
						shuffle(possible_targets)
						target_defined = False
						if node[2] != 0:
							if position[node[2][1][1]][node[2][1][0]] != 0:
								range = node[8]
								if abs(node[1][0]-node[2][1][0]) == 1 and abs(node[1][1]-node[2][1][1]) == 1:
									range = 2
								#Ensimmainen ehto on varmistus, etteivat ala tappamaan omia
								if ((position[node[2][1][1]][node[2][1][0]][0] < 0 and node[0] > 0) or (position[node[2][1][1]][node[2][1][0]][0] > 0 and node[0] < 0)) and node[2][0] == 5 and abs(node[1][0]-node[2][1][0])+abs(node[1][1]-node[2][1][1]) <= range:
									target = list(node[2][1])
									target_defined = True
						if not target_defined:
							target = self.prioritizeTarget(position,node,possible_targets)
						combats.append([[int(j),int(i)],target]) #[attacker, defender]
				j+=1
			i+=1

		#print combats
		while len(combats) > 0:

			i = int(random()*len(combats))
			attacker = position[combats[i][0][1]][combats[i][0][0]]
			defender = position[combats[i][1][1]][combats[i][1][0]]

			if defender != 0 and attacker != 0:

				attacker_id = attacker[0]
				defender_id = defender[0]

				HP = defender[4].pop(0)

				#Rakennus, joka ei ole valmis, ei ole yhta vahva
				if defender[9] != -1:
					armour = defender[7]
				else:
					armour = 0

				#Swordman vs knight -attack bonus
				if abs(attacker_id) == 2 and abs(defender_id) == 5:
					attack = attacker[6]+Features().swordmanABvsN()
				elif abs(attacker_id) == 3 and abs(defender_id) == 5:
					attack = attacker[6]+Features().eliteswordmanABvsN()
				else:
					attack = attacker[6]

				if armour >= attack:
					HP -= 1
				else:
					HP -= int(attack-armour)

				if abs(attacker_id) == 1:
					playSound("villagerfight")
				else:
					playSound("warriorfight")

				if HP > 0:
					defender[4].insert(0,HP)

					Messages().printAttackInfo("hand to hand",int(attacker[6]),attacker,defender,p1,p2)
					events.append("393 Damage.")
				else:

					Commands().playDyingSound(abs(defender_id))

					position = Commands().objectDiedReactions(position,defender)

					Messages().printAttackInfo("hand to hand",-1,attacker,defender,p1,p2)
					position[combats[i][1][1]].pop(combats[i][1][0])
					position[combats[i][1][1]].insert(combats[i][1][0],0)
					events.append("397 Fatality.")

				PygAct().drawAction(screen,5,attacker,attacker[1],defender,defender[1])

				combats.pop(i)
			else:
				combats.pop(i)

		return events, position

	def createCombats(self,screen,p1,p2,position):
		combat_event_list = []
		events, position = self.rangedAttacks(screen,p1,p2,position)
		combat_event_list.append(list(events))
		events, position = self.handtohandAttacks(screen,p1,p2,position)
		combat_event_list.append(list(events))
		return combat_event_list,position






class EconomicGenerator():
	def __init__(self):
		pass

	def generateAutomaticResourceGatherings(self,side,position):
		i=0
		for row in position:
			j=0
			for node in row:
				if type(node) is list:
					if node[0] == side:
						if node[2] == 0:
							position[i][j].pop(2)
							position[i][j].insert(2,[2,[int(j),int(i)],[int(j),int(i)]])
				j+=1
			i+=1
		return position

	def automaticEconomicMovement(self,screen,position):
		#TODO May include unnecessary duplicates
		movement_lists = []

		N = [[1,0],[1,-1],[0,-1],[-1,-1],[-1,0],[-1,1],[0,1],[1,1]]

		#Resource -> resource stock place
		i=0
		for row in position:
			j=0
			for node in row:
				if type(node) is list:

					#Player1 -logiikka
					if node[0] == 1:
						if type(node[2]) is list:

							#Worker kantaa jo maksimimaaraa resursseja -> kasky palata lahimmalle HQ:lle tai Resource campille
							if node[2][0] == 2 and node[10] == Features().workerCarry():

								worker_next_to_correct_building = False
								for n in N:
									if (0 <= i+n[1] < len(position)) and (0 <= j+n[0] < len(row)):
										if type(position[i+n[1]][j+n[0]]) is list:
											if 7 <= position[i+n[1]][j+n[0]][0] <= 8:
												worker_next_to_correct_building = True
												break

								if worker_next_to_correct_building == False:

									worker_resourcestockplace_distances = []
									n=0
									for row2 in position:
										m=0
										for node2 in row2:
											if type(node2) is list:
												if 7 <= node2[0] <= 8:
													worker_resourcestockplace_distance = abs(i-n)+abs(j-m)
													worker_resourcestockplace_distances.append([list([m,n]),int(worker_resourcestockplace_distance)]) #node = [[C(x),C(y)], distance from unit to the node]
											m+=1
										n+=1

									min_distance = 1000 #Mielivaltainen suuri luku
									min_coordinates = [0,0]
									for k in worker_resourcestockplace_distances:
										if k[1] < min_distance:
											min_distance = int(k[1])
											min_coordinates = list(k[0])

									if min_distance != 1000:
										movement_lists.append([2,[j,i],min_coordinates])

					#Player2 -logiikka
					if node[0] == -1:
						if type(node[2]) is list:

							#Worker kantaa jo maksimimaaraa resursseja -> kasky palata lahimmalle HQ:lle tai Resource campille
							if node[2][0] == 2 and node[10] == Features().workerCarry():

								worker_next_to_correct_building = False
								for n in N:
									if (0 <= i+n[1] < len(position)) and (0 <= j+n[0] < len(row)):
										if type(position[i+n[1]][j+n[0]]) is list:
											if -7 >= position[i+n[1]][j+n[0]][0] >= -8:
												worker_next_to_correct_building = True
												break

								if worker_next_to_correct_building == False:

									worker_resourcestockplace_distances = []
									n=0
									for row2 in position:
										m=0
										for node2 in row2:
											if type(node2) is list:
												if -7 >= node2[0] >= -8:
													worker_resourcestockplace_distance = abs(i-n)+abs(j-m)
													worker_resourcestockplace_distances.append([list([m,n]),int(worker_resourcestockplace_distance)]) #node = [[C(x),C(y)], distance from unit to the node]
											m+=1
										n+=1

									min_distance = 1000 #Mielivaltainen suuri luku
									min_coordinates = [0,0]
									for k in worker_resourcestockplace_distances:
										if k[1] < min_distance:
											min_distance = int(k[1])
											min_coordinates = list(k[0])

									movement_lists.append([2,[j,i],min_coordinates])

				j+=1
			i+=1

		#Resource stock place or lost -> resource
		i=0
		for row in position:
			j=0
			for node in row:
				if type(node) is list:

					#Player1 & Player2 -logiikka
					if abs(node[0]) == 1:
						if type(node[2]) is list:

							conditions_correct = False
							if (node[2][0] == 2 and node[10] == 0):
								conditions_correct = True
							if type(position[node[2][1][1]][node[2][1][0]]) is list:
								if node[2][0] == 2 and node[10] != Features().workerCarry() and not(7 <= abs(position[node[2][1][1]][node[2][1][0]][0]) <= 8):
									conditions_correct = True
							elif node[2][0] == 2 and node[10] != Features().workerCarry():
								conditions_correct = True

							if conditions_correct:
								worker_not_already_heading_for_resources = True
								if type(position[node[2][1][1]][node[2][1][0]]) is list:
									if 14 <= position[node[2][1][1]][node[2][1][0]][0] <= 15:
										worker_not_already_heading_for_resources = False

								worker_next_to_resource = False
								for n in N:
									if (0 <= i+n[1] < len(position)) and (0 <= j+n[0] < len(row)):
										if type(position[i+n[1]][j+n[0]]) is list:
											if 14 <= position[i+n[1]][j+n[0]][0] <= 15:
												worker_next_to_resource = True
												break

								if worker_next_to_resource == False:

									if worker_not_already_heading_for_resources:

										worker_resource_distances = []
										n=0
										for row2 in position:
											m=0
											for node2 in row2:
												if type(node2) is list:
													if 14 <= node2[0] <= 15:
														worker_resource_distance = abs(i-n)+abs(j-m)
														worker_resource_distances.append([list([m,n]),int(worker_resource_distance)]) #node = [[C(x),C(y)], distance from unit to the node]
												m+=1
											n+=1

										min_distance = 1000 #Mielivaltainen suuri luku
										min_coordinates = [0,0]
										for k in worker_resource_distances:
											if k[1] < min_distance:
												min_distance = int(k[1])
												min_coordinates = list(k[0])

										movement_lists.append([2,[j,i],min_coordinates])

									else:

										movement_lists.append([2,[j,i],node[2][1]])

				j+=1
			i+=1

		#Refreshes the current action of the workers
		for movement_list in movement_lists:
			position[movement_list[1][1]][movement_list[1][0]].pop(2)
			position[movement_list[1][1]][movement_list[1][0]].insert(2,[movement_list[0],movement_list[2]])

		#Chopping of wood
		N=[[1,0],[1,-1],[0,-1],[-1,-1],[-1,0],[-1,1],[0,1],[1,1]]
		for line in position:
			for node in line:
				if node != 0:
					if abs(node[0]) == 1 and node[2] != 0:
						if node[2][0] == 6 and position[node[2][1][1]][node[2][1][0]] != 0:
							if position[node[2][1][1]][node[2][1][0]][0] == 16:
								for n in N:
									if 0 <= node[2][1][0]+n[0] < len(position[0]) and 0 <= node[2][1][1]+n[1] < len(position):
										if node[1] == [node[2][1][0]+n[0],node[2][1][1]+n[1]]:
											HP = position[node[2][1][1]][node[2][1][0]][4][0]
											HP -= Features().workerChoppingSpeed()
											if HP < 0:
												HP = 0
											position[node[2][1][1]][node[2][1][0]][4][0] = HP

											PygAct().drawAction(screen,6,node,node[1],position[node[2][1][1]][node[2][1][0]],[node[2][1][0],node[2][1][1]])

											if HP > 0:
												playSound("villagerchop")
											else:
												target = list(position[node[2][1][1]][node[2][1][0]])
												position[node[2][1][1]][node[2][1][0]] = 0
												playSound("treefalls")
												falling_sq_n = list(N[int(random()*8)])
												if 0 <= node[2][1][1]+falling_sq_n[1] < len(position) and 0 <= node[2][1][0]+falling_sq_n[0] < len(position[0]):
													if position[node[2][1][1]+falling_sq_n[1]][node[2][1][0]+falling_sq_n[0]] != 0:
														if abs(position[node[2][1][1]+falling_sq_n[1]][node[2][1][0]+falling_sq_n[0]][0]) in [1,2,3,4,5,6,7,8,9,10,11,12,13,17]:
															HP = position[node[2][1][1]+falling_sq_n[1]][node[2][1][0]+falling_sq_n[0]][4].pop(0)
															HP -= Features().blockFallingDamage()
															if HP > 0:
																position[node[2][1][1]+falling_sq_n[1]][node[2][1][0]+falling_sq_n[0]][4].insert(0,HP)
															else:
																Commands().playDyingSound(abs(position[node[2][1][1]+falling_sq_n[1]][node[2][1][0]+falling_sq_n[0]][0]))
																position[node[2][1][1]+falling_sq_n[1]][node[2][1][0]+falling_sq_n[0]] = 0

												position = Commands().objectiveNoMoreReactions(position,target)

												break

		return movement_lists, position

	def gatherResources(self, screen, position):
		#TODO testaus voi olla puutteellista
		N = [[1,0],[1,-1],[0,-1],[-1,-1],[-1,0],[-1,1],[0,1],[1,1]]
		i=0
		for row in position:
			j=0
			for node in row:
				if type(node) is list:
					if abs(node[0]) == 1:
						if type(node[2]) is list:
							if node[2][0] == 2:
								resource_mined = False
								for n in N:
									if not resource_mined:
										if (0 <= i+n[1] < len(position)) and (0 <= j+n[0] < len(row)):
											if type(position[i+n[1]][j+n[0]]) is list:
												if 14 <= position[i+n[1]][j+n[0]][0] <= 15:
													resource_plant = position[i+n[1]][j+n[0]]

													surplus = 0

													carry = position[i][j].pop(10)

													resources = resource_plant[2].pop(0)
													if resources >= Features().workerGatheringSpeed():
														carry += Features().workerGatheringSpeed()
													else:
														carry += resources

													resource_mined = True
													playSound("villagermining")
													PygAct().drawAction(screen,2,node,node[1],resource_plant,resource_plant[1])

													if carry > Features().workerCarry():
														surplus = int(carry - Features().workerCarry())
														carry = Features().workerCarry()

													position[i][j].insert(10,carry)

													resources = int(resources - (Features().workerGatheringSpeed() - surplus))

													if resources > 0:
														resource_plant[2].insert(0,resources)
													else:
														#Resurssikentasta loppuivat resurssit
														position[i+n[1]][j+n[0]] = 0

				j+=1
			i+=1
		return position

	def stockUpResources(self, p1_resources, p2_resources, position):
		#TODO testaus voi olla puutteellista
		N = [[1,0],[1,-1],[0,-1],[-1,-1],[-1,0],[-1,1],[0,1],[1,1]]
		i=0
		for row in position:
			j=0
			for node in row:
				if type(node) is list:
					if 7 <= node[0] <= 8:
						for n in N:
							if (0 <= i+n[1] < len(position)) and (0 <= j+n[0] < len(row)):
								if type(position[i+n[1]][j+n[0]]) is list:
									if position[i+n[1]][j+n[0]][0] == 1:
										worker_resources = position[i+n[1]][j+n[0]].pop(10)
										p1_resources += worker_resources
										position[i+n[1]][j+n[0]].insert(10,0)

					elif -7 >= node[0] >= -8:
						for n in N:
							if (0 <= i+n[1] < len(position)) and (0 <= j+n[0] < len(row)):
								if type(position[i+n[1]][j+n[0]]) is list:
									if position[i+n[1]][j+n[0]][0] == -1:
										worker_resources = position[i+n[1]][j+n[0]].pop(10)
										p2_resources += worker_resources
										position[i+n[1]][j+n[0]].insert(10,0)

				j+=1
			i+=1

		return p1_resources, p2_resources, position

	def buildAndRepair(self, screen, p1_resources, p2_resources, position):
		N = [[1,0],[1,-1],[0,-1],[-1,-1],[-1,0],[-1,1],[0,1],[1,1]]
		i=0
		for line in position:
			j=0
			for node in line:
				if type(node) is list:
					if abs(node[0]) == 1:
						if type(node[2]) is list:
							build = False
							repair = False
							building_c = []
							if node[2][0] == 3:
								build = True
								for n in N:
									if 0 <= i+n[1] < len(position) and 0 <= j+n[0] < len(line):
										if type(position[i+n[1]][j+n[0]]) is list:
											if ((node[0] == 1 and (7 <= position[i+n[1]][j+n[0]][0] <= 13 or position[i+n[1]][j+n[0]][0] == 17)) or (node[0] == -1 and (-7 >= position[i+n[1]][j+n[0]][0] >= -13 or position[i+n[1]][j+n[0]][0] == -17))) and node[2][1] == [int(j+n[0]),int(i+n[1])] and position[i+n[1]][j+n[0]][9] == -1:
												building_c = [j+n[0],i+n[1]]

							elif node[2][0] == 4:
								repair = True
								for n in N:
									if 0 <= i+n[1] < len(position) and 0 <= j+n[0] < len(line):
										if type(position[i+n[1]][j+n[0]]) is list:
											if ((node[0] == 1 and (7 <= position[i+n[1]][j+n[0]][0] <= 13 or position[i+n[1]][j+n[0]][0] == 17)) or (node[0] == -1 and (-7 >= position[i+n[1]][j+n[0]][0] >= -13 or position[i+n[1]][j+n[0]][0] == -17))) and node[2][1] == [int(j+n[0]),int(i+n[1])] and position[i+n[1]][j+n[0]][4][0] < position[i+n[1]][j+n[0]][4][1]:
												building_c = [j+n[0],i+n[1]]

							if 3 <= node[2][0] <= 4 and building_c:
								target = position[building_c[1]][building_c[0]]
								HP_max = int(target[4][1])
								HP = target[4].pop(0)
								if node[2][0] == 3:
									if HP + int(Features().workerBuilderSpeed()) > HP_max:
										increment = int(HP_max - HP)
									else:
										increment = int(Features().workerBuilderSpeed())
								elif node[2][0] == 4:
									if HP + int(Features().workerRepairerSpeed()) > HP_max:
										increment = int(HP_max - HP)
									else:
										increment = int(Features().workerRepairerSpeed())
								if build:
									action = 3
									HP += increment
									if HP >= HP_max:
										target[9] = int(Commands().returnBuildingSpecial(target[0]))
										position = Commands().objectiveNoMoreReactions(position,target)
										playSound(Commands().getBuildingSound(abs(target[0])))
								elif repair:
									action = 4
									if node[0] == 1:
										while p1_resources < int((increment / (1.0 * int(Commands().getBuildingMaxHPByInt(int(target[0]))))) * int(target[3])):
											increment -= 1
										HP += increment
										if HP == Commands().getBuildingMaxHPByInt(int(target[0])):
											position = Commands().objectiveNoMoreReactions(position,target)
										p1_resources -= int((increment / (1.0 * int(Commands().getBuildingMaxHPByInt(int(target[0]))))) * int(target[3]))
									elif node[0] == -1:
										while p2_resources < int((increment / (1.0 * int(Commands().getBuildingMaxHPByInt(int(target[0]))))) * int(target[3])):
											increment -= 1
										HP += increment
										if HP == Commands().getBuildingMaxHPByInt(int(target[0])):
											position = Commands().objectiveNoMoreReactions(position,target)
										p2_resources -= int((increment / (1.0 * int(Commands().getBuildingMaxHPByInt(int(target[0]))))) * int(target[3])) #TODO vahentaa vaarin

								target[4].insert(0,int(HP))
								playSound("villagerbuildrepair")

								PygAct().drawAction(screen,action,node,node[1],target,target[1])

				j+=1
			i+=1

		return p1_resources, p2_resources, position

	def runEconomics(self, screen, p1_resources, p2_resources, position):
		economic_event_list = []

		position = self.gatherResources(screen, position)
		p1_resources, p2_resources, position = self.stockUpResources(p1_resources, p2_resources, position)

		p1_resources, p2_resources, position = self.buildAndRepair(screen, p1_resources, p2_resources, position)

		return economic_event_list, p1_resources, p2_resources, position






class GameEndGenerator():
	def __init__(self):
		pass
	def HQvictory(self,position):
		p1 = False
		p2 = False
		for row in position:
			for node in row:
				if type(node) is list:
					if node[0] == 7:
						if node[9] != -1:
							p1 = True
					elif node[0] == -7:
						if node[9] != -1:
							p2 = True
		if p1 and p2:
			return False
		elif p1 and not p2:
			return [1,1] #winner index, winning method
		elif p2 and not p1:
			return [-1,1]
		elif not p1 and not p2:
			return [0,1] #tie

	def runTests(self,position):
		end = self.HQvictory(position)
		return end
