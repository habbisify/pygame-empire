import math,time,pygame,os,ctypes

from startgenerator import *
from generator import *
from pathing import *
from commands import *
from textures import *
from messages import *
from pygameevent import *
from pygameactivity import *
from audio import *
from graphics import *

class Main:
	def __init__(self):
		pygame.mixer.pre_init(44100, -16, 2, 2048) # setup mixer to avoid sound lag
		pygame.init()
		user32 = ctypes.windll.user32
		self.screensize = user32.GetSystemMetrics(0), user32.GetSystemMetrics(1)

		self.position = []
		self.position_width = 19
		self.position_height = 10

		self.turn_timer = 0 #Turn time in seconds, 0 = no timer

		self.player1_type = 0 #0 = human, 1 = AI
		self.player2_type = 1
		self.player1_name = ""
		self.player2_name = ""
		self.player1_resources = 50
		self.player2_resources = 55
		self.player1_score = 0
		self.player2_score = 0
		self.player1_strategy = 1
		self.player2_strategy = 1

		self.turn = 0

		self.training_queue = [[],[]] #Molemmille pelaajille jono
		self.all_movement_list = []
		self.events_log = []

		self.cheats = True
	def gameStart(self):
		#playMusic(0)
		Textures().printLogo2()
		#time.sleep(1.0)
		player1_type = raw_input("Player 1 type (leave empty = human, 1 = AI): ")
		if player1_type == "1":
			player1_type = 1
		else:
			player1_type = 0
		self.player1_type = int(player1_type)
		self.player1_name = raw_input("Player 1 name (leave empty for random): ")
		player2_type = raw_input("Player 2 type (leave empty = human, 1 = AI): ")
		if player2_type == "1":
			player2_type = 1
		else:
			player2_type = 0
		self.player2_type = int(player2_type)
		self.player2_name = raw_input("Player 2 name (leave empty for random): ")
		self.player1_name, self.player2_name = StartGenerator().setDefaultNames(self.player1_name, self.player2_name)
		Messages().initPlayerNames(self.player1_name,self.player2_name)

		resource_p = raw_input("Prevalence of resources (1-10, leave empty for random): ")
		if resource_p.isdigit() and int(resource_p) in range(10):
			resource_p = int(resource_p)
		else:
			resource_p = 0

		block_p = raw_input("Prevalence of blocks (1-10, leave empty for random): ")
		if block_p.isdigit() and int(block_p) in range(10):
			block_p = int(block_p)
		else:
			block_p = 0

		self.position = StartGenerator().generateMap(self.position_width,self.position_height,int(resource_p),int(block_p))	#resources, blocks
		self.position = EconomicGenerator().generateAutomaticResourceGatherings(1, self.position)
		self.position = EconomicGenerator().generateAutomaticResourceGatherings(-1, self.position)
		start_res_multiplier = raw_input("Starting resources (leave empty for default, otherwise input multiplier): ")
		if start_res_multiplier.isdigit():
			self.player1_resources *= int(start_res_multiplier)
			self.player2_resources *= int(start_res_multiplier)

		self.gameMain()

	def gameMain(self):
		'''Pygame attributes
		'''



		self.screen = pygame.display.set_mode([len(self.position[0])*40,20+len(self.position)*40+126],pygame.FULLSCREEN) #self.screensize,pygame.FULLSCREEN



		clock = pygame.time.Clock()

		time_passed = 0

		playMusic(1)

		mb1down_x = 0
		mb1down_y = 0
		mb1down_pos = [mb1down_x,mb1down_y]
		mb3down_x = 0
		mb3down_y = 0
		mb3down_pos = [mb3down_x,mb3down_y]
		mb3up_x = 0
		mb3up_y = 0
		mb3up_pos = [mb3up_x,mb3up_y]
		ctrldown = False

		action = 0
		sfx = False
		sq_query = False
		selected_objects = []
		remove_from_selected_objects = False
		produce_unit = [0,0]
		constructable_building = 0
		build_info = [0,[0,0],[0,0]]
		building_view1 = False
		building_view2 = False
		object = [999] #Random big number

		game_loop = True

		while game_loop:

			'''Pygame code
			'''
			clock.tick(10) #40 is preferred, but for testing purposes 10 is good to see the printable events etc.
			time_passed += clock.get_time()
			if self.turn_timer:
				time_left = self.turn_timer*1000 - time_passed
			else:
				time_left = 1000 + time_passed

			self.player1_score = ScoreGenerator().generateScore(1,self.player1_resources,self.position)
			self.player2_score = ScoreGenerator().generateScore(2,self.player2_resources,self.position)

			columns,lines = PygAct().defaultGUI(self.screen,time_left,self.position,self.player1_name,self.player2_name,self.player1_resources,self.player2_resources,self.player1_score,self.player2_score,self.turn)

			#Textures().printPosition(self.position, self.player1_name, self.player2_name, self.player1_resources, self.player2_resources, self.player1_score, self.player2_score)
			player_in_turn = self.returnPlayerInTurn()
			#print player_in_turn + ":s turn" + ( 67-len(player_in_turn)-len(str(int((self.turn+2)/2))) ) *" " + "Turn: " + str(int((self.turn+2)/2))





			#Kaikkien tapahtumien kasittelija
			game_loop,action,self.player1_resources,self.player2_resources,sq_query,selected_objects,remove_from_selected_objects,produce_unit,constructable_building,build_info,building_view1,building_view2,object,lines,sfx,ctrldown,mb1down_pos,mb1down_x,mb1down_y,mb3down_pos,mb3down_x,mb3down_y,mb3up_pos,mb3up_x,mb3up_y,destination = \
			PygEvent().eventHandler(
			self.screen,time_left,game_loop,action,self.position,self.turn,self.player1_type,self.player2_type,self.player1_name,self.player2_name,self.player1_resources,self.player2_resources,self.training_queue,sq_query,selected_objects,remove_from_selected_objects,produce_unit,constructable_building,build_info,building_view1,building_view2,object,lines,sfx,ctrldown,mb1down_pos,mb1down_x,mb1down_y,mb3down_pos,mb3down_x,mb3down_y,mb3up_pos,mb3up_x,mb3up_y
			)





			pygame.display.flip()

			if action:
				ask = False
				if action not in [3,4]:
					sq_query = False

				Textures().printPosition(self.position, self.player1_name, self.player2_name, self.player1_resources, self.player2_resources, self.player1_score, self.player2_score)
				player_in_turn = self.returnPlayerInTurn()
				print player_in_turn + ":s turn" + ( 67-len(player_in_turn)-len(str(int((self.turn+2)/2))) ) *" " + "Turn: " + str(int((self.turn+2)/2))

				'''move unit to a square
				'''
				if action == 2:
					ask = True
					for obj in selected_objects:
						keep_on = self.moveAUnit([int(obj[1][0]),int(obj[1][1])],[int(mb3up_x),int(mb3up_y)])
						if not keep_on:
							ask = False
					if not ask:
						PygAct().waitingScreen(self.screen,columns,lines)
						pygame.display.flip()

				'''move unit to a square - AI
				'''
				if action == -2:
					ask = True
					for obj in selected_objects:
						keep_on = self.moveAUnit([int(obj[1][0]),int(obj[1][1])],[destination[0],destination[1]])
						if not keep_on:
							ask = False
					if not ask:
						PygAct().waitingScreen(self.screen,columns,lines)
						pygame.display.flip()

				'''order all unemployed workers to gather resources
				'''
				if action == "R":
					self.position = EconomicGenerator().generateAutomaticResourceGatherings(int((self.turn%2)*-2+1),self.position)
					ask = False

				'''train unit(s)
				'''
				if action == 3:
					self.trainUnits(action,produce_unit)
					ask = True

				'''build a structure
				'''

				if action == 4:
					ask = self.buildStructure(build_info)

				'''skip turn
				'''
				if action == 1:
					PygAct().waitingScreen(self.screen,columns,lines)
					pygame.display.flip()
					ask = False

				'''resign
				'''
				'''if action is "r":
					print "Player " + player_in_turn + " resigns!"
					ask = False
					game_loop = False'''

				'''show events
				'''
				if action == "eventslog":
					Messages().printEventsLog(self.events_log)

				'''unrecgnized action
				'''
				#else:
				#	print Messages().unrecognizedAction()

				'''cheats
				'''
				if self.cheats:
					if action == "ScroogeMcDuck":
						if self.turn % 2 == 0:
							self.player1_resources += int(Features().cheatScroogeMcDuckAmount())
						else:
							self.player2_resources += int(Features().cheatScroogeMcDuckAmount())

				if not ask:

					sleep(0.3)
					columns,lines = PygAct().defaultGUI(self.screen,time_left,self.position,self.player1_name,self.player2_name,self.player1_resources,self.player2_resources,self.player1_score,self.player2_score,self.turn)



					#Pelimoottori
					win = self.gameEngineProcess()



					if type(win) is list:
						if win[0] == 1:
							win_player = self.player1_name
						else:
							win_player = self.player2_name
						print "Player " + win_player + " wins by " + Messages().returnWinningMethodStr(win[1]) + "!"
						PygAct().winningScreen(self.screen,columns,lines,win_player,Messages().returnWinningMethodStr(win[1]))
						pygame.display.flip()
						playSound("win")
						sleep(4.0)
						ask = False
						game_loop = False

					selected_objects = []

					time_passed = 0
					self.turn += 1

				action = 0

	def returnPlayerInTurn(self):
		if self.turn % 2 == 0:
			player_in_turn = self.player1_name
		else:
			player_in_turn = self.player2_name
		return player_in_turn
	def returnPlayerInTurnIndex(self):
		if self.turn%2==0:
			player_in_turn_id = 1
		else:
			player_in_turn_id = -1
		return player_in_turn_id


	def showSquareDetails(self, action):
		unit = Commands().selectUnit(self.position, action)
		Textures().questionUnit(unit, self.player1_name, self.player2_name)
		time.sleep(3.0)

	def trainUnits(self,action,produce_unit):
		player_in_turn_id = self.returnPlayerInTurnIndex()

		if action == "ClearQueue":
			deleted_queue = self.training_queue.pop(abs(player_in_turn_id-1)/2)
			self.training_queue.insert(abs(player_in_turn_id-1)/2,[])
			for order in deleted_queue:
				if player_in_turn_id == 1:
					self.player1_resources += order[1]*int(Commands().getUnitPriceByInt(order[0]))
				else:
					self.player2_resources += order[1]*int(Commands().getUnitPriceByInt(order[0]))
			return True

		amount = produce_unit[2]

		unit0 = produce_unit[0]
		unit_OK = True
		if not Commands().returnIfUnitPossible(self.position,unit0*self.returnPlayerInTurnIndex()):
			unit_OK = False

		unit = int(player_in_turn_id*unit0)

		if unit_OK:
			if amount >= 0:
				if player_in_turn_id == 1:
					while self.player1_resources < amount*int(Commands().getUnitPriceByInt(unit)):
						amount-=1
						if amount == 0:
							Messages().printUnitNotResources()
					self.player1_resources-=amount*int(Commands().getUnitPriceByInt(unit))
				else:
					while self.player2_resources < amount*int(Commands().getUnitPriceByInt(unit)):
						amount-=1
						if amount == 0:
							Messages().printUnitNotResources()
					self.player2_resources-=amount*int(Commands().getUnitPriceByInt(unit))

			no_order_equals_old_unit = True
			i=0
			for player_queue in self.training_queue:
				j=0
				for order in player_queue:
					if order[0] == unit and order[2] == produce_unit[1]:
						old_amount = order.pop(1)
						order.insert(1,old_amount+amount)
						if amount < 0:
							if old_amount <= amount:
								amount = int(old_amount)
								order = "trash"
							if old_amount > 0:
								if player_in_turn_id == 1:
									self.player1_resources+=abs(amount)*Commands().getUnitPriceByInt(unit)
								else:
									self.player2_resources+=abs(amount)*Commands().getUnitPriceByInt(unit)
							else:
								self.training_queue[int(i)][int(j)] = "trash"
						no_order_equals_old_unit = False
					j+=1
				while player_queue.count("trash"):
					player_queue.remove("trash")
				i+=1
			if no_order_equals_old_unit and unit > 0 and amount >= 0:
				self.training_queue[0].append([unit, amount, produce_unit[1], 0.0])
			elif no_order_equals_old_unit and unit < 0 and amount >= 0:
				self.training_queue[1].append([unit, amount, produce_unit[1], 0.0])

		return False

	def buildStructure(self,build_info):

		if self.returnPlayerInTurnIndex() == 1:
			if self.position[build_info[1][1]][build_info[1][0]] == 0 and self.player1_resources >= Commands().getBuildingPriceByInt(build_info[0]):
				self.position[build_info[1][1]][build_info[1][0]] = Commands().insertBuildingUnderConstructionFeatures(ObjectGenerator().generateByInt(self.returnPlayerInTurnIndex(),build_info[1],build_info[0]))
				self.player1_resources -= Commands().getBuildingPriceByInt(build_info[0])
			else:
				if self.position[build_info[1][1]][build_info[1][0]] != 0:
					Messages().printSquareNotEmpty()
				if self.player1_resources < Commands().getBuildingPriceByInt(build_info[0]):
					Messages().printBuildingNotResources()
				playSound("impossible")
				return True

		else:
			if self.position[build_info[1][1]][build_info[1][0]] == 0 and self.player2_resources >= Commands().getBuildingPriceByInt(build_info[0]):
				self.position[build_info[1][1]][build_info[1][0]] = Commands().insertBuildingUnderConstructionFeatures(ObjectGenerator().generateByInt(self.returnPlayerInTurnIndex(),build_info[1],build_info[0]))
				self.player2_resources -= Commands().getBuildingPriceByInt(build_info[0])
			else:
				if self.position[build_info[1][1]][build_info[1][0]] != 0:
					Messages().printSquareNotEmpty()
				if self.player2_resources < Commands().getBuildingPriceByInt(build_info[0]):
					Messages().printBuildingNotResources()
				playSound("impossible")
				return True

		for worker in build_info[2]:
			action, self.position = Commands().defineActionStatus(self.turn, worker, build_info[1], self.position)
			self.all_movement_list.insert(0,[int(action), list(worker), list(build_info[1])])

		return False

	def moveAUnit(self, pre_coordinates, post_coordinates):
		unit = self.position[pre_coordinates[1]][pre_coordinates[0]][0]
		player = self.returnPlayerInTurnIndex()
		if 1 <= abs(unit) <= 6:
			if (unit > 0 and player == 1) or (unit < 0 and player == -1):
				action, self.position = Commands().defineActionStatus(self.turn, pre_coordinates, post_coordinates, self.position)
				self.all_movement_list.insert(0,[action, pre_coordinates, post_coordinates])

				PygAct().orderVisualAndAudio(self.screen,action,unit,post_coordinates,audio=1,vivid=1)

				return False #ask = False
			else:
				print "You need a wololoo-priest to legally order your opponent's troops to move!"
		else:
			print "You can't order non-human beings to move!"
		return True



	def gameEngineProcess(self):

		training_event_list, self.training_queue, self.position = TrainingGenerator().trainUnits(self.screen,self.training_queue, self.position)
		#print self.training_queue

		movement_lists, self.position = EconomicGenerator().automaticEconomicMovement(self.screen,self.position)
		for movement_list in movement_lists:
			self.all_movement_list.insert(0,movement_list)

		#print self.all_movement_list
		self.all_movement_list, movement_event_list, self.position = PathGenerator2().runPaths(self.screen, self.all_movement_list, self.position)
		#print "all_movement_list",self.all_movement_list,"\nevent_list",movement_event_list #movement_list syntax: [action (1=movement,2=gather resources etc.), [A(x),A(y)], [C(x),C(y)]]

		combat_event_list, self.position = CombatGenerator().createCombats(self.screen,self.player1_name,self.player2_name,self.position)

		economic_event_list, self.player1_resources, self.player2_resources, self.position = EconomicGenerator().runEconomics(self.screen, self.player1_resources, self.player2_resources, self.position)

		end = GameEndGenerator().runTests(self.position)
		if type(end) is list:
			return end

		self.events_log.append([int(self.turn%2),training_event_list,movement_event_list,combat_event_list,economic_event_list])

		return False
