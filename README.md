This project is a two-player turn-based strategy game, based on the Age of 
Empires series.
It includes a GUI and AI to play against.
Most of the graphics and sound are loaned from Age of Empires I.

Start the program by running PygameEmpire.py. Run it with Python 2.7, with 
compatible Pygame installed. Version pygame-1.9.2a0.win32-py2.7.msi should 
work and it can be found in here http://www.pygame.org/download.shtml

I've implemented 100% of the code in the files, mainly during a time-span of 
2014-2016 (started during my high school studies). I programmed a Dijkstra 
pathfinding algorithm without ever having heard of that, I implemented an AI 
that sometimes even outplayed myself, and I created a very pleasant user 
interface. I decided to upload this project to Git even though the code is 
ugly, since it's a huge project for a single person to handle, and I would say 
an accomplishment for a beginner.

My main principle during developing this game was not readability of the code 
but coding feature after feature as quickly as possible without breaking 
anything. As long as I made progress, I was happy. When the technical debt 
became too great to bear, I applied some good coding practises here and there 
to manage the situation.

The current (and probably the final) version of the program introduces quite a 
lot of anti-patterns and no-nos from the perspective of code quality and 
understandability, and there are bugs and missing features. But the fact that 
the game works as well as it does is an enormous feat in my opinion and proves 
that dedication is the most important element to get things done.

Try beating the AI with default settings, it shouldn't be too easy!
Instructions on how to play the game are on "instructions.txt".