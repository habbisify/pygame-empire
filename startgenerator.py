from random import random,choice
from time import sleep

from commands import *
from features import *
from messages import *

class StartGenerator:
	def __init__(self):
		self.map = []
		self.hq1_position = []
		self.hq2_position = []
		self.resource_positions = []
	def setDefaultNames(self, name1, name2):
		p_names = Messages().returnPNames()
		if name1 == "":
			name1 = str(choice(p_names))
		if name2 == "":
			name2 = str(choice(p_names))
		#while name1 == name2:
		#	name2 = str(choice(p_names))
		return name1, name2

	def generateMap(self,x,y,resource_p,block_p):

		if resource_p == 0:
			resource_p = random()*10+1
		if block_p == 0:
			block_p = random()*10+1

		good = False
		while not good:

			self.map = []
			line = []
			for i in range(x):
				line.append(0)
			for i in range(y):
				self.map.append(list(line))
			self.createHQs()
			self.createWorkers()
			self.createHQResources()
			self.createResources(resource_p)
			self.createBlocks(block_p)
			good = self.checkIfGood(resource_p,block_p)

		return self.map

	def createHQs(self):
		y_r1 = float(random())
		y_r2 = float(choice([y_r1,1.0-y_r1]))
		x_r = float(random())
		player1_headquarters_y = int(1.0*(len(self.map)-2)*y_r1+1)
		player1_headquarters_x = int(3.0*x_r+1)

		player2_headquarters_y = int(1.0*(len(self.map)-2)*y_r2+1)
		player2_headquarters_x = int(len(self.map[0])-1-3.0*x_r)

		self.hq1_position = [player1_headquarters_x, player1_headquarters_y]
		self.hq2_position = [player2_headquarters_x, player2_headquarters_y]

		self.map[player1_headquarters_y][player1_headquarters_x] = ObjectGenerator().HQ(1,[player1_headquarters_x,player1_headquarters_y])
		self.map[player2_headquarters_y][player2_headquarters_x] = ObjectGenerator().HQ(-1,[player2_headquarters_x,player2_headquarters_y])

	def createWorkers(self):
		worker1_position_x = self.hq1_position[0]+1
		worker1_position_y = self.hq1_position[1]
		worker2_position_x = self.hq1_position[0]+1
		worker2_position_y = self.hq1_position[1]+1
		worker3_position_x = self.hq1_position[0]+1
		worker3_position_y = self.hq1_position[1]-1
		worker4_position_x = self.hq2_position[0]-1
		worker4_position_y = self.hq2_position[1]
		worker5_position_x = self.hq2_position[0]-1
		worker5_position_y = self.hq2_position[1]+1
		worker6_position_x = self.hq2_position[0]-1
		worker6_position_y = self.hq2_position[1]-1

		self.map[worker1_position_y][worker1_position_x] = ObjectGenerator().worker(1,[worker1_position_x,worker1_position_y])
		self.map[worker2_position_y][worker2_position_x] = ObjectGenerator().worker(1,[worker2_position_x,worker2_position_y])
		self.map[worker3_position_y][worker3_position_x] = ObjectGenerator().worker(1,[worker3_position_x,worker3_position_y])
		self.map[worker4_position_y][worker4_position_x] = ObjectGenerator().worker(-1,[worker4_position_x,worker4_position_y])
		self.map[worker5_position_y][worker5_position_x] = ObjectGenerator().worker(-1,[worker5_position_x,worker5_position_y])
		self.map[worker6_position_y][worker6_position_x] = ObjectGenerator().worker(-1,[worker6_position_x,worker6_position_y])

	def createHQResources(self):
		resource1_x = self.hq1_position[0]+3
		resource1_y = self.hq1_position[1]
		resource2_x = self.hq2_position[0]-3
		resource2_y = self.hq2_position[1]

		self.map[resource1_y][resource1_x] = ObjectGenerator().resourceplant([resource1_x,resource1_y])
		self.map[resource2_y][resource2_x] = ObjectGenerator().resourceplant([resource2_x,resource2_y])

	def createResources(self,resource_p):
		a = -1
		b = -1
		for i in self.map:
			a+=1
			for j in i:
				b+=1
				if (((b < 5 or b > len(self.map[0])-5) and random() > 0.97-resource_p/100.0) or (random() > 1.0-resource_p/100.0)) and (abs(a - self.hq1_position[1]) > 2 or abs(b - self.hq1_position[0]) > 2) and (abs(a - self.hq2_position[1]) > 2 or abs(b - self.hq2_position[0]) > 2):
					if random() > 0.8:
						self.map[a][b] = ObjectGenerator().hugeresourceplant([b,a])
					else:
						self.map[a][b] = ObjectGenerator().resourceplant([b,a])
					self.resource_positions.append([b,a])
			b=-1

	def createBlocks(self,block_p):
		a = -1
		b = -1
		for i in self.map:
			a+=1
			for j in i:
				b+=1
				if ((b > 5 and b < len(self.map[0])-5 and random() > 0.98-block_p/80.0) or (random() > 1.0-block_p/80.0)) and (abs(a - self.hq1_position[1]) > 1 or abs(b - self.hq1_position[0]) > 1) and (abs(a - self.hq2_position[1]) > 1 or abs(b - self.hq2_position[0]) > 1) and (abs(a - self.hq1_position[1]) > 1 or abs(b - (self.hq1_position[0]+3)) > 1) and (abs(a - self.hq2_position[1]) > 1 or abs(b - (self.hq2_position[0]-3)) > 1) and j == 0:
					self.map[a][b] = ObjectGenerator().block([b,a])
			b=-1

	def checkIfGood(self,resource_p,block_p):

		hq1p = self.hq1_position
		hq2p = self.hq2_position
		r_amount = 0
		r_distance1 = 0
		r_distance2 = 0
		b_distance1 = 0
		b_distance2 = 0

		j=0
		for line in self.map:
			i=0
			for node in line:
				if type(node) is list:
					if node[0] == 14:
						r_distance1 += abs(i-hq1p[0])+abs(j-hq1p[1])
						r_distance2 += abs(i-hq2p[0])+abs(j-hq2p[1])
						r_amount += 300
					elif node[0] == 15:
						r_distance1 += int(abs(i-hq1p[0])+abs(j-hq1p[1])*1.5)
						r_distance2 += int(abs(i-hq2p[0])+abs(j-hq2p[1])*1.5)
						r_amount += 500
					elif node[0] == 16:
						b_distance1 += abs(i-hq1p[0])+abs(j-hq1p[1])
						b_distance2 += abs(i-hq2p[0])+abs(j-hq2p[1])
				i+=1
			j+=1

		if r_amount < 2400 or abs(r_distance1-r_distance2) > 3+resource_p*1.2 or abs(b_distance1-b_distance2) > 5+block_p*1.5:
			return False
		return True

class ObjectGenerator():
	def __init__(self):
		pass
	def generateByInt(self, side, location, number):
		if abs(number) == 1:
			return self.worker(side, location)
		elif abs(number) == 2:
			return self.swordman(side,location)
		elif abs(number) == 3:
			return self.eliteswordman(side,location)
		elif abs(number) == 4:
			return self.archer(side,location)
		elif abs(number) == 5:
			return self.knight(side,location)
		elif abs(number) == 6:
			return self.catapult(side,location)
		elif abs(number) == 7:
			return self.HQ(side,location)
		elif abs(number) == 8:
			return self.resourcecamp(side,location)
		elif abs(number) == 9:
			return self.barracks(side,location)
		elif abs(number) == 10:
			return self.archeryrange(side,location)
		elif abs(number) == 11:
			return self.stable(side,location)
		elif abs(number) == 17:
			return self.siegeworkshop(side,location)
		elif abs(number) == 12:
			return self.watchtower(side,location)
		elif abs(number) == 13:
			return self.castle(side,location)
		elif abs(number) == 14:
			return self.resourceplant(side,location)
		elif abs(number) == 15:
			return self.hugeresourceplant(side,location)

	def worker(self, side, location):
		return [1*side,[location[0],location[1]],0,Features().workerPrice(),[Features().workerHitpoints(),Features().workerHitpoints()], Features().workerMovement(), Features().workerAttack(), Features().workerDefence(), Features().workerRange(), Features().workerSpecial(), 0]
	def swordman(self, side, location):
		return [2*side,[location[0],location[1]],0,Features().swordmanPrice(),[Features().swordmanHitpoints(),Features().swordmanHitpoints()], Features().swordmanMovement(), Features().swordmanAttack(), Features().swordmanDefence(), Features().swordmanRange(), Features().swordmanSpecial()]
	def eliteswordman(self, side, location):
		return [3*side,[location[0],location[1]],0,Features().eliteswordmanPrice(),[Features().eliteswordmanHitpoints(),Features().eliteswordmanHitpoints()], Features().eliteswordmanMovement(), Features().eliteswordmanAttack(), Features().eliteswordmanDefence(), Features().eliteswordmanRange(), Features().eliteswordmanSpecial()]
	def archer(self, side, location):
		return [4*side,[location[0],location[1]],0,Features().archerPrice(),[Features().archerHitpoints(),Features().archerHitpoints()], Features().archerMovement(), Features().archerAttack(), Features().archerDefence(), Features().archerRange(), Features().archerSpecial()]
	def knight(self, side, location):
		return [5*side,[location[0],location[1]],0,Features().knightPrice(),[Features().knightHitpoints(),Features().knightHitpoints()], Features().knightMovement(), Features().knightAttack(), Features().knightDefence(), Features().knightRange(), Features().knightSpecial()]
	def catapult(self, side, location):
		return [6*side,[location[0],location[1]],0,Features().catapultPrice(),[Features().catapultHitpoints(),Features().catapultHitpoints()], Features().catapultMovement(), Features().catapultAttack(), Features().catapultDefence(), Features().catapultRange(), Features().catapultSpecial(),1]
	def HQ(self, side, location):
		return [7*side,[location[0],location[1]],0,Features().HQPrice(),[Features().HQHitpoints(),Features().HQHitpoints()], 0, 0, Features().HQDefence(), 0, Features().HQSpecial()]
	def resourcecamp(self, side, location):
		return [8*side,[location[0],location[1]],0,Features().resourcecampPrice(),[Features().resourcecampHitpoints(),Features().resourcecampHitpoints()], 0, 0, Features().resourcecampDefence(), 0, Features().resourcecampSpecial()]
	def barracks(self, side, location):
		return [9*side,[location[0],location[1]],0,Features().barracksPrice(),[Features().barracksHitpoints(),Features().barracksHitpoints()], 0, 0, Features().barracksDefence(), 0, Features().barracksSpecial()]
	def archeryrange(self, side, location):
		return [10*side,[location[0],location[1]],0,Features().archeryrangePrice(),[Features().archeryrangeHitpoints(),Features().archeryrangeHitpoints()], 0, 0, Features().archeryrangeDefence(), 0, Features().archeryrangeSpecial()]
	def stable(self, side, location):
		return [11*side,[location[0],location[1]],0,Features().stablePrice(),[Features().stableHitpoints(),Features().stableHitpoints()], 0, 0, Features().stableDefence(), 0, Features().stableSpecial()]
	def siegeworkshop(self, side, location):
		return [17*side,[location[0],location[1]],0,Features().siegeworkshopPrice(),[Features().siegeworkshopHitpoints(),Features().siegeworkshopHitpoints()], 0, 0, Features().siegeworkshopDefence(), 0, Features().siegeworkshopSpecial()]
	def watchtower(self, side, location):
		return [12*side,[location[0],location[1]],0,Features().watchtowerPrice(),[Features().watchtowerHitpoints(),Features().watchtowerHitpoints()], 0, Features().watchtowerAttack(), Features().watchtowerDefence(), Features().watchtowerRange(), Features().watchtowerSpecial()]
	def castle(self, side, location):
		return [13*side,[location[0],location[1]],0,Features().castlePrice(),[Features().castleHitpoints(),Features().castleHitpoints()], 0, Features().castleAttack(), Features().castleDefence(), Features().castleRange(), Features().castleSpecial()]
	def resourceplant(self, location):
		return [14, [location[0],location[1]],[Features().resourceplantContain(),Features().resourceplantContain()]]
	def hugeresourceplant(self, location):
		return [15, [location[0],location[1]],[Features().hugeresourceplantContain(),Features().hugeresourceplantContain()]]
	def block(self,location):
		return [16, [location[0],location[1]],0,0,[Features().blockHitpoints(),Features().blockHitpoints()],int(random()*8)]

	#UNITS return [*side,[location[0],location[1]], 0, [Features().Hitpoints(),Features().Hitpoints()], Features().Movement(), Features().Attack(), Features().Defence(), Features().Range(), Features().Special()]
	#BUILDINGS return [*side,[location[0],location[1]], 0, [Features().Hitpoints(),Features().Hitpoints()], 0, 0, Features().Defence(), 0, Features().Special()]

	#X[2] = action, syntax: [action_index,[x,y]]
