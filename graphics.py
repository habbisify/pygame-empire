import os, sys, pygame

def loadImage(name, colorkey=None):
	fullname = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__))) + "\\img_data\\" + name + ".png"
	image = pygame.image.load(fullname)
	return image, image.get_rect()
	
def colours(cn):
	try:
		if cn == "black":
			colour = [0,0,0]
		elif cn == "gray":
			colour = [123,123,123]
		elif cn == "silver":
			colour = [191,191,191]
		elif cn == "white":
			colour = [255,255,255]
		elif cn == "red":
			colour = [255,0,0]
		elif cn == "tomato":
			colour = [255,99,71]
		elif cn == "maroon":
			colour = [123,0,0]
		elif cn == "firebrick":
			colour = [178,34,34]
		elif cn == "yellow":
			colour = [255,255,0]
		elif cn == "lightyellow3":
			colour = [255,255,102]
		elif cn == "olive":
			colour = [123,123,0]
		elif cn == "lime":
			colour = [0,255,0]
		elif cn == "green":
			colour = [0,123,0]
		elif cn == "brightgreen":
			colour = [102,255,0]
		elif cn == "lightgreen":
			colour = [144,238,144]
		elif cn == "aqua":
			colour = [0,255,255]
		elif cn == "teal":
			colour = [0,123,123]
		elif cn == "blue":
			colour = [0,0,123]
		elif cn == "lightblue":
			colour = [173,216,230]
		elif cn == "fuchsia":
			colour = [255,0,255]
		elif cn == "purple":
			colour = [123,0,123]
		elif cn == "orchid":
			colour = [218,112,214]
		else:
			raise NameError
	except NameError as UnidentifiedColour:
		print ("Unidentified colour; " + str(cn) + " is not a string of a used colour.")
		sys.exit()

	return colour