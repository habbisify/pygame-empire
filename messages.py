import time
from features import *
from random import choice
from audio import playSound

class Messages:
	def __init__(self):
		self.player1_name = ""
		self.player2_name = ""
	def initPlayerNames(self,p1,p2): #Ei toimi :(
		self.player1_name = str(p1)
		self.player2_name = str(p2)
	
	def returnPNames(self):
		p_names1 = [
		"William",
		"Henry",
		"Richard",
		"Edward",
		"Harold",
		"Theuderic",
		"Charlemagne",
		"Charles",
		"Louis",
		"Philip",
		"Lothair",
		"Frederic",
		"Sigismund",
		"Pedro",
		"Robert",
		"Francis",
		"Ferdinand",
		"Joseph",
		"Isabella",
		"Alfonso",
		"Felipe",
		"Guimond",
		"Radolf",
		"Alainon",
		"Iohannes",
		"Fouquet",
		"Gabryell",
		"Beneger",
		"Tristan",
		"Azorius",
		"Guillemot",
		"Yvon",
		"Herman",
		"Tierri",
		"Roget",
		"Michelet",
		"Jaquelinne",
		"Bethia",
		"Griselda",
		"Alicia",
		"Bathia",
		"Christiania",
		"Thyphainne"
		]
		p_names2 = ["I","II","III","IV","V","VI","VII","VIII"]
		p_names3 = [
		"Great",
		"Medic",
		"Kind",
		"Mage",
		"Unfortunate",
		"Round",
		"Fat",
		"Bull",
		"Acrobat",
		"Exalted",
		"Unlucky",
		"Gentle Heart",
		"Good",
		"Illustious",
		"Disguised",
		"Vengeful",
		"Victorious",
		"Peaceful",
		"Warrior",
		"Loyal",
		"Lionroar",
		"Blind",
		"Infamous",
		"Majestic",
		"Cautious",
		"Sleepy",
		"Sophisticated",
		"Brave",
		"Reliable",
		"Lion",
		"Immortal",
		"Hound",
		"Thoughtful",
		"Eager",
		"Careless",
		"Wolf",
		"Loyal Heart",
		"Humble",
		"Rich",
		"Zealous",
		"Idealist",
		"Fearless",
		"Master",
		"Thirsty",
		"Strong",
		"Enforcer",
		"Reckless",
		"Terrible",
		"Corruptor",
		"Wreckage",
		"Greedy",
		"Dark Lord",
		"Fury",
		"Crafty",
		"Witch",
		"Anguished",
		"Shameless",
		"Stupid",
		"Defiant",
		"Dreary",
		"Broken",
		"Damned",
		"Halfman",
		"Chicken",
		"Demon",
		"Worthless",
		"Nightowl",
		"Dangerous",
		"Warlock",
		"Merciful",
		"Vulture",
		"Failure",
		"Insecure",
		"Cursed",
		"Black Knight",
		"Dark",
		"Lazy",
		"Harsh",
		"Hermit",
		"Bastard"
		]
		p_names = []
		for name in p_names1:
			p_name = str(str(name)+" "+str(choice(p_names2))+" the "+str(choice(p_names3)))
			if len(p_name) < 26:
				p_names.append(p_name)
		return p_names
	
	def introduction(self):
		ask = True
		while ask:
			print "Press enter to continue..."
			action = raw_input()
			if action is "":
				ask = False
	def commands(self):
		print ""
	
	def strategy(self, number):
		if number == 1:
			return "Agressive - all unordered troops march to their fighting range of an enemy, if one lies within five squares of whom"
		elif number == 2:
			return "Defensive - all unordered troops march to their fighting range of an enemy, if one lies within it's fighting range of whom or one lies within two squares of whom"
		elif number == 3:
			return "Hold ground - all unordered troops keep their position"
	def strategyLite(self, number):
		if number == 1:
			return "Agressive"
		elif number == 2:
			return "Defensive"
		elif number == 3:
			return "Hold"
	
	def order(self, number):
		if number == 0:
			return "None"
		elif number[0] == 1:
			return "Moving towards " + str(self.coordinatesToReadable(number[1]))
		elif number[0] == 2:
			return "Collecting resources"
		elif number[0] == 3:
			return "Constructing a building to " + str(self.coordinatesToReadable(number[1]))
		elif number[0] == 4:
			return "Repairing a building at " + str(self.coordinatesToReadable(number[1]))
		elif number[0] == 5:
			return "Chasing the enemy target" #TODO
	def special(self, number):
		if number == 0:
			return "None"
		elif number == -1:
			return "Building not finished."
		elif number == 1:
			return "Able to collect resources and build structures"
		elif number == 2:
			return "Attack +" + str(Features().swordmanABvsN) + " vs knight"
		elif number == 3:
			return "Attack +" + str(Features().eliteswordmanABvsN) + " vs knight"
		elif number == 8:
			return "May store resources"
		return ""
		#TODO jatka loppuun

	def printEventsLog(self,events_log):
		print "Events log:"
		for event_set in events_log:
			if event_set[0]%2==0:
				print "Turn " + str(int(event_set[0]/2)) + " (" + self.player1_name + "):"
			else:
				print "Turn " + str(int(event_set[0]/2)) + " (" + self.player2_name + "):"
			for event in event_set[1]:
				print str(event)
			time.sleep(1.0)
		time.sleep(3.0)
		
	def unrecognizedAction(self):
		print "Unrecognized action. Type i for introduction and c for commands."
	
	def createMovementInProgressEvent(self,movement_list,path,new_coordinates,unit):
		return str(unit) + " moves from " + self.coordinatesToReadable(path[0]) + " to " + self.coordinatesToReadable(new_coordinates) + " aiming at " + self.coordinatesToReadable(path[-1]) + self.movementInProgressNotification(movement_list[0])
	
	def createMovementCompleteEvent(self,movement_list,path,new_coordinates,unit):
		return str(unit) + " moves from " + self.coordinatesToReadable(path[0]) + " to " + self.coordinatesToReadable(new_coordinates) + self.movementCompleteNotification(movement_list[0])
	
	def printMovementEvent(self,event):
		print event
	
	def printAttackInfo(self,atype,admg,attacker,defender,p1,p2):
		if admg != -1:
			if attacker[0] > 0:
				print str(p1) + ":s " + str(self.numberToUnit(attacker[0])) + " attacks " + str(p2) + ":s " + str(self.numberToUnit(defender[0])) + " by " + str(atype) + " making " + str(admg) + " damage"
			else:
				print str(p2) + ":s " + str(self.numberToUnit(attacker[0])) + " attacks " + str(p1) + ":s " + str(self.numberToUnit(defender[0])) + " by " + str(atype) + " making " + str(admg) + " damage"
		else:
			if abs(int(defender[0])) < 7:
				if attacker[0] > 0:
					print str(p1) + ":s " + str(self.numberToUnit(attacker[0])) + " attacks " + str(p2) + ":s " + str(self.numberToUnit(defender[0])) + " by " + str(atype) + " killing it"
				else:
					print str(p2) + ":s " + str(self.numberToUnit(attacker[0])) + " attacks " + str(p1) + ":s " + str(self.numberToUnit(defender[0])) + " by " + str(atype) + " killing it"
			else:
				if attacker[0] > 0:
					print str(p1) + ":s " + str(self.numberToUnit(attacker[0])) + " attacks " + str(p2) + ":s " + str(self.numberToUnit(defender[0])) + " by " + str(atype) + " destroying it"
				else:
					print str(p2) + ":s " + str(self.numberToUnit(attacker[0])) + " attacks " + str(p1) + ":s " + str(self.numberToUnit(defender[0])) + " by " + str(atype) + " destroying it"
	
	def printSquareNotEmpty(self):
		print "The square you wanted to construct a building is not empty!"
	
	def printUnitNotResources(self):
		print "You don't have enough resources to train that unit!"
		playSound("impossible")
	
	def printBuildingNotResources(self):
		print "You need more resources to construct that building!"
	
	def movementInProgressNotification(self,action):
		if action == 1:
			return ""
		elif action == 2:
			return " (gathering resources)"
		elif action == 3:
			return " (constructing a building)"
		elif action == 4:
			return " (repairing a building)"
		elif action == 5:
			return " (chasing unit)"

	def movementCompleteNotification(self,action):
		if action == 1:
			return " reaching it's destination"
		elif action == 2:
			return " and begins to gather resources"
		elif action == 3:
			return " and begins to construct a building"
		elif action == 4:
			return " and begins to repair a building"
		elif action == 5:
			return " and continues to chase the enemy target if it stays alive"
	
	def getButtonInfo(self,i):
		build = "Build (B)"
		stop = "Stop (S)"
		info = [build,stop]
		return info[i]
	
	def getSelectObjectInfo(self,object_id):
		
		objects = ["","worker","swordman","elite swordman","archer","knight","catapult","HQ","resource camp","barracks","archery range","stable","watch tower","castle","","","","siege workshop"]
		if object_id in [1,2,3,4,5,6,7,8,9,10,11,12,13,17]:
			return "Click to select this " + objects[object_id] + "."
		elif object_id in [14,15]:
			return "Mine here for resources."
		else:
			return "Chop down trees for more space."
	
	def getUnitInfo(self,object_id,j):
		
		worker = "Create WORKER (A) (Price: " +  str(Features().workerPrice()) + " )\n" + "Gathers resources, builds and repairs buildings. Isn't very capable for fighting." + "\n" + "HP: " + str(Features().workerHitpoints()) + " Movement: " + str(Features().workerMovement()) + " Attack: " + str(Features().workerAttack()) + " Defence: " + str(Features().workerDefence())
		swordman = "Create SWORDMAN (A) (Price: " +  str(Features().swordmanPrice()) + ")\n" + "Cheap and a very quick unit to train. Has +4 attack against mounted units and is therefore used in countering them." + "\n" + "HP: " + str(Features().swordmanHitpoints()) + " Movement: " + str(Features().swordmanMovement()) + " Attack: " + str(Features().swordmanAttack()) + " Defence: " + str(Features().swordmanDefence())
		elite_swordman = "Create ELITE SWORDMAN (S) (Price: " +  str(Features().eliteswordmanPrice()) + ")\n" + "Relatively cheap, quick to train, fast and devastating unit against all other units, especially mounted units with the +5 attack bonus." + "\n" + "HP: " + str(Features().eliteswordmanHitpoints()) + " Movement: " + str(Features().eliteswordmanMovement()) + " Attack: " + str(Features().eliteswordmanAttack()) + " Defence: " + str(Features().eliteswordmanDefence())
		archer = "Create ARCHER (A) (Price: " +  str(Features().archerPrice()) + ")\n" + "Fairly cheap and quite quick unit to train. Excellent supportative unit on the battlefield, weak on it's own. Has a +2 attack bonus vs targets on range 2,\n+1 attack bonus vs targets on range 3. +2 attack bonus vs units with movement equal to or less than 2. The bonuses do stack. Damages buildings only with attack = 1." + "\n" + "HP: " + str(Features().archerHitpoints()) + " Movement: " + str(Features().archerMovement()) + " Attack: " + str(Features().archerAttack()) + " Defence: " + str(Features().archerDefence()) + " Range: " + str(Features().archerRange())
		knight = "Create KNIGHT (A) (Price: " +  str(Features().knightPrice()) + ")\n" + "Expensive, swift and slow to train heavy unit. Can take and deal a great amount of damage. Good for raiding and countering enemy archers and catapults." + "\n" + "HP: " + str(Features().knightHitpoints()) + " Movement: " + str(Features().knightMovement()) + " Attack: " + str(Features().knightAttack()) + " Defence: " + str(Features().knightDefence())
		catapult = "Create CATAPULT (A) (Price: " +  str(Features().catapultPrice()) + ")\n" + "Expensive, weak, sluggish and very slow to train unit with the greatest range and attack damage in the game. Due to it's range, it's the best weapon against enemy\ndefensive structures. Must reload every other turn. Penetrates buildings' armour (defence = 0 when hit). Area of effective: all the squares adjacent vertically or\nhorizontally to the target will be hit with 1/4 of the attack damage. Can't fire when range = 1." + "\n" + "HP: " + str(Features().catapultHitpoints()) + " Movement: " + str(Features().catapultMovement()) + " Attack: " + str(Features().catapultAttack()) + " Defence: " + str(Features().catapultDefence()) + " Range: " + str(Features().catapultRange())
		units = [worker,swordman,elite_swordman,archer,knight,catapult]
		
		if object_id == 7 and j == 0:
			i = 0
		elif object_id == 9 and j == 0:
			i = 1
		elif object_id == 9 and j == 1:
			i = 2
		elif object_id == 10 and j == 0:
			i = 3
		elif object_id == 11 and j == 0:
			i = 4
		elif object_id == 17 and j == 0:
			i = 5
		else:
			return -1
		
		return units[i]
	
	def getBuildingInfo(self,i):
		
		hq = "Build HQ (Q) (Price: " + str(Features().HQPrice()) + ")\n" + "Used to create workers and store resources. Losing all HQs will result in a loss." + "\n" + "HP: " + str(Features().HQHitpoints()) + " Defence: " + str(Features().HQDefence())
		resource_camp = "Build RESOURCE CAMP (W) (Price: " + str(Features().resourcecampPrice()) + ")\n" + "A cheap building used as a place to deposit resources and as a strategic blocker." + "\n" + "HP: " + str(Features().resourcecampHitpoints()) + " Defence: " + str(Features().resourcecampDefence())
		barracks = "Build BARRACKS (E) (Price: " + str(Features().barracksPrice()) + ")\n" + "Used to train infantry. SWORDMAN is available as the building is finished, but training ELITE SWORDMAN needs CASTLE." + "\n" + "HP: " + str(Features().barracksHitpoints()) + " Defence: " + str(Features().barracksDefence())
		archery_range = "Build ARCHERY RANGE (R) (Price: " + str(Features().archeryrangePrice()) + ")\n" + "Used to train units specialized in the ranged attack. ARCHER is available as the building is finished." + "\n" + "HP: " + str(Features().archeryrangeHitpoints()) + " Defence: " + str(Features().archeryrangeDefence())
		stable = "Build STABLE (T) (Price: " + str(Features().stablePrice()) + ")\n" + "Used to train mounted units. KNIGHT is available as the building is finished." + "\n" + "HP: " + str(Features().stableHitpoints()) + " Defence: " + str(Features().stableDefence())
		siege_workshop = "Build SIEGE WORKSHOP (Y) (Price: " + str(Features().siegeworkshopPrice()) + ")\n" + "Used to create siege weapons. CATAPULT is available as the building is finished." + "\n" + "HP: " + str(Features().siegeworkshopHitpoints()) + " Defence: " + str(Features().siegeworkshopDefence())
		watch_tower = "Build WATCH TOWER (A) (Price: " + str(Features().watchtowerPrice()) + ")\n" + "Defensive structure ideal to obtain control of large areas on map. Doesn't have a strong attack, but isn't easy to destroy." + "\n" + "HP: " + str(Features().watchtowerHitpoints()) + " Attack: " + str(Features().watchtowerAttack()) + " Defence: " + str(Features().watchtowerDefence()) + " Range: " + str(Features().watchtowerRange())
		castle = "Build CASTLE (S) (Price: " + str(Features().castlePrice()) + ")\n" + "Expensive defensive structure that is like a watch tower with double HP and more than a double attack. Needed to train ELITE SWORDMAN." + "\n" + "HP: " + str(Features().castleHitpoints()) + " Attack: " + str(Features().castleAttack()) + " Defence: " + str(Features().castleDefence()) + " Range: " + str(Features().castleRange())
		buildings = [hq,resource_camp,barracks,archery_range,stable,siege_workshop,watch_tower,castle]
		
		if len(buildings) > i:
			return buildings[i]
		else:
			return -1
	
	def buildingNumberToString(self,number):
		if abs(number) == 7:
			return "HQ"
		elif abs(number) == 8:
			return "resource camp"
		elif abs(number) == 9:
			return "barracks"
		elif abs(number) == 10:
			return "archery range"
		elif abs(number) == 11:
			return "stable"
		elif abs(number) == 17:
			return "siege workshop"
		elif abs(number) == 12:
			return "watch tower"
		elif abs(number) == 13:
			return "castle"
	
	def returnWinningMethodStr(self, win):
		if win == 1:
			return "HQ destructions"
		else:
			return "missing wm"
			

	def coordinatesToReadable(self, coordinates0):
		print coordinates0
		coordinates1 = ["a",1]
		if coordinates0[1] == 0:
			coordinates1[0] = "j"
		elif coordinates0[1] == 1:
			coordinates1[0] = "i"
		elif coordinates0[1] == 2:
			coordinates1[0] = "h"
		elif coordinates0[1] == 3:
			coordinates1[0] = "g"
		elif coordinates0[1] == 4:
			coordinates1[0] = "f"
		elif coordinates0[1] == 5:
			coordinates1[0] = "e"
		elif coordinates0[1] == 6:
			coordinates1[0] = "d"
		elif coordinates0[1] == 7:
			coordinates1[0] = "c"
		elif coordinates0[1] == 8:
			coordinates1[0] = "b"
		elif coordinates0[1] == 9:
			coordinates1[0] = "a"
		coordinates1[1] = coordinates0[0]+1
		coordinates = str(coordinates1[0]) + str(coordinates1[1])
		return coordinates
	
	def numberToUnit(self, number): #could be extended to include buildings
		if abs(number) == 0:
			unit = "Blank square"
		elif abs(number) == 1:
			unit = "Worker"
		elif abs(number) == 2:
			unit = "Swordman"
		elif abs(number) == 3:
			unit = "Elite swordman"
		elif abs(number) == 4:
			unit = "Archer"
		elif abs(number) == 5:
			unit = "Knight"
		elif abs(number) == 6:
			unit = "Catapult"
		elif abs(number) == 7:
			unit = "HQ"
		elif abs(number) == 8:
			unit = "Resource camp"
		elif abs(number) == 9:
			unit = "Barracks"
		elif abs(number) == 10:
			unit = "Archery range"
		elif abs(number) == 11:
			unit = "Stable"
		elif abs(number) == 17:
			unit = "Siege workshop"
		elif abs(number) == 12:
			unit = "Watch tower"
		elif abs(number) == 13:
			unit = "Castle"
		return unit