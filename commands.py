from messages import *
from features import *

class Commands():
	def __init__(self):
		pass

	def unDuplicate(self,original_list):
		'''Deletes duplicates in a list.
		'''
		unique_list = []
		[unique_list.append(obj) for obj in original_list if obj not in unique_list]
		return unique_list

	def getUnitsList(self):
		return [1,2,3,4,5,6]
	def getBuildingsList(self):
		return [7,8,9,10,11,12,13,17]
	def getRangedUnits(self):
		return [4,6]
	def getDefStrct(self):
		return [12,13]
	def getHPUnits(self):
		return [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,17]

	def selectUnit(self, position, coordinates0):
		coordinates = self.initCoordinates(coordinates0)
		return position[coordinates[1]][coordinates[0]]
	def initCoordinates(self, coordinates0):
		if len(coordinates0) == 2:
			y = self.charToNumber(coordinates0[0])
			x = int(coordinates0[1])-1
		elif len(coordinates0) == 3:
			y = self.charToNumber(coordinates0[0])
			x = int(coordinates0[1])*10 + int(coordinates0[2])-1
		coordinates = [x,y]
		return coordinates
	def charToNumber(self, char):
		if char is "a":
			return 9
		elif char is "b":
			return 8
		elif char is "c":
			return 7
		elif char is "d":
			return 6
		elif char is "e":
			return 5
		elif char is "f":
			return 4
		elif char is "g":
			return 3
		elif char is "h":
			return 2
		elif char is "i":
			return 1
		else:
			return 0

	def charToUnit(self,char):
		if char == "w":
			return 1
		elif char == "s":
			return 2
		elif char == "e":
			return 3
		elif char == "a":
			return 4
		elif char == "k":
			return 5
		elif char == "c":
			return 6

	def charToBuilding(self,char):
		if char == "h":
			return 7
		elif char == "r":
			return 8
		elif char == "b":
			return 9
		elif char == "a":
			return 10
		elif char == "s":
			return 11
		elif char == "w":
			return 17
		elif char == "t":
			return 12
		elif char == "c":
			return 13

	def intToBuildingSpriteName(self,i):
		buildings = ["","","","","","","towncenter","resourcecamp","barracks","archeryrange","stable","watchtower","castle","","","","siegeworkshop"]
		return buildings[i-1]

	def orderMovementCoordinates(self, pre_coordinates0, post_coordinates0):
		pre_coordinates = self.initCoordinates(pre_coordinates0)
		post_coordinates = self.initCoordinates(post_coordinates0)
		return pre_coordinates, post_coordinates

	def getCrdsOfObjs(self,position,object_id):
		objects = []
		i=0
		for line in position:
			j=0
			for node in line:
				if type(node) is list:
					if node[0] == object_id:
						objects.append([int(j),int(i)])
				j+=1
			i+=1
		return objects

	def getAllObjs(self,position,object_ids,conditions):
		if type(object_ids) is int:
			object_ids = [object_ids]
		objects = []
		for object_id in object_ids:
			for line in position:
				for node in line:
					if node != 0:
						if node[0] == object_id:
							objects.append(list(node))
		if conditions:
			objects_new = []
			for object in objects:
				for condition in conditions:
					if object[condition[0]] == condition[1]:
						objects_new.append(list(object))
			objects = objects_new

		return objects

	def getSprMvmnt(self,pre,post,xy):
		M = 3
		if pre[1]-post[1] == 0:
			x = M
		else:
			x = M*((-pre[0]+post[0])/abs(pre[1]-post[1]))
			if x > M:
				x = M
		if pre[0]-post[0] == 0:
			y = M
		else:
			y = M*((-pre[1]+post[1])/abs(pre[0]-post[0]))
			if y > M:
				y = M
		if xy:
			return y
		else:
			return x

	def defineActionStatus(self, turn, pre_coordinates, post_coordinates, position):
		#action =1 -> basic movement, =2 -> gathering resources, =3 -> constructing a building (building HP=-10000), =4 -> repairing a building (building HP < building max_HP), =5 -> chasing an enemy unit
		object = position[pre_coordinates[1]][pre_coordinates[0]]
		target = position[post_coordinates[1]][post_coordinates[0]]
		if type(target) is list:
			if turn%2==0:
				if object[0] == 1 and target[0] in [14,15]:
					action = 2 #Gather resources
				elif object[0] == 1 and target[0] in [7,8,9,10,11,12,13,17] and target[9] == -1:
					action = 3 #Build
				elif object[0] == 1 and target[0] in [7,8,9,10,11,12,13,17] and (target[4][0] < target[4][1]):
					action = 4 #Repair
				elif target[0] < 0:
					action = 5 #Attack
				elif target[0] == 16:
					action = 6 #Chop
				else:
					action = 1
			else:
				if object[0] == -1 and target[0] in [14,15]:
					action = 2
				elif object[0] == -1 and target[0] in [-7,-8,-9,-10,-11,-12,-13,-17] and target[9] == -1:
					action = 3
				elif object[0] == -1 and target[0] in [-7,-8,-9,-10,-11,-12,-13,-17] and (target[4][0] < target[4][1]):
					action = 4
				elif target[0] in [1,2,3,4,5,6,7,8,9,10,11,12,13,17]:
					action = 5
				elif target[0] == 16:
					action = 6 #Chop
				else:
					action = 1
		else:
			action = 1

		position[pre_coordinates[1]][pre_coordinates[0]][2] = [action,post_coordinates] #vaihtaa unitin current action

		#print [action,post_coordinates]

		return action, position

	def moveToBuildingView2(self,b_id):
		playSound("button1")
		return b_id,True,False,True

	def giveGUIChosenBuilding(self,mb1down_pos,mb1up_pos):
		if 137 <= mb1down_pos[0] <= 186 and 137 <= mb1up_pos[0] <= 186 and 427 <= mb1down_pos[1] <= 476 and 427 <= mb1up_pos[1] <= 476:
			building_id = 7
		elif 191 <= mb1down_pos[0] <= 240 and 191 <= mb1up_pos[0] <= 240 and 427 <= mb1down_pos[1] <= 476 and 427 <= mb1up_pos[1] <= 476:
			building_id = 8
		elif 245 <= mb1down_pos[0] <= 294 and 245 <= mb1up_pos[0] <= 294 and 427 <= mb1down_pos[1] <= 476 and 427 <= mb1up_pos[1] <= 476:
			building_id = 9
		elif 299 <= mb1down_pos[0] <= 348 and 299 <= mb1up_pos[0] <= 348 and 427 <= mb1down_pos[1] <= 476 and 427 <= mb1up_pos[1] <= 476:
			building_id = 10
		elif 353 <= mb1down_pos[0] <= 402 and 353 <= mb1up_pos[0] <= 402 and 427 <= mb1down_pos[1] <= 476 and 427 <= mb1up_pos[1] <= 476:
			building_id = 11
		elif 407 <= mb1down_pos[0] <= 456 and 407 <= mb1up_pos[0] <= 456 and 427 <= mb1down_pos[1] <= 476 and 427 <= mb1up_pos[1] <= 476:
			building_id = 17
		elif 137 <= mb1down_pos[0] <= 186 and 137 <= mb1up_pos[0] <= 186 and 481 <= mb1down_pos[1] <= 530 and 481 <= mb1up_pos[1] <= 530:
			building_id = 12
		elif 191 <= mb1down_pos[0] <= 240 and 191 <= mb1up_pos[0] <= 240 and 481 <= mb1down_pos[1] <= 530 and 481 <= mb1up_pos[1] <= 530:
			building_id = 13
		else:
			return 0
		playSound("button1")
		return building_id

	def getBuildingsUnit(self,i,btn_id):
		if i == 7:
			return 1
		elif i == 9:
			if btn_id == 1:
				return 2
			elif btn_id == 2:
				return 3
		elif i == 10:
			return 4
		elif i == 11:
			return 5
		else:
			return 6

	def launchUnitProduction(self,button_id,object,amount,turn):
		action = 0
		produce_unit = []
		if ((object[0] > 0 and turn%2==0) or (object[0] < 0 and turn%2!=0)) and abs(object[0]) in [7,9,10,11,17]:
			if object[9] != -1:
				action = 3
				produce_unit = [self.getBuildingsUnit(abs(object[0]),button_id),object[1],amount] #[villager, from coordinates of the town center, the defined amount]
				playSound("button1")
		return action, produce_unit

	def playGoingSound(self,i):
		if i == 1:
			playSound("villagergo")
		elif i == 5:
			playSound("knightgo")
		elif i == 6:
			playSound("catapultgo")
		else:
			playSound("warriorgo")

	def playDyingSound(self,i):
		if i in [1,2,3,4]:
			playSound("die")
		elif i == 5:
			playSound("knightdie")
		elif i == 6:
			playSound("catapultdie")
		elif i == 13:
			playSound("castledestruction")
		else:
			playSound("destruction")

	def objectiveNoMoreReactions(self,position,target):
		#Tehdaan workereista idleja
		for line in position:
			for node in line:
				if node != 0:
					if abs(node[0]) == 1:
						if node[2] != 0:
							if node[2][0] in [3,4,6] and node[2][1] == target[1]:
								node[2] = 0
		return position

	def objectDiedReactions(self,position,defender):
		#Muutetaan jahtaavien hyokkaajien jahtaus liikkumiseksi
		for line in position:
			for node in line:
				if node != 0:
					if abs(node[0]) in [1,2,3,4,5,6]:
						if node[2] != 0:
							if node[2][0] == 5 and node[2][1] == defender[1]:
								node[2] = list([1,node[2][1]])
							if node[2][0] in [3,4] and node[2][1] == defender[1]:
								node[2] = 0
		return position

	def returnIfUnitPossible(self,position,unit):
		#Returns boolean of ability to create specific unit
		#TODO suppea eika testattu
		cond = True
		if abs(unit) == 3:
			cond = False
			for row in position:
				for node in row:
					if type(node) is list:
						if (node[0] == 13 and unit > 0) or (node[0] == -13 and unit < 0):
							if node[9] != -1:
								cond = True

		if cond:
			return True
		else:
			return False

	def getObjectPriceByInt(self,object):
		price = self.getUnitPriceByInt(object)
		if price == -1:
			price = self.getBuildingPriceByInt(object)
		return price

	def getUnitPriceByInt(self,unit):
		if abs(unit) == 1:
			return Features().workerPrice()
		elif abs(unit) == 2:
			return Features().swordmanPrice()
		elif abs(unit) == 3:
			return Features().eliteswordmanPrice()
		elif abs(unit) == 4:
			return Features().archerPrice()
		elif abs(unit) == 5:
			return Features().knightPrice()
		elif abs(unit) == 6:
			return Features().catapultPrice()
		else:
			return -1

	def getBuildingPriceByInt(self,building):
		if abs(building) == 7:
			return Features().HQPrice()
		elif abs(building) == 8:
			return Features().resourcecampPrice()
		elif abs(building) == 9:
			return Features().barracksPrice()
		elif abs(building) == 10:
			return Features().archeryrangePrice()
		elif abs(building) == 11:
			return Features().stablePrice()
		elif abs(building) == 17:
			return Features().siegeworkshopPrice()
		elif abs(building) == 12:
			return Features().watchtowerPrice()
		elif abs(building) == 13:
			return Features().castlePrice()
		else:
			return -1

	def getBuildingMaxHPByInt(self,building):
		if abs(building) == 7:
			return Features().HQHitpoints()
		elif abs(building) == 8:
			return Features().resourcecampHitpoints()
		elif abs(building) == 9:
			return Features().barracksHitpoints()
		elif abs(building) == 10:
			return Features().archeryrangeHitpoints()
		elif abs(building) == 11:
			return Features().stableHitpoints()
		elif abs(building) == 17:
			return Features().siegeworkshopHitpoints()
		elif abs(building) == 12:
			return Features().watchtowerHitpoints()
		elif abs(building) == 13:
			return Features().castleHitpoints()

	def getFunctioningBuildingByInt(self,unit):
		if unit > 0:
			sign = 1
		else:
			sign = -1

		if abs(unit) == 1:
			return 7*sign
		elif abs(unit) == 2 or abs(unit) == 3:
			return 9*sign
		elif abs(unit) == 4:
			return 10*sign
		elif abs(unit) == 5:
			return 11*sign
		elif abs(unit) == 6:
			return 17*sign

	def getBuildingSound(self,i):
		if i == 7:
			return "hq"
		elif i == 8:
			return "resourcecamp"
		elif i == 9:
			return "barracks"
		elif i == 10:
			return "archeryrange"
		elif i == 11:
			return "stable"
		elif i == 12:
			return "watchtower"
		elif i == 13:
			return "castle"
		else:
			return "siegeworkshop"

	def getUnitTrainingSpeedByInt(self,unit):
		if abs(unit) == 1:
			return Features().workerTrainingSpeed()
		elif abs(unit) == 2:
			return Features().swordmanTrainingSpeed()
		elif abs(unit) == 3:
			return Features().eliteswordmanTrainingSpeed()
		elif abs(unit) == 4:
			return Features().archerTrainingSpeed()
		elif abs(unit) == 5:
			return Features().knightTrainingSpeed()
		elif abs(unit) == 6:
			return Features().catapultTrainingSpeed()

	def insertBuildingUnderConstructionFeatures(self,building):
		building[4][0] = 1 #1 = uusi HP
		building[9] = -1 #-1 = under construction
		return building



	def returnBuildingSpecial(self,special_index):
		#TODO ei oikea toteutus!
		return abs(special_index)

	def changeStrategy(self, current_strategy):
		print "\tCurrent strategy: " + Messages().strategyLite(current_strategy) + "\n\n" + "A: " + Messages().strategy(1) + "\n\n" + "D: " + Messages().strategy(2) + "\n\n" + "H: " + Messages().strategy(3) + "\n"
		strategy0 = raw_input("New strategy: ")
		strategy = self.charToNumber2(strategy0)
		return strategy
	def charToNumber2(self, strategy0):
		strategy = 1
		if strategy0 is "a":
			strategy = 1
		elif strategy0 is "d":
			strategy = 2
		elif strategy0 is "h":
			strategy = 3
		return strategy
