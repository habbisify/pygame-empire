import pygame

from commands import *
from pygameactivity import *
from ai import *

class PygEvent:
	def __init__(self):
		pass

	def eventHandler(self,screen,time_left,game_loop,action,position,turn,player1_type,player2_type,player1_name,player2_name,player1_resources,player2_resources,training_queue,sq_query,selected_objects,remove_from_selected_objects,produce_unit,constructable_building,build_info,building_view1,building_view2,object,lines,sfx,ctrldown,mb1down_pos,mb1down_x,mb1down_y,mb3down_pos,mb3down_x,mb3down_y,mb3up_pos,mb3up_x,mb3up_y):

		destination = [0,0]

		if (turn%2==0 and player1_type==0) or (turn%2!=0 and player2_type==0):
			for event in pygame.event.get():

				#print event

				#Esc alas
				if event.type == 2 and event.key == 27:
					game_loop = False

				#Vasen hiiren painike alas
				elif event.type == 5 and event.button == 1:
					mb1down_pos = list(event.pos)
					mb1down_x = int(mb1down_pos[0]/40)
					mb1down_y = int((mb1down_pos[1]-20)/40)

				#Vasen hiiren painike ylos
				elif event.type == 6 and event.button == 1:
					mb1up_pos = list(event.pos)
					mb1up_x = int(mb1up_pos[0]/40)
					mb1up_y = int((mb1up_pos[1]-20)/40)

					#Objectin valinta kartalta hiirella
					if mb1down_x == mb1up_x and mb1down_y == mb1up_y and 0 <= mb1down_y < len(position) and position[mb1down_y][mb1down_x] != 0 and not building_view2:
						building_view1 = False
						sq_query = True
						sfx = False
						no_duplicates = True

						#Valitsee yksikon. Jos valitsee jo valitun yksikon, valitsee kaikki samanlaiset yksikot
						if not ctrldown:
							selected_objects_temp = list(selected_objects) #Ilman temp-ratkaisua joutuu forever-looppiin
							for object in selected_objects:
								if object == position[int(mb1up_y)][int(mb1up_x)]:
									for line in position:
										for node in line:
											if type(node) is list:
												if node[0] == object[0]:
													selected_objects_temp.append(list(node))
													no_duplicates = False
							selected_objects = list(Commands().unDuplicate(selected_objects_temp))
							if no_duplicates:
								selected_objects = []
							else:
								object = position[int(mb1up_y)][int(mb1up_x)]

						#Lisaa tai poistaa klikatun objectin valinnan
						else:
							i=0
							for object in selected_objects:
								if object == position[int(mb1up_y)][int(mb1up_x)]:
									remove_index = int(i)
									remove_from_selected_objects = True
								i+=1

						#Tehdaan tuoreimmasta valinnasta object
						if not remove_from_selected_objects:
							if no_duplicates:
								object = position[int(mb1up_y)][int(mb1up_x)]
								selected_objects.append(list(object))

						#Poistetaan tuorein valinta ja tehdaan seuraavaksi tuoreimmasta valinnasta object. Jos ei ole, sq_query = False
						else:
							selected_objects.pop(remove_index)
							if len(selected_objects) == 0:
								sq_query = False
							else:
								object = selected_objects[len(selected_objects)-1]
							sfx = True
							remove_from_selected_objects = False

					#Rakennuksen sijoittaminen
					elif mb1down_x == mb1up_x and mb1down_y == mb1up_y and 0 <= mb1down_y < len(position) and building_view2:

						#Rakennuspaikka on tyhja
						if position[mb1down_y][mb1down_x] == 0:
							builders = []
							for object in selected_objects:
								if abs(object[0]) == 1:
									builders.append([int(object[1][0]),int(object[1][1])])
							build_info = [int(constructable_building),[int(mb1down_x),int(mb1down_y)],builders] #[bld_id, [crds of bld], [crds of workers]]
							action = 4
							building = PygAct().loadObjectImage(constructable_building*(turn%2*-2+1))
							screen.blit(building,(1+mb1down_x*40,21+mb1down_y*40))
							pygame.draw.rect(screen,colours("blue"),[[1+mb1down_x*40,21+mb1down_y*40],[39,39]],1)
							playSound("villagergobuilding")
							building_view2 = False

						#Ei ole tyhja
						else:
							playSound("impossible")
							building_view2 = False

					#Villagerin rakennusvalikon avaaminen
					elif PygAct().btn1(mb1down_pos,mb1up_pos) and abs(object[0]) == 1 and not (building_view1 or building_view2) and sq_query:
						playSound("button1")
						building_view1 = True

					#Rakennuksen valinta rakennusvalikossa
					elif abs(object[0]) == 1 and building_view1 and sq_query and Commands().giveGUIChosenBuilding(mb1down_pos,mb1up_pos):
						constructable_building, self.sq_query, building_view1, building_view2 = \
						Commands().moveToBuildingView2(Commands().giveGUIChosenBuilding(mb1down_pos,mb1up_pos))

					#Yksikon kouluttaminen, ensimmainen nappi
					elif PygAct().btn1(mb1down_pos,mb1up_pos) and (abs(int(object[0])) in [7,9,10,11,17]) and sq_query and object[9] != -1:
						amount = 1
						action, produce_unit = Commands().launchUnitProduction(1,object,amount,turn)
					#Toinen nappi
					elif PygAct().btn2(mb1down_pos,mb1up_pos) and abs(int(object[0])) == 9 and sq_query:
						amount = 1
						action, produce_unit = Commands().launchUnitProduction(2,object,amount,turn)

					#Rakennuksen keskeyttaminen
					elif PygAct().btn1(mb1down_pos,mb1up_pos) and (abs(int(object[0])) in [7,9,10,11,12,13,17]) and sq_query and object[9] == -1 and ((object[0] > 0 and turn%2==0) or (object[0] < 0 and turn%2!=0)):
						sq_query = False
						selected_objects = []
						building_view1 = False

						Commands().objectDiedReactions(position,object)
						if turn%2==0:
							player1_resources+=int(Commands().getBuildingPriceByInt(object[0])*0.5)
						else:
							player2_resources+=int(Commands().getBuildingPriceByInt(object[0])*0.5)
						position[object[1][1]][object[1][0]] = 0
						playSound("destruction")

					#Next turn -nappi
					elif PygAct().btn31(mb1down_pos,mb1up_pos):
						playSound("button1")
						action = 1

					#Idle workers -nappi
					elif PygAct().btn32(mb1down_pos,mb1up_pos):
						playSound("button1")
						selected_objects = list(Commands().getAllObjs(position,turn%2*-2+1,[[2,0]]))
						if len(selected_objects) > 0:
							object = selected_objects[0]
							building_view1 = False
							sq_query = True
							sfx = False

					#Idle warriors -nappi
					elif PygAct().btn33(mb1down_pos,mb1up_pos):
						playSound("button1")
						selected_objects = list(Commands().getAllObjs(position,[(turn%2*-2+1)*2,(turn%2*-2+1)*3,(turn%2*-2+1)*4,(turn%2*-2+1)*5,(turn%2*-2+1)*6],[[2,0]]))
						if len(selected_objects) > 0:
							object = selected_objects[0]
							building_view1 = False
							sq_query = True
							sfx = False

					#Ei mikaan tunnistettu klikkaus
					else:
						sq_query = False
						selected_objects = []
						building_view1 = False

				#Oikea hiiren painike alas
				elif event.type == 5 and event.button == 3:
					mb3down_pos = list(event.pos)
					mb3down_x = int(mb3down_pos[0]/40)
					mb3down_y = int((mb3down_pos[1]-20)/40)

				#Oikea hiiren painike ylos
				elif event.type == 6 and event.button == 3:
					if not building_view2:
						mb3up_pos = list(event.pos)
						mb3up_x = int(mb3up_pos[0]/40)
						mb3up_y = int((mb3up_pos[1]-20)/40)
						if mb3down_x == mb3up_x and mb3down_y == mb3up_y and 0 <= mb3down_y < len(position) and len(selected_objects) > 0:
							action = 2
						elif PygAct().btn1(mb3down_pos,mb3up_pos) and (abs(int(object[0])) in [7,9,10,11,17]) and sq_query:
							amount = -1
							action, produce_unit = Commands().launchUnitProduction(1,object,amount,turn)

					else:
						building_view2 = False

				#Ctrl alas
				elif event.type == 2 and event.key == 306:
					ctrldown = True
				#Ctrl ylos
				elif event.type == 3 and event.key == 306:
					ctrldown = False

				#h alas
				elif event.type == 2 and event.key == 104:
					sq_query = True
					sfx = False
					selected_objects = []
					hqs = Commands().getCrdsOfObjs(position,7*(turn%2*-2+1))
					obj_coordinates = list(hqs[int(random()*len(hqs))])
					object = position[obj_coordinates[1]][obj_coordinates[0]]
					selected_objects.append(list(object))

				#b alas
				elif event.type == 2 and event.key == 98 and ((object[0] == 1 and turn%2==0) or (object[0] == -1 and turn%2!=0)) and sq_query:
					playSound("button1")
					building_view1 = True

				#q alas
				elif event.type == 2 and event.key == 113 and building_view1:
					constructable_building, self.sq_query, building_view1, building_view2 = \
					Commands().moveToBuildingView2(7)

				#w alas
				elif event.type == 2 and event.key == 119 and building_view1:
					constructable_building, self.sq_query, building_view1, building_view2 = \
					Commands().moveToBuildingView2(8)

				#e alas
				elif event.type == 2 and event.key == 101 and building_view1:
					constructable_building, self.sq_query, building_view1, building_view2 = \
					Commands().moveToBuildingView2(9)

				#r alas
				elif event.type == 2 and event.key == 114 and building_view1:
					constructable_building, self.sq_query, building_view1, building_view2 = \
					Commands().moveToBuildingView2(10)

				#t alas
				elif event.type == 2 and event.key == 116 and building_view1:
					constructable_building, self.sq_query, building_view1, building_view2 = \
					Commands().moveToBuildingView2(11)

				#y alas
				elif event.type == 2 and event.key == 121 and building_view1:
					constructable_building, self.sq_query, building_view1, building_view2 = \
					Commands().moveToBuildingView2(17)

				#a alas
				elif event.type == 2 and event.key == 97:
					if sq_query and not building_view1:
						if not ctrldown:
							amount = 1
							action, produce_unit = Commands().launchUnitProduction(1,object,amount,turn)
						else:
							amount = -1
							action, produce_unit = Commands().launchUnitProduction(1,object,amount,turn)
					elif building_view1:
						constructable_building, self.sq_query, building_view1, building_view2 = \
						Commands().moveToBuildingView2(12)

				#s alas
				elif event.type == 2 and event.key == 115:
					if sq_query and not building_view1:
						if not ctrldown:
							amount = 1
							action, produce_unit = Commands().launchUnitProduction(2,object,amount,turn)
						else:
							amount = -1
							action, produce_unit = Commands().launchUnitProduction(2,object,amount,turn)
					elif building_view1:
						constructable_building, self.sq_query, building_view1, building_view2 = \
						Commands().moveToBuildingView2(13)

				#. alas
				elif event.type == 2 and event.key == 46:
					playSound("button1")
					selected_objects = list(Commands().getAllObjs(position,turn%2*-2+1,[[2,0]]))
					if len(selected_objects) > 0:
						object = selected_objects[0]
						building_view1 = False
						sq_query = True
						sfx = False

				#, alas
				elif event.type == 2 and event.key == 44:
					playSound("button1")
					selected_objects = list(Commands().getAllObjs(position,[(turn%2*-2+1)*2,(turn%2*-2+1)*3,(turn%2*-2+1)*4,(turn%2*-2+1)*5,(turn%2*-2+1)*6],[[2,0]]))
					if len(selected_objects) > 0:
						object = selected_objects[0]
						building_view1 = False
						sq_query = True
						sfx = False

				#Delete alas
				elif event.type == 2 and event.key == 127:
					if sq_query:
						destroy = False
						for selected_object in selected_objects:
							if selected_object[0] not in [14,15,16] and (selected_object[0] > 0 and turn%2==0) or (selected_object[0] < 0 and turn%2!=0):
								Commands().playDyingSound(abs(position[selected_object[1][1]][selected_object[1][0]][0]))
								Commands().objectDiedReactions(position,position[selected_object[1][1]][selected_object[1][0]])
								position[selected_object[1][1]][selected_object[1][0]] = 0
								destroy = True
						if destroy:
							sq_query = False

				#Space alas
				elif event.type == 3 and event.key == 32:
					playSound("button1")
					action = 1

				#Vakio
				elif event.type == pygame.QUIT:
					game_loop = False
		else:
			for event in pygame.event.get():
				if event.type == pygame.QUIT:
					game_loop = False

			action,selected_objects,produce_unit,build_info,destination = AI().selectAction(position,turn,turn%2*-2+1,player1_resources,player2_resources,training_queue[turn%2])

		if sq_query:
			sfx = PygAct().sqQuery(screen,position,object,selected_objects,player1_name,player2_name,turn,sfx,lines,training_queue)
			if building_view1:
				PygAct().buildingView1(screen,lines,turn%2+1)
			if building_view2:
				PygAct().buildingView2(screen,lines,position,turn%2+1,constructable_building)

		if time_left <= 200:
			action = 1

		return game_loop,action,player1_resources,player2_resources,sq_query,selected_objects,remove_from_selected_objects,produce_unit,constructable_building,build_info,building_view1,building_view2,object,lines,sfx,ctrldown,mb1down_pos,mb1down_x,mb1down_y,mb3down_pos,mb3down_x,mb3down_y,mb3up_pos,mb3up_x,mb3up_y,destination
