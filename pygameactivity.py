import pygame
from time import sleep

from commands import *
from messages import *
from audio import *
from graphics import *

class PygAct:
	def __init__(self):
		
		#TODO Fontteja ei olla varmistettu millaan exception-checkilla
		self.font12 = pygame.font.Font(pygame.font.match_font('arial'), 12)
		self.font12b = pygame.font.Font(pygame.font.match_font('arial',True), 12) #Bold
		self.font18 = pygame.font.Font(pygame.font.match_font('arial'), 18)
		self.font24 = pygame.font.Font(pygame.font.match_font('arial'), 24)
	
	def btn1(self,mbdown_pos,mbup_pos):
	
		if 137 <= mbdown_pos[0] <= 186 and 137 <= mbup_pos[0] <= 186 and 427 <= mbdown_pos[1] <= 476 and 427 <= mbup_pos[1] <= 476:
			return True
		else:
			return False
	
	def btn2(self,mbdown_pos,mbup_pos):
	
		if 191 <= mbdown_pos[0] <= 240 and 191 <= mbup_pos[0] <= 240 and 427 <= mbdown_pos[1] <= 476 and 427 <= mbup_pos[1] <= 476:
			return True
		else:
			return False
	
	def btn3(self,mbdown_pos,mbup_pos):
		
		if 245 <= mbdown_pos[0] <= 294 and 245 <= mbup_pos[0] <= 294 and 427 <= mbdown_pos[1] <= 476 and 427 <= mbup_pos[1] <= 476:
			return True
		else:
			return False
	
	def btn4(self,mbdown_pos,mbup_pos):
		
		if 299 <= mbdown_pos[0] <= 348 and 299 <= mbup_pos[0] <= 348 and 427 <= mbdown_pos[1] <= 476 and 427 <= mbup_pos[1] <= 476:
			return True
		else:
			return False
		
	def btn5(self,mbdown_pos,mbup_pos):
		
		if 353 <= mbdown_pos[0] <= 402 and 353 <= mbup_pos[0] <= 402 and 427 <= mbdown_pos[1] <= 476 and 427 <= mbup_pos[1] <= 476:
			return True
		else:
			return False
	
	def btn6(self,mbdown_pos,mbup_pos):
		
		if 407 <= mbdown_pos[0] <= 456 and 407 <= mbup_pos[0] <= 456 and 427 <= mbdown_pos[1] <= 476 and 427 <= mbup_pos[1] <= 476:
			return True
		else:
			return False
	
	def btn11(self,mbdown_pos,mbup_pos):
		
		if 137 <= mbdown_pos[0] <= 186 and 137 <= mbup_pos[0] <= 186 and 481 <= mbdown_pos[1] <= 530 and 481 <= mbup_pos[1] <= 530:
			return True
		else:
			return False
	
	def btn12(self,mbdown_pos,mbup_pos):
		
		if 191 <= mbdown_pos[0] <= 240 and 191 <= mbup_pos[0] <= 240 and 481 <= mbdown_pos[1] <= 530 and 481 <= mbup_pos[1] <= 530:
			return True
		else:
			return False
	
	def btn31(self,mbdown_pos,mbup_pos):
		
		if 624 <= mbdown_pos[0] <= 760 and 624 <= mbup_pos[0] <= 760 and 422 <= mbdown_pos[1] <= 452 and 422 <= mbup_pos[1] <= 452:
			return True
		else:
			return False
	
	def btn32(self,mbdown_pos,mbup_pos):
		
		if (735-mbdown_pos[0])**2+(10*40+128-mbdown_pos[1])**2 <= 17**2 and (735-mbup_pos[0])**2+(10*40+128-mbup_pos[1])**2 <= 17**2:
			return True
		else:
			return False
	
	def btn33(self,mbdown_pos,mbup_pos):
	
		if (700-mbdown_pos[0])**2+(10*40+133-mbdown_pos[1])**2 <= 13**2 and (700-mbup_pos[0])**2+(10*40+133-mbup_pos[1])**2 <= 13**2:
			return True
		else:
			return False
	
	def loadObjectImage(self,object_id):
		
		if object_id == 0:
			object,object_rect = loadImage("grass")
		elif int(object_id) == 1:
			if object_id == 1:
				object,object_rect = loadImage("villager1")
			elif object_id == 1.1:
				object,object_rect = loadImage("villager1miner")
			elif object_id == 1.2:
				object,object_rect = loadImage("villager1lumberjack")
		elif int(object_id) == -1:
			if object_id == -1:
				object,object_rect = loadImage("villager2")
			elif object_id == -1.1:
				object,object_rect = loadImage("villager2miner")
			elif object_id == -1.2:
				object,object_rect = loadImage("villager2lumberjack")
		elif object_id == 2:
			object,object_rect = loadImage("swordman1")
		elif object_id == -2:
			object,object_rect = loadImage("swordman2")
		elif object_id == 3:
			object,object_rect = loadImage("eliteswordman1")
		elif object_id == -3:
			object,object_rect = loadImage("eliteswordman2")
		elif object_id == 4:
			object,object_rect = loadImage("archer1")
		elif object_id == -4:
			object,object_rect = loadImage("archer2")
		elif object_id == 5:
			object,object_rect = loadImage("knight1")
		elif object_id == -5:
			object,object_rect = loadImage("knight2")
		elif object_id == 6:
			object,object_rect = loadImage("catapult1")
		elif object_id == -6:
			object,object_rect = loadImage("catapult2")
		elif object_id == 7:
			object,object_rect = loadImage("towncenter1")
		elif object_id == -7:
			object,object_rect = loadImage("towncenter2")
		elif object_id == 8:
			object,object_rect = loadImage("resourcecamp1")
		elif object_id == -8:
			object,object_rect = loadImage("resourcecamp2")
		elif object_id == 9:
			object,object_rect = loadImage("barracks1")
		elif object_id == -9:
			object,object_rect = loadImage("barracks2")
		elif object_id == 10:
			object,object_rect = loadImage("archeryrange1")
		elif object_id == -10:
			object,object_rect = loadImage("archeryrange2")
		elif object_id == 11:
			object,object_rect = loadImage("stable1")
		elif object_id == -11:
			object,object_rect = loadImage("stable2")
		elif object_id == 12:
			object,object_rect = loadImage("watchtower1")
		elif object_id == -12:
			object,object_rect = loadImage("watchtower2")
		elif object_id == 13:
			object,object_rect = loadImage("castle1")
		elif object_id == -13:
			object,object_rect = loadImage("castle2")
		elif object_id == 14:
			object,object_rect = loadImage("resourceplant")
		elif object_id == 15:
			object,object_rect = loadImage("hugeresourceplant")
		elif int(object_id) == 16:
			object,object_rect = loadImage("block"+str(int(10.0*object_id-159)))
		elif object_id == 17:
			object,object_rect = loadImage("siegeworkshop1")
		elif object_id == -17:
			object,object_rect = loadImage("siegeworkshop2")
		return object
	
	
	
	
	
	
	def defaultGUI(self,screen,time_left,position,player1_name,player2_name,player1_resources,player2_resources,player1_score,player2_score,turn):

		grass, grass_rect = loadImage("grass")
		villager1, villager1_rect = loadImage("villager1")
		villager1miner, villager1miner_rect = loadImage("villager1miner")
		villager1lumberjack, villager1lumberjack_rect = loadImage("villager1lumberjack")
		villager2, villager2_rect = loadImage("villager2")
		villager2miner, villager2miner_rect = loadImage("villager2miner")
		villager2lumberjack, villager2lumberjack_rect = loadImage("villager2lumberjack")
		towncenter1, towncenter1_rect = loadImage("towncenter1")
		towncenter2, towncenter2_rect = loadImage("towncenter2")
		resourceplant, resourceplant_rect = loadImage("resourceplant")
		hugeresourceplant, hugeresourceplant_rect = loadImage("hugeresourceplant")
		block1, block_rect1 = loadImage("block1")
		block2, block_rect2 = loadImage("block2")
		block3, block_rect3 = loadImage("block3")
		block4, block_rect4 = loadImage("block4")
		block5, block_rect5 = loadImage("block5")
		block6, block_rect6 = loadImage("block6")
		block7, block_rect7 = loadImage("block7")
		block8, block_rect8 = loadImage("block8")
		blocks = [block1,block2,block3,block4,block5,block6,block7,block8]
		
		screen.fill(colours("white"))
		
		x_begin = 0
		y_begin = 20
		
		menubar, menubar_rect = loadImage("menubar")
		screen.blit(menubar, (x_begin,0))
			
		player1 = self.font12.render(player1_name, 1, colours("white"))
		screen.blit(player1, [30,2])
		player2 = self.font12.render(player2_name, 1, colours("white"))
		screen.blit(player2, [655-len(player2_name)*5,2])
		
		player1_r = self.font12.render(str(player1_resources), 1, colours("white"))
		screen.blit(player1_r, [190,2])
		player2_r = self.font12.render(str(player2_resources), 1, colours("white"))
		screen.blit(player2_r, [497-len(str(player2_resources))*5,2])
		
		player1_s = self.font12.render(str(player1_score), 1, colours("white"))
		screen.blit(player1_s, [280,2])
		player2_s = self.font12.render(str(player2_score), 1, colours("white"))
		screen.blit(player2_s, [407-len(str(player2_score))*5,2])
		
		if turn%2==0:
			pygame.draw.rect(screen,colours("blue"),[[334,3],[20,13]],0)
			pygame.draw.rect(screen,colours("white"),[[1,1],[355,18]],1)
		else:
			pygame.draw.rect(screen,colours("red"),[[334,3],[20,13]],0)
			pygame.draw.rect(screen,colours("white"),[[332,1],[356,18]],1)
		
		time_left_sec = int(time_left/1000)
		if time_left_sec < 60:
			time_left_str = str(time_left_sec)
		else:
			time_mod_sec = str(time_left_sec%60)
			if len(time_mod_sec) == 1:
				time_mod_sec = "0"+time_mod_sec
			time_left_str = str(int(time_left_sec/60))+"."+time_mod_sec
			
		clock_display = self.font12b.render(time_left_str, 1, colours("white"))
		screen.blit(clock_display, [341-((len(time_left_str)-1)*2),2])
		
		th=1
		k=0
		for column in position[0]:
			pygame.draw.line(screen,colours("black"),[x_begin+k*40,y_begin],[x_begin+k*40,y_begin+40*len(position)],th)
			k+=1
		pygame.draw.line(screen,colours("black"),[x_begin+k*40,y_begin],[x_begin+k*40,y_begin+40*len(position)],th)
		columns = int(k)
		k=0
		for line in position:
			pygame.draw.line(screen,colours("black"),[x_begin,y_begin+k*40],[x_begin+40*len(position[0]),y_begin+k*40],th)
			k+=1
		pygame.draw.line(screen,colours("black"),[x_begin,y_begin+k*40],[x_begin+40*len(position[0]),y_begin+k*40],th)
		lines = int(k)
		
		infobar, infobar_rect = loadImage("infobar")
		screen.blit(infobar, (x_begin,y_begin+lines*40))
		btnframe,btnframe_rect = loadImage("btnframe")
		screen.blit(btnframe, (x_begin+15*40+23,y_begin+lines*40+2))
		next_t = self.font18.render("Next turn", 1, colours("white"))
		screen.blit(next_t, [x_begin+16*40+21,y_begin+lines*40+7])
		slctidlersbtn,slctidlersbtn_rect = loadImage("slctidlersbtn")
		screen.blit(slctidlersbtn, (x_begin+17*40+38,y_begin+lines*40+89))
		slctwarriorsbtn,slctwarriorsbtn_rect = loadImage("slctwarriorsbtn")
		screen.blit(slctwarriorsbtn, (x_begin+17*40+8,y_begin+lines*40+99))
		
		image_ids_loaded = []
		images_loaded = []
		x_begin += 1
		y_begin += 1
		j=0
		for line in position:
			i=0
			for node in line:
				
				screen.blit(grass, (x_begin+i*40,y_begin+j*40))
				
				if type(node) is list:
					if node[0] == 1:
						
						if node[2] == 0:
							screen.blit(villager1, (x_begin+i*40,y_begin+j*40))
						elif node[2][0] in [1,3,4,5]:
							screen.blit(villager1, (x_begin+i*40,y_begin+j*40))
						elif node[2][0] == 2:
							screen.blit(villager1miner, (x_begin+i*40,y_begin+j*40))
						elif node[2][0] == 6:
							screen.blit(villager1lumberjack, (x_begin+i*40,y_begin+j*40))
						
					elif node[0] == -1:
						
						if node[2] == 0:
							screen.blit(villager2, (x_begin+i*40,y_begin+j*40))
						elif node[2][0] in [1,3,4,5]:
							screen.blit(villager2, (x_begin+i*40,y_begin+j*40))
						elif node[2][0] == 2:
							screen.blit(villager2miner, (x_begin+i*40,y_begin+j*40))
						elif node[2][0] == 6:
							screen.blit(villager2lumberjack, (x_begin+i*40,y_begin+j*40))
							
					elif node[0] == 7:
						screen.blit(towncenter1, (x_begin+i*40,y_begin+j*40))
					elif node[0] == -7:
						screen.blit(towncenter2, (x_begin+i*40,y_begin+j*40))
					elif node[0] == 14:
						screen.blit(resourceplant, (x_begin+i*40,y_begin+j*40))
					elif node[0] == 15:
						screen.blit(hugeresourceplant, (x_begin+i*40,y_begin+j*40))
					elif node[0] == 16:
						screen.blit(blocks[node[5]], (x_begin+i*40,y_begin+j*40))
					elif int(node[0]) in image_ids_loaded:
						
						image_id = image_ids_loaded.index(int(node[0]))
						screen.blit(images_loaded[image_id], (x_begin+i*40,y_begin+j*40))
					else:
						
						object = self.loadObjectImage(node[0])
						screen.blit(object, (x_begin+i*40,y_begin+j*40))
						image_ids_loaded.append(int(node[0]))
						images_loaded.append(object)
						
				i+=1
			j+=1
		
		m_pos = list(pygame.mouse.get_pos())
		m_pos_x = int(m_pos[0]/40)
		m_pos_y = int((m_pos[1]-20)/40)
		if 0 <= m_pos_y < len(position):
			if position[m_pos_y][m_pos_x] != 0:
				if abs(position[m_pos_y][m_pos_x][0]) != 0 and ((position[m_pos_y][m_pos_x][0] > 0 and turn%2==0) or ((position[m_pos_y][m_pos_x][0] < 0 and turn%2!=0) or position[m_pos_y][m_pos_x][0] in [14,15,16])):
					select_info = Messages().getSelectObjectInfo(abs(position[m_pos_y][m_pos_x][0]))
					select_info_t = self.font12.render(select_info, 1, colours("white"))
					screen.blit(select_info_t, [5,5+lines*40])
					
		
		return columns,lines
		
		
		
		
		
		
	def sqQuery(self,screen,position,object,selected_objects,player1_name,player2_name,turn,sfx,lines,training_queue):
		
		#Objektin highlight
		for object1 in selected_objects:
			pygame.draw.rect(screen,colours("white"),[[object1[1][0]*40+1,object1[1][1]*40+21],[39,39]],1)
		
		object_id = object[0]
		object_coordinates = object[1]
		object_producing = False
		
		#Objektin targetin highlight
		if object_id not in [14,15,16]:
			object_target = object[2]
			if object_target != 0:
				self.orderVisualAndAudio(screen,action=object_target[0],unit=object_id,post_coordinates=object_target[1],audio=0,vivid=0)
		
		#Objektin attribuuttien julistaminen
		if object_id == 1:
			object_sprite, object_rect = loadImage("villager1info")
			object_name = "Worker"
			object_side = player1_name
			object_hp = object[4]
			object_attack = str(object[6])
			object_carry = str(object[10])
			sound = "villager"
		elif object_id == -1:
			object_sprite, object_rect = loadImage("villager2info")
			object_name = "Worker"
			object_side = player2_name
			object_hp = object[4]
			object_attack = str(object[6])
			object_carry = str(object[10])
			sound = "villager"
		elif object_id == 2:
			object_sprite, object_rect = loadImage("swordman1info")
			object_name = "Swordman"
			sound = "warrior"
		elif object_id == -2:
			object_sprite, object_rect = loadImage("swordman2info")
			object_name = "Swordman"
			sound = "warrior"
		elif object_id == 3:
			object_sprite, object_rect = loadImage("eliteswordman1info")
			object_name = "Elite swordman"
			sound = "warrior"
		elif object_id == -3:
			object_sprite, object_rect = loadImage("eliteswordman2info")
			object_name = "Elite swordman"
			sound = "warrior"
		elif object_id == 4:
			object_sprite, object_rect = loadImage("archer1info")
			object_name = "Archer"
			sound = "warrior"
		elif object_id == -4:
			object_sprite, object_rect = loadImage("archer2info")
			object_name = "Archer"
			sound = "warrior"
		elif object_id == 5:
			object_sprite, object_rect = loadImage("knight1info")
			object_name = "Knight"
			sound = "knight"
		elif object_id == -5:
			object_sprite, object_rect = loadImage("knight2info")
			object_name = "Knight"
			sound = "knight"
		elif object_id == 6:
			object_sprite, object_rect = loadImage("catapult1info")
			object_name = "Catapult"
			sound = "catapult"
		elif object_id == -6:
			object_sprite, object_rect = loadImage("catapult2info")
			object_name = "Catapult"
			sound = "catapult"
		elif object_id == 7:
			object_sprite, object_rect = loadImage("towncenter1info")
			object_name = "HQ"
			object_side = player1_name
			object_hp = object[4]
			sound = "hq"
		elif object_id == -7:
			object_sprite, object_rect = loadImage("towncenter2info")
			object_name = "HQ"
			sound = "hq"
		elif object_id == 8:
			object_sprite, object_rect = loadImage("resourcecamp1info")
			object_name = "Resource camp"
			sound = "resourcecamp"
		elif object_id == -8:
			object_sprite, object_rect = loadImage("resourcecamp2info")
			object_name = "Resource camp"
			sound = "resourcecamp"
		elif object_id == 9:
			object_sprite, object_rect = loadImage("barracks1info")
			object_name = "Barracks"
			sound = "barracks"
		elif object_id == -9:
			object_sprite, object_rect = loadImage("barracks2info")
			object_name = "Barracks"
			sound = "barracks"
		elif object_id == 10:
			object_sprite, object_rect = loadImage("archeryrangeinfo")
			object_name = "Archery range"
			sound = "archeryrange"
		elif object_id == -10:
			object_sprite, object_rect = loadImage("archeryrangeinfo")
			object_name = "Archery range"
			sound = "archeryrange"
		elif object_id == 11:
			object_sprite, object_rect = loadImage("stable1info")
			object_name = "Stable"
			sound = "stable"
		elif object_id == -11:
			object_sprite, object_rect = loadImage("stable2info")
			object_name = "Stable"
			sound = "stable"
		elif object_id == 17:
			object_sprite, object_rect = loadImage("siegeworkshopinfo")
			object_name = "Siege workshop"
			sound = "siegeworkshop"
		elif object_id == -17:
			object_sprite, object_rect = loadImage("siegeworkshopinfo")
			object_name = "Siege workshop"
			sound = "siegeworkshop"
		elif object_id == 12:
			object_sprite, object_rect = loadImage("watchtower1info")
			object_name = "Watch tower"
			sound = "watchtower"
		elif object_id == -12:
			object_sprite, object_rect = loadImage("watchtower2info")
			object_name = "Watch tower"
			sound = "watchtower"
		elif object_id == 13:
			object_sprite, object_rect = loadImage("castleinfo")
			object_name = "Castle"
			sound = "castle"
		elif object_id == -13:
			object_sprite, object_rect = loadImage("castleinfo")
			object_name = "Castle"
			sound = "castle"
		elif object_id == 14:
			object_sprite, object_rect = loadImage("resourceplantinfo")
			object_name = "Resource plant"
			object_side = ""
			object_n = str(object[2][0])
		elif object_id == 15:
			object_sprite, object_rect = loadImage("resourceplantinfo")
			object_name = "Huge resource plant"
			object_side = ""
			object_n = str(object[2][0])
		elif object_id == 16:
			object_sprite, object_rect = loadImage("blockinfo")
			object_name = "Tree"
			object_side = ""
			object_hp = object[4]
			sound = "block"
		
		#Workereiden yhtenevat attribuutit
		if abs(object_id) == 1:
			if object[2] == 0:
				object_name = "Sossupummi"
			elif object[2][0] == 1:
				object_name = "Adventurer"
			elif object[2][0] == 2:
				object_name = "Gold miner"
			elif object[2][0] == 3:
				object_name = "Builder"
			elif object[2][0] == 4:
				object_name = "Repairer"
			elif object[2][0] == 5:
				object_name = "Angry worker"
			elif object[2][0] == 6:
				object_name = "Lumberjack"
		
		#Sotilaiden yhtenevat attribuutit
		elif 2 <= object_id <= 6:
			object_side = player1_name
			object_hp = object[4]
			object_attack = str(object[6])
			object_defence = str(object[7])
		elif -2 >= object_id >= -6:
			object_side = player2_name
			object_hp = object[4]
			object_attack = str(object[6])
			object_defence = str(object[7])
		
		#Rakennuksien yhtenevat attribuutit
		elif 7 <= object_id <= 13 or object_id == 17:
			object_side = player1_name
			object_hp = object[4]
			object_finished = object[9]
		elif -7 >= object_id >= -13 or object_id == -17:
			object_side = player2_name
			object_hp = object[4]
			object_finished = object[9]
		
		#Ranged sotilaiden yhtenevat attribuutit
		if abs(object_id) in [4,6]:
			object_range = str(object[8])
		
		#Ranged rakennusten yhtenevat attribuutit
		if abs(object_id) in [12,13]:
			object_attack = str(object[6])
			object_range = str(object[8])
		
		#Yksikoiden tekemisen attribuutit
		if abs(object_id) in [7,9,10,11,17]:
			object_production = []
			for queue in training_queue[turn%2]:
				if queue[2] == object_coordinates:
					if queue[1] > 0:
						object_producing = True
						object_being_producted = int(queue[0])
						object_production_process = float(queue[3])
						object_production_amount = int(queue[1])
						object_production.append([object_being_producted,object_production_process,object_production_amount])
		
			#Yksikoiden tekemisen info
			if object[9] != -1:
				m_pos = list(pygame.mouse.get_pos())
				info = ""
				if self.btn1(m_pos,m_pos):
					info = Messages().getUnitInfo(abs(object_id),0)
				elif abs(object_id) == 9 and self.btn2(m_pos,m_pos) and Commands().returnIfUnitPossible(position,3):
					info = Messages().getUnitInfo(abs(object_id),1)
				if info != "":
					self.viewInfo(screen,lines,info)
		
		
		
		#Aaniefekti
		if not sfx and ((turn%2==0 and object_id > 0) or (turn%2!=0 and object_id < 0)) and not object_id in [14,15]:
			playSound(sound)
			sfx = True
		
		#Objektin inforuutu
		screen.blit(object_sprite, (5,27+lines*40))
		
		#Objektin nimi ja puoli
		object_name = self.font12.render(object_name, 1, colours("white"))
		screen.blit(object_name, [8,28+lines*40])
		object_side = self.font12.render(object_side, 1, colours("white"))
		screen.blit(object_side, [8,40+lines*40])
		
		#Worker
		if abs(object_id) == 1:
			
			#Kaikille nakyvat ominaisuudet
			object_attack = self.font12.render(object_attack, 1, colours("white"))
			screen.blit(object_attack, [96,60+lines*40])
			
			#Oma vai vastustajan
			if (object_id == 1 and turn%2==0) or (object_id == -1 and turn%2!=0):
				villagerworkbtn_sprite, villager_btn_sprite_rect = loadImage("villagerworkbtn")
				screen.blit(villagerworkbtn_sprite, (135,25+lines*40))
				
				#Resurssien kanto
				if int(object_carry) > 0:
					resource_sprite, resource_sprite_rect = loadImage("resource")
					screen.blit(resource_sprite, (59,79+lines*40))
					object_carry = self.font12.render(object_carry, 1, colours("white"))
					screen.blit(object_carry, [96,81+lines*40])
		
		#Kaikki sotilaat
		if 2 <= abs(object_id) <= 6:
			
			#Kaikille nakyvat ominaisuudet
			object_attack_txt = self.font12.render(object_attack, 1, colours("white"))
			screen.blit(object_attack_txt, [96,60+lines*40])
			object_defence = self.font12.render(object_defence, 1, colours("white"))
			screen.blit(object_defence, [96,81+lines*40])
		
		#Kaikki sotilaat, joilla ranged attack
		if abs(object_id) in [4,6]:
			object_range_txt = self.font12.render(object_range, 1, colours("white"))
			screen.blit(object_range_txt, [96,102+lines*40])
		
		#HQ
		if ((object_id == 7 and turn%2==0) or (object_id == -7 and turn%2!=0)) and object_finished != -1:
			
			#Workerin teko -nappi
			worker_btn_sprite, worker_btn_sprite_rect = loadImage("crtvillager"+str(turn%2+1)+"btn")
			screen.blit(worker_btn_sprite, (135,25+lines*40))
		
		#Barracks
		elif ((object_id == 9 and turn%2==0) or (object_id == -9 and turn%2!=0)) and object_finished != -1:
			
			disable1 = False
			disable2 = False
			for obj_prod in object_production:
				if abs(obj_prod[0]) == 2:
					disable2 = True
				elif abs(obj_prod[0]) == 3:
					disable1 = True
			
			if not disable1:
				#Swordmanin teko -nappi
				swordman_btn_sprite, swordman_btn_sprite_rect = loadImage("crtswordman"+str(turn%2+1)+"btn")
				screen.blit(swordman_btn_sprite, (135,25+lines*40))
			
			if not disable2:
				#Elite swordmanin teko -nappi
				if Commands().returnIfUnitPossible(position,3*(turn%2*-2+1)):
					eliteswordman_btn_sprite, swordman_btn_sprite_rect = loadImage("crteliteswordman"+str(turn%2+1)+"btn")
					screen.blit(eliteswordman_btn_sprite, (135+54,25+lines*40))
		
		#Archery range
		elif ((object_id == 10 and turn%2==0) or (object_id == -10 and turn%2!=0)) and object_finished != -1:
			
			#Archerin teko -nappi
			archer_btn_sprite, archer_btn_sprite_rect = loadImage("crtarcher"+str(turn%2+1)+"btn")
			screen.blit(archer_btn_sprite, (135,25+lines*40))
		
		#Stable
		elif ((object_id == 11 and turn%2==0) or (object_id == -11 and turn%2!=0)) and object_finished != -1:
			
			#Knightin teko -nappi
			knight_btn_sprite, knight_btn_sprite_rect = loadImage("crtknight"+str(turn%2+1)+"btn")
			screen.blit(knight_btn_sprite, (135,25+lines*40))
		
		#Siege workshop
		elif ((object_id == 17 and turn%2==0) or (object_id == -17 and turn%2!=0)) and object_finished != -1:
			
			#Catapultin teko -nappi
			catapult_btn_sprite, catapult_btn_sprite_rect = loadImage("crtcatapult"+str(turn%2+1)+"btn")
			screen.blit(catapult_btn_sprite, (135,25+lines*40))
		
		#Kesken oleva rakennus
		elif abs(object_id) in [7,8,9,10,11,12,13,17] and ((object_id > 0 and turn%2==0) or (object_id < 0 and turn%2!=0)) and object_finished == -1:
			
			#Prosentit rakennuksen valmiudelle
			hourglass_sprite, hourglass_sprite_rect = loadImage("hourglass")
			screen.blit(hourglass_sprite, (61,57+lines*40))
			object_production_process = self.font12.render(str(int(object_hp[0]*1.0/object_hp[1]*100))+"%", 1, colours("white"))
			screen.blit(object_production_process, [96,60+lines*40])
			
			#Rakennuksen perumisnappi
			stopbtn_sprite, stopbtn_sprite_rect = loadImage("stopbtn")
			screen.blit(stopbtn_sprite, (135,25+lines*40))
		
		#Yksikoiden tekemisen info
		if ((object_id > 0 and turn%2==0) or (object_id < 0 and turn%2!=0)) and abs(object_id) in [7,9,10,11,17] and object_producing:
			
			hourglass_sprite, hourglass_sprite_rect = loadImage("hourglass")
			screen.blit(hourglass_sprite, (61,57+lines*40))
			
			for obj_prod in object_production:
				if abs(obj_prod[0]) in [1,2,4,5,6]:
					i=0
				elif abs(obj_prod[0]) == 3:
					i=1
				object_production_process = self.font12.render(str(int(obj_prod[1]*100))+"%", 1, colours("white"))
				screen.blit(object_production_process, [96,60+lines*40])
				object_production_amount = self.font12.render(str(obj_prod[2]), 1, colours("white"))
				screen.blit(object_production_amount, [137+54*i,27+lines*40])
		
		#Kaikki rakennukset, joilla ranged attack
		if abs(object_id) in [12,13] and object_finished != -1:
			defstructicons_sprite, defstructicons_sprite_rect = loadImage("defstructicons")
			screen.blit(defstructicons_sprite, (61,57+lines*40))
			object_attack_txt = self.font12.render(object_attack, 1, colours("white"))
			screen.blit(object_attack_txt, [96,60+lines*40])
			object_range_txt = self.font12.render(object_range, 1, colours("white"))
			screen.blit(object_range_txt, [96,81+lines*40])
		
		#Kaikki paitsi resurssikimpaleet
		if not (object_id == 14 or object_id == 15):
			
			#HP-palkki inforuutuun
			pygame.draw.rect(screen,colours("brightgreen"),[[8,lines*40+111],[int(50*(float(object_hp[0])/float(object_hp[1]))),7]],0)
			object_hp_str = str(object_hp[0])+"/"+str(object_hp[1])
			object_hp_str = self.font12.render(object_hp_str, 1, colours("white"))
			screen.blit(object_hp_str, [7,117+lines*40])
		
		else:
			
			#Resurssien maara resurssikimpaleessa
			object_n = self.font12.render(object_n, 1, colours("white"))
			screen.blit(object_n, [95,62+lines*40])
		
		#HP-palkkien piirtaminen valittujen objektien ylapuolelle
		for object1 in selected_objects:
			if object1[0] != 14 and object1[0] != 15:
				pygame.draw.rect(screen,colours("red"),[[object1[1][0]*40+10,object1[1][1]*40+22],[20,2]],0)
				pygame.draw.rect(screen,colours("brightgreen"),[[object1[1][0]*40+10,object1[1][1]*40+22],[int(20*(float(object1[4][0])/float(object1[4][1]))),2]],0)
		
		#Hiiren leijuminen infobarin paalla
		m_pos = list(pygame.mouse.get_pos())
		if abs(object_id) == 1 and self.btn1(m_pos,m_pos):
			button_info = Messages().getButtonInfo(0)
			button_info_t = self.font12.render(button_info, 1, colours("white"))
			screen.blit(button_info_t, [5,5+lines*40])
		
		return sfx
	
	
	
	
	
	
	def orderVisualAndAudio(self,screen,action,unit,post_coordinates,audio,vivid):
		
		colours1_list = ["brightgreen","yellow","blue","purple","red","maroon"]
		colours2_list = ["lightgreen","lightyellow3","lightblue","orchid","tomato","firebrick"]
		if vivid:
			colours_list = colours1_list
		else:
			colours_list = colours2_list
		
		if action == 1:
			
			if vivid:
				arrows_sprite, arrows_sprite_rect = loadImage("arrows")
				screen.blit(arrows_sprite, (2+post_coordinates[0]*40,27+post_coordinates[1]*40))
			pygame.draw.rect(screen,colours(colours_list[0]),[[1+post_coordinates[0]*40,21+post_coordinates[1]*40],[39,39]],1)
			if audio:
				Commands().playGoingSound(abs(unit))
		
		elif action == 2:
			
			pygame.draw.rect(screen,colours(colours_list[1]),[[1+post_coordinates[0]*40,21+post_coordinates[1]*40],[39,39]],1)
			if audio:
				playSound("villagergomining")
		
		elif action == 3:
			
			pygame.draw.rect(screen,colours(colours_list[2]),[[1+post_coordinates[0]*40,21+post_coordinates[1]*40],[39,39]],1)
			if audio:
				playSound("villagergobuilding")
		
		elif action == 4:
			
			pygame.draw.rect(screen,colours(colours_list[3]),[[1+post_coordinates[0]*40,21+post_coordinates[1]*40],[39,39]],1)
			if audio:
				playSound("villagergorepairing")
			
		elif action == 5:
			
			pygame.draw.rect(screen,colours(colours_list[4]),[[1+post_coordinates[0]*40,21+post_coordinates[1]*40],[39,39]],1)
			if audio:
				Commands().playGoingSound(abs(unit))
		
		elif action == 6:
			
			pygame.draw.rect(screen,colours(colours_list[5]),[[1+post_coordinates[0]*40,21+post_coordinates[1]*40],[39,39]],1)
			if audio:
				playSound("villagergochopping")
	
	def drawAction(self,screen,action,object,object_pos,target,target_pos):
		
		if abs(object[0]) == 1 and object[2] != 0:
			if object[2][0] == 2:
				object_id = object[0] * 1.1
			elif object[2][0] == 6:
				object_id = object[0] * 1.2
			else:
				object_id = object[0]
		else:
			object_id = object[0]
		
		if action == 1: #Movement
			
			grass = self.loadObjectImage(0)
			screen.blit(grass, (1+object_pos[0]*40,21+object_pos[1]*40))
			object = self.loadObjectImage(object_id)
			screen.blit(object, (1+target_pos[0]*40,21+target_pos[1]*40))
			
			pygame.draw.rect(screen,colours("silver"),[[object_pos[0]*40+1,object_pos[1]*40+21],[39,39]],1)
			pygame.draw.rect(screen,colours("white"),[[target_pos[0]*40+1,target_pos[1]*40+21],[39,39]],1)
			
			pygame.display.flip()
			sleep(0.1)
			
			screen.blit(grass, (1+object_pos[0]*40,21+object_pos[1]*40))
			screen.blit(object, (1+target_pos[0]*40,21+target_pos[1]*40))
		
		elif action == 2: #Mining
			
			grass = self.loadObjectImage(0)
			object = self.loadObjectImage(object_id)
			screen.blit(grass, (1+object_pos[0]*40,21+object_pos[1]*40))
			screen.blit(object, (1+object_pos[0]*40+Commands().getSprMvmnt(object_pos,target_pos,0),21+object_pos[1]*40+Commands().getSprMvmnt(object_pos,target_pos,1)))
			pygame.draw.rect(screen,colours("white"),[[object_pos[0]*40+1,object_pos[1]*40+21],[39,39]],1)
			pygame.draw.rect(screen,colours("lightyellow3"),[[target_pos[0]*40+1,target_pos[1]*40+21],[39,39]],1)
			
			pygame.display.flip()
			sleep(0.05)
			
			screen.blit(grass, (1+object_pos[0]*40,21+object_pos[1]*40))
			screen.blit(object, (1+object_pos[0]*40,21+object_pos[1]*40))
			pygame.draw.rect(screen,colours("white"),[[object_pos[0]*40+1,object_pos[1]*40+21],[39,39]],1)
			pygame.draw.rect(screen,colours("lightyellow3"),[[target_pos[0]*40+1,target_pos[1]*40+21],[39,39]],1)
			
			pygame.display.flip()
			sleep(0.05)
			
			screen.blit(grass, (1+object_pos[0]*40,21+object_pos[1]*40))
			screen.blit(object, (1+object_pos[0]*40,21+object_pos[1]*40))
			object = self.loadObjectImage(target[0])
			screen.blit(grass, (1+target_pos[0]*40,21+target_pos[1]*40))
			screen.blit(object, (1+target_pos[0]*40,21+target_pos[1]*40))
			
		elif action == 3: #Build
			
			pygame.draw.rect(screen,colours("white"),[[object_pos[0]*40+1,object_pos[1]*40+21],[39,39]],1)
			pygame.draw.rect(screen,colours("blue"),[[target_pos[0]*40+1,target_pos[1]*40+21],[39,39]],1)
			pygame.draw.rect(screen,colours("red"),[[target_pos[0]*40+10,target_pos[1]*40+22],[20,2]],0)
			pygame.draw.rect(screen,colours("brightgreen"),[[target_pos[0]*40+10,target_pos[1]*40+22],[int(20*(float(target[4][0])/float(target[4][1]))),2]],0)
			
			pygame.display.flip()
			sleep(0.1)
			
			object = self.loadObjectImage(object_id)
			screen.blit(object, (1+object_pos[0]*40,21+object_pos[1]*40))
			object = self.loadObjectImage(target[0])
			screen.blit(object, (1+target_pos[0]*40,21+target_pos[1]*40))
			pygame.draw.rect(screen,colours("red"),[[target_pos[0]*40+10,target_pos[1]*40+22],[20,2]],0)
			pygame.draw.rect(screen,colours("brightgreen"),[[target_pos[0]*40+10,target_pos[1]*40+22],[int(20*(float(target[4][0])/float(target[4][1]))),2]],0)
			
		elif action == 4: #Repair
			
			pygame.draw.rect(screen,colours("white"),[[object_pos[0]*40+1,object_pos[1]*40+21],[39,39]],1)
			pygame.draw.rect(screen,colours("purple"),[[target_pos[0]*40+1,target_pos[1]*40+21],[39,39]],1)
			pygame.draw.rect(screen,colours("red"),[[target_pos[0]*40+10,target_pos[1]*40+22],[20,2]],0)
			pygame.draw.rect(screen,colours("brightgreen"),[[target_pos[0]*40+10,target_pos[1]*40+22],[int(20*(float(target[4][0])/float(target[4][1]))),2]],0)
			
			pygame.display.flip()
			sleep(0.1)
			
			object = self.loadObjectImage(object_id)
			screen.blit(object, (1+object_pos[0]*40,21+object_pos[1]*40))
			object = self.loadObjectImage(target[0])
			screen.blit(object, (1+target_pos[0]*40,21+target_pos[1]*40))
			pygame.draw.rect(screen,colours("red"),[[target_pos[0]*40+10,target_pos[1]*40+22],[20,2]],0)
			pygame.draw.rect(screen,colours("brightgreen"),[[target_pos[0]*40+10,target_pos[1]*40+22],[int(20*(float(target[4][0])/float(target[4][1]))),2]],0)
			
		elif action == 5: #Combat
			
			pygame.draw.rect(screen,colours("white"),[[object_pos[0]*40+1,object_pos[1]*40+21],[39,39]],1)
			pygame.draw.rect(screen,colours("red"),[[target_pos[0]*40+1,target_pos[1]*40+21],[39,39]],1)
			
			if len(target[4]) > 1:
				pygame.draw.rect(screen,colours("red"),[[target_pos[0]*40+10,target_pos[1]*40+22],[20,2]],0)
				pygame.draw.rect(screen,colours("brightgreen"),[[target_pos[0]*40+10,target_pos[1]*40+22],[int(20*(float(target[4][0])/float(target[4][1]))),2]],0)
				
				pygame.display.flip()
				sleep(0.15)
				
				object = self.loadObjectImage(object_id)
				screen.blit(object, (1+object_pos[0]*40,21+object_pos[1]*40))
				object = self.loadObjectImage(target[0])
				screen.blit(object, (1+target_pos[0]*40,21+target_pos[1]*40))
				
				pygame.draw.rect(screen,colours("red"),[[target_pos[0]*40+10,target_pos[1]*40+22],[20,2]],0)
				pygame.draw.rect(screen,colours("brightgreen"),[[target_pos[0]*40+10,target_pos[1]*40+22],[int(20*(float(target[4][0])/float(target[4][1]))),2]],0)
				
			else:
				pygame.draw.rect(screen,colours("red"),[[target_pos[0]*40+10,target_pos[1]*40+22],[20,2]],0)
				
				pygame.display.flip()
				sleep(0.15)
				
				grass = self.loadObjectImage(0)
				screen.blit(grass, (1+target_pos[0]*40,21+target_pos[1]*40))
		
		elif action == 6: #Chopping
			
			pygame.draw.rect(screen,colours("white"),[[object_pos[0]*40+1,object_pos[1]*40+21],[39,39]],1)
			pygame.draw.rect(screen,colours("maroon"),[[target_pos[0]*40+1,target_pos[1]*40+21],[39,39]],1)
			pygame.draw.rect(screen,colours("red"),[[target_pos[0]*40+10,target_pos[1]*40+22],[20,2]],0)
			if target[4][0] > 0:
				pygame.draw.rect(screen,colours("brightgreen"),[[target_pos[0]*40+10,target_pos[1]*40+22],[int(20*(float(target[4][0])/float(target[4][1]))),2]],0)
			
			pygame.display.flip()
			sleep(0.1)
			
			if target[4][0] > 0:
				object = self.loadObjectImage(object_id)
				screen.blit(object, (1+object_pos[0]*40,21+object_pos[1]*40))
				object = self.loadObjectImage(target[0]*1.0+target[5]*1.0/10)
				screen.blit(object, (1+target_pos[0]*40,21+target_pos[1]*40))
				pygame.draw.rect(screen,colours("red"),[[target_pos[0]*40+10,target_pos[1]*40+22],[20,2]],0)
				pygame.draw.rect(screen,colours("brightgreen"),[[target_pos[0]*40+10,target_pos[1]*40+22],[int(20*(float(target[4][0])/float(target[4][1]))),2]],0)
			else:
				grass = self.loadObjectImage(0)
				screen.blit(grass, (1+target_pos[0]*40,21+target_pos[1]*40))
	
		elif action == 7: #Unit spawn
			
			object = self.loadObjectImage(object_id)
			screen.blit(object, (1+object_pos[0]*40,21+object_pos[1]*40))
			pygame.draw.rect(screen,colours("white"),[[object_pos[0]*40+1,object_pos[1]*40+21],[39,39]],1)
			
			pygame.display.flip()
			sleep(0.2)
			
			screen.blit(object, (1+object_pos[0]*40,21+object_pos[1]*40))
	
	def viewInfo(self,screen,lines,info):
		
		info = info.splitlines()
		if len(info)==4:
			extra="12"
		elif len(info)==5:
			extra="24"
		else:
			extra="0"
		
		building_info_bg, building_info_bg_rect = loadImage("buildinginfobg"+extra)
		screen.blit(building_info_bg, (0,-int(extra)-20+lines*40))
		
		info_t = []
		for line in info:
			info_t.append(self.font12.render(line, 1, colours("white")))
		i=0
		for text in info_t:
			screen.blit(text, [5,-int(extra)-20+lines*40+i*12])
			i+=1
	
	def buildingView1(self,screen,lines,turn):
		
		bld_hq_btn_sprite, bld_hq_btn_sprite_rect = loadImage("bldtowncenterbtn")
		screen.blit(bld_hq_btn_sprite, (135+54*0,25+lines*40))
		bld_resourcecamp_btn_sprite, bld_resourcecamp_btn_sprite_rect = loadImage("bldresourcecamp"+str(turn)+"btn")
		screen.blit(bld_resourcecamp_btn_sprite, (135+54*1,25+lines*40))
		bld_barracks_btn_sprite, bld_barracks_btn_sprite_rect = loadImage("bldbarracks"+str(turn)+"btn")
		screen.blit(bld_barracks_btn_sprite, (135+54*2,25+lines*40))
		bld_archeryrange_btn_sprite, bld_archeryrange_btn_sprite_rect = loadImage("bldarcheryrangebtn")
		screen.blit(bld_archeryrange_btn_sprite, (135+54*3,25+lines*40))
		bld_stable_btn_sprite, bld_stable_btn_sprite_rect = loadImage("bldstable"+str(turn)+"btn")
		screen.blit(bld_stable_btn_sprite, (135+54*4,25+lines*40))
		bld_siegeworkshop_btn_sprite, bld_siegeworkshop_btn_sprite_rect = loadImage("bldsiegeworkshopbtn")
		screen.blit(bld_siegeworkshop_btn_sprite, (135+54*5,25+lines*40))
		bld_watchtower_btn_sprite, bld_watchtower_btn_sprite_rect = loadImage("bldwatchtower"+str(turn)+"btn")
		screen.blit(bld_watchtower_btn_sprite, (135+54*0,80+lines*40))
		bld_castle_btn_sprite, bld_castle_btn_sprite_rect = loadImage("bldcastlebtn")
		screen.blit(bld_castle_btn_sprite, (135+54*1,80+lines*40))
		
		m_pos = list(pygame.mouse.get_pos())
		info = ""
		if self.btn1(m_pos,m_pos):
			info = Messages().getBuildingInfo(0)
		elif self.btn2(m_pos,m_pos):
			info = Messages().getBuildingInfo(1)
		elif self.btn3(m_pos,m_pos):
			info = Messages().getBuildingInfo(2)
		elif self.btn4(m_pos,m_pos):
			info = Messages().getBuildingInfo(3)
		elif self.btn5(m_pos,m_pos):
			info = Messages().getBuildingInfo(4)
		elif self.btn6(m_pos,m_pos):
			info = Messages().getBuildingInfo(5)
		elif self.btn11(m_pos,m_pos):
			info = Messages().getBuildingInfo(6)
		elif self.btn12(m_pos,m_pos):
			info = Messages().getBuildingInfo(7)
		if info != "":
			
			self.viewInfo(screen,lines,info)
	
	def buildingView2(self,screen,lines,position,turn,constructable_building):
		
		cancel_btn_sprite, cancel_btn_sprite_rect = loadImage("cancelbtn")
		screen.blit(cancel_btn_sprite, (135+54*5,80+lines*40))
		no_btn1_sprite, no_btn1_sprite_rect = loadImage("nobtn1")
		screen.blit(no_btn1_sprite, (135+54*0,25+lines*40))
		
		m_pos = list(pygame.mouse.get_pos())
		m_pos_x = int(m_pos[0]/40)
		m_pos_y = int((m_pos[1]-20)/40)
		if 0 <= m_pos_y < len(position):
			if position[m_pos_y][m_pos_x] == 0:
				building_sprite, building_sprite_rect = loadImage(Commands().intToBuildingSpriteName(constructable_building)+str(turn))
				greentrans,greentrans_rect = loadImage("greentrans")
				screen.blit(building_sprite, (m_pos_x*40+1,m_pos_y*40+21))
				screen.blit(greentrans, (m_pos_x*40+1,m_pos_y*40+21))
			else:
				building_sprite, building_sprite_rect = loadImage(Commands().intToBuildingSpriteName(constructable_building)+str(turn))
				redtrans,redtrans_rect = loadImage("redtrans")
				screen.blit(building_sprite, (m_pos_x*40+1,m_pos_y*40+21))
				screen.blit(redtrans, (m_pos_x*40+1,m_pos_y*40+21))
	
	def notifScreen(self,screen,columns,lines,trans):
		
		notifscreen_sprite, notifscreen_sprite_rect = loadImage("notifscreen" + trans)
		screen.blit(notifscreen_sprite, (int(columns*20)-225,int(lines*20)-30))
	
	def waitingScreen(self,screen,columns,lines):
		
		self.notifScreen(screen,columns,lines,"trans")
		
		text = self.font24.render("Processing to the next turn...", 1, colours("white"))
		screen.blit(text,[int(columns*20)-120,int(lines*20)+5])
		
	def winningScreen(self,screen,columns,lines,player_name,winning_method):
		
		self.notifScreen(screen,columns,lines,"")
		
		if len(player_name) + len(winning_method) < 25:
			font_size = 24
		else:
			font_size = 24 - ((len(player_name) + len(winning_method) - 25) / 2)
		font = pygame.font.Font(pygame.font.match_font('arial'), font_size)
		
		text = font.render("Player " + player_name + " wins by " + winning_method + "!", 1, colours("white"))
		screen.blit(text,[int(columns*20)-(160+(len(player_name)+len(winning_method))/2),int(lines*20)+5])