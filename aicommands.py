class AICommands:
	
	def __init__(self):
		pass
		
		
		
	def calcAvgCrds(self,objects):
		'''Input: Objects list
		   Output: Average coordinates of the given objects list
		'''
		x = 0
		y = 0
		i=0
		for object in objects:
			x += int(object[1][0])
			y += int(object[1][1])
			i+=1
		x = int(x/i)
		y = int(y/i)
		return [x,y]
	
	def calcAvgCrdsFloat(self,group):
		'''Input: group is a list containing the objects. Objects' 2nd item (object[1]) must be its coordinates
		   Output: the calculated central coordinates as a list of floats
		'''
		cntr_crds = [0,0]
		for member in group:
			cntr_crds[0] += int(member[1][0])
			cntr_crds[1] += int(member[1][1])
		cntr_crds[0] = (cntr_crds[0]*1.0)/(len(group)*1.0)
		cntr_crds[1] = (cntr_crds[1]*1.0)/(len(group)*1.0)
		return cntr_crds
	
	def calcAvgCrdsExtended(self,OBJECTS):
		'''Input: List of objects lists, objects = [factor,object]
		   Output: Average coordinates of the given objects list
		'''
		x = 0.0
		y = 0.0
		i=0.0
		for objects in OBJECTS:
			for object in objects[1]:
				x += object[1][0]*objects[0]
				y += object[1][1]*objects[0]
				i+=1*objects[0]
		x = int(x/i)
		y = int(y/i)
		return [x,y]
	
	
	def launchUnitProductionAI(self,unit,amount,structure,turn):
		possibility = 0
		prod_instructs = []
		if ((structure[0] > 0 and turn%2==0) or (structure[0] < 0 and turn%2!=0)) and abs(structure[0]) in [7,9,10,11,17]:
			if structure[9] != -1:
				possibility = 1
				prod_instructs = [abs(unit),structure[1],amount] #[villager, from coordinates of the town center, the defined amount]
		return possibility, prod_instructs
	
	
	
	def getClosestCombination(self,members,group_size):
		'''members is a list containing the objects that are acceptable to the combination
		   group_size is the size of the combination. List that contains possible sizes is an acceptable input
		   Returns the best combination from the members
		'''
		groups = []
		m = members
		if type(group_size) is list:
			for node in group_size:
				if node == 2:
					s2 = True
				if node == 3:
					s3 = True
		else:
			if group_size == 2:
				s2 = True
			elif group_size == 3:
				s3 = True
		
		if s2:
			for i in range(len(m)):
				for j in range(len(m)):
					if i != j:
						groups.append([list(m[int(i)]),list(m[int(j)])])
		if s3:
			for i in range(len(m)):
				for j in range(len(m)):
					for k in range(len(m)):
						if i != j or j != k or i != k:
							groups.append([list(m[int(i)]),list(m[int(j)]),list(m[int(k)])])
		
		dist = 1000
		i=0
		for group in groups:
			cntr_crds = self.calcAvgCrdsFloat(group)
			temp_dist = 0
			for member in group:
				temp_dist += (abs(cntr_crds[0]-member[1][0]*1.0)+abs(cntr_crds[1]-member[1][1]*1.0))
			if len(group) > 2:
				temp_dist *= (2.0/len(group)*1.0)
			if temp_dist < dist:
				g_i = int(i)
				dist = int(temp_dist)
			i+=1
		if groups:
			comb = list(groups[g_i])
		else:
			comb = [members[0]] #If the group_size is larger than the amount of members, return the first member
		return comb
	
	def getAllObjectsInRange(self,object_id,target,range,position):
		'''Input: object_id is the id of the wanted objects. object_id may be a list containing the acceptable ids
		          target is the coordinates that are set as the origo
		          range is the amount of squares vertically + horizontally from the origo where the target is acceptable to be
		          position
		   Output: list of the objects
		'''
		objects = []
		if type(object_id) is not list:
			object_id = [object_id]
		for row in position:
			for node in row:
				if node != 0:
					if node[0] in object_id and abs(node[1][0]-target[0])+abs(node[1][1]-target[1]) <= range:
						objects.append(list(node))
		return objects
	
	def getClosestObject(self,crds,object_id,position,min_distance=1000):
		'''Returns an object with the corresponding object_id closest to the given coordinates in the given position
		   Object_id can be a list, which contains the acceptable object_ids
		'''
		
		objects = []
		for line in position:
			for node in line:
				if node != 0:
					if type(object_id) is list:
						for obj_id in object_id:
							if node[0] == obj_id:
								objects.append(list(node))
					elif node[0] == object_id:
						objects.append(list(node))
		
		min_object = []
		for object in objects:
			if abs(object[1][0]-crds[0])+abs(object[1][1]-crds[1]) < min_distance:
				min_distance = int(abs(object[1][0]-crds[0])+abs(object[1][1]-crds[1]))
				min_object = list(object)
		
		return min_object
	
	def getClosestOfObjects(self,crds,objects):
		min_distance = 1000
		min_object = []
		for object in objects:
			if abs(object[1][0]-crds[0])+abs(object[1][1]-crds[1]) < min_distance:
				min_distance = int(abs(object[1][0]-crds[0])+abs(object[1][1]-crds[1]))
				min_object = list(object)
		
		return min_object
	
	def getClosestWorker(self,player_in_turn_index,coordinates,position):
		#TODO
		workers = []
		i=0
		for row in position:
			j=0
			for node in row:
				if type(node) is list:
					if node[0] == player_in_turn_index and type(node[2]) == type(0):
						workers.append([int(i),int(j)])
					elif node[0] == player_in_turn_index and type(node[2]) is list:
						if node[2][0] == 1:
							workers.append([int(i),int(j)])
				j+=1
			i+=1
		
		if len(workers) == 0:
			i=0
			for row in position:
				j=0
				for node in row:
					if type(node) is list:
						if node[0] == player_in_turn_index and type(node[2]) is list:
							if node[2][0] == 2:
								workers.append([int(i),int(j)])
					j+=1
				i+=1
		
		min_distance = 1000
		min_coordinates = [-1,-1]
		for worker in workers:
			worker.reverse()
			if abs(worker[0]-coordinates[0])+abs(worker[1]-coordinates[1]) < min_distance:
				min_distance = int(abs(worker[0]-coordinates[0])+abs(worker[1]-coordinates[1]))
				min_coordinates = list(worker)
		
		return min_coordinates
	
	def getChasers(self,crds,position):
		chasers = []
		for line in position:
			for node in line:
				if node != 0:
					if abs(node[0]) in [1,2,3,4,5,6]:
						if node[2] != 0:
							if node[2] == [5,crds]:
								chasers.append(list(node))
		return chasers