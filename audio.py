import pygame,sys,os
from time import sleep
from random import random

def playMusic(music_index):
	path = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
	music = ["aoe2thememetal.wav","empires01.wav"]
	pygame.mixer.init()
	pygame.mixer.music.load(path + '\\audio_data\\music\\' + music[music_index])
	pygame.mixer.music.set_volume(1)
	pygame.mixer.music.play(-1)
	pygame.mixer.music.stop()

def playSound(sound_name):
	
	path = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
	
	'''If the sound name is marked with square brackets [], then the sound is random (=multiple sounds possible).
	   If there is D before the sound name, it means that there are duplicates of it in the directory.
	
		Sounds:
			Object selection (and creation):
				[villager] 5
				[warrior] 2
				[knight] 2
				D catapult
				hq
				resourcecamp
				barracks
				archeryrange
				stable
				siegeworkshop
				watchtower
				castle
				block
			Object creation:
				workerspawn
				warriorspawn
				D catapult
			Villager actions:
				villagergo
				villagergomining
				villagergobuilding
				villagergorepairing
				D [villagerfight] 3
				villagermining
				villagerbuildrepair
			Other unit actions:
				[warriorgo] 3
				knightgo
				catapultgo
			Attack sounds:
				Hand-to-hand fighting sounds:
					D [villagerfight] 3
					[warriorfight] 5
				Ranged attack sounds:
					[archershoot] 3
					castleshoot
					catapultshoot
			Dying/destruction sounds:
				[die] 9
				[knightdie] 1
				catapultdie
				destruction
				castledestruction
			Misc:
				button1
				button2
				impossible
				win
	'''
	
	#Random sounds' lengths
	rsl = [5,2,2,3,3,5,3,9,1]
	#Random sounds' names
	rsn = ["villager","warrior","knight","warriorgo","villagerfight","warriorfight","archershoot","die","knightdie"]
	
	if sound_name in rsn:
		rsl_id = rsn.index(sound_name)
		sound_name += str(int(random()*rsl[rsl_id]+1))
	
	effect = pygame.mixer.Sound(path + '\\audio_data\\sfx\\' + sound_name + '.wav')
	effect.set_volume(1)
	effect.play()