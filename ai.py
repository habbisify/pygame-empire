from time import sleep
from random import random,choice,shuffle
from math import pow

from aicommands import *
from aimsg import *
from pathing import *
from features import *
from audio import playSound

class AI:
	def __init__(self):
		self.squares = 0
		self.hqs = []
		self.enemy_hqs = []
		self.resourcecamps = []
		self.enemy_resourcecamps = []
		self.barracks = []
		self.enemy_barracks = []
		self.archeryranges = []
		self.enemy_archeryranges = []
		self.stables = []
		self.enemy_stables = []
		self.siegeworkshops = []
		self.enemy_siegeworkshops = []
		self.watchtowers = []
		self.enemy_watchtowers = []
		self.castles = []
		self.enemy_castles = []
		self.workers = []
		self.goldminers = []
		self.far_goldminers = []
		self.enemy_workers = []
		self.swordmen = []
		self.enemy_swordmen = []
		self.eliteswordmen = []
		self.enemy_eliteswordmen = []
		self.archers = []
		self.enemy_archers = []
		self.knights = []
		self.enemy_knights = []
		self.catapults = []
		self.enemy_catapults = []
		self.all_resourceplants = []
		self.blocks = []
		self.empty_sqs = []

	def selectBldCrds(self,bld,aggro,style,position):

		enemy_units = [self.enemy_workers,self.enemy_swordmen,self.enemy_eliteswordmen,self.enemy_archers,self.enemy_knights,self.enemy_catapults,self.enemy_watchtowers,self.enemy_castles]

		aggro = choice(aggro)
		style = choice(style)

		crds = []
		if bld in [9,10,11,12,13,17]:
			factored_objects1 = [[5.0,self.hqs],[1.0,self.resourcecamps],[2.0,self.barracks],[2.0,self.archeryranges],[2.0,self.stables],[2.0,self.siegeworkshops]]
			crds1 = AICommands().calcAvgCrdsExtended(factored_objects1)
			factored_objects2 = [[5.0,self.enemy_hqs],[1.0,self.enemy_resourcecamps],[2.0,self.enemy_barracks],[2.0,self.enemy_archeryranges],[2.0,self.enemy_stables],[2.0,self.enemy_siegeworkshops]]
			crds2 = AICommands().calcAvgCrdsExtended(factored_objects2)
			if -5 <= aggro <= 5:
				aggro_factory = pow((aggro+5)/5,2)+0.2
				x = int((crds2[0]*aggro_factory+crds1[0])/(aggro_factory+1)+random()*4.0-2.0)
				y = int((crds2[1]*aggro_factory+crds1[1])/(aggro_factory+1)+random()*4.0-2.0)
				ideal_crds = [x,y]
				if bld in [12,13]:
					r=Features().watchtowerRange()
					if x<r:
						x=r
					elif x>len(position[0])-(r+1):
						x=int(len(position[0])-(r+1))
					if y<r:
						y=r
					elif y>len(position)-(r+1):
						y=int(len(position)-(r+1))

		#TODO
		if bld in [7]:
			ideal_crds = [0,0]
		if bld in [8]:

			if self.far_goldminers:
				goldminers = self.far_goldminers
			else:
				goldminers = self.goldminers
			goldminer_group = AICommands().getClosestCombination(goldminers,[2,3])
			close_goldmines = AICommands().getAllObjectsInRange([14,15],AICommands().calcAvgCrds(goldminer_group),4,position)

			factored_objects = [[1.0,close_goldmines],[1.0,goldminer_group]]
			crds = AICommands().calcAvgCrdsExtended(factored_objects)
			ideal_crds = list(crds)

		good_sqs = []
		for s in self.empty_sqs:
			good = True
			for enemy_unit_type in enemy_units:
				for enemy_unit in enemy_unit_type:
					if enemy_unit[0] in [4,12,13]:
						if abs(s[0] - enemy_unit[1][0]) + abs(s[1] - enemy_unit[1][1]) <= 4:
							good = False
					elif enemy_unit[0] in [6]:
						if abs(s[0] - enemy_unit[1][0]) + abs(s[1] - enemy_unit[1][1]) <= 5:
							good = False
					else:
						if not (abs(s[0] - enemy_unit[1][0]) > 1 or abs(s[1] - enemy_unit[1][1]) > 1):
							good = False
			if bld not in [7,8]:
				for goldminer in self.goldminers:
					if abs(s[0] - goldminer[1][0]) + abs(s[1] - goldminer[1][1]) <= 1:
						good = False
			if bld in [7,8]:
				for resourcecamp in self.resourcecamps:
					if not (abs(s[0] - resourcecamp[1][0]) > 1 or abs(s[1] - resourcecamp[1][1]) > 1):
						good = False
				for hq in self.hqs:
					if not (abs(s[0] - hq[1][0]) > 1 or abs(s[1] - hq[1][1]) > 1):
						good = False
			for hq in self.hqs:
				if not (abs(s[0] - hq[1][0]) > 1 or abs(s[1] - hq[1][1]) > 1):
					good = False
			if type(PathGenerator2().pathfinding(list(AICommands().getClosestOfObjects(s,self.workers))[1],s,position,15,True)) is not list: #ATTENTION! Really heavy operation
				good = False
			if good:
				good_sqs.append(list(s))
		if good_sqs:
			use_sqs = good_sqs
		else:
			use_sqs = self.empty_sqs
		min_distance = 1000
		for empty_sq in use_sqs:
			if abs(empty_sq[0]-ideal_crds[0])+abs(empty_sq[1]-ideal_crds[1]) < min_distance:
				min_distance = int(abs(empty_sq[0]-ideal_crds[0])+abs(empty_sq[1]-ideal_crds[1]))
				crds = list(empty_sq)

		return crds

	def selectTarget(self,units,side,style,position):
		nearest_prior_obj = []
		economy = 1*-1*side
		military = [2*-1*side,3*-1*side,4*-1*side,5*-1*side,6*-1*side]
		infrastructure = [7*-1*side,12*-1*side,13*-1*side]
		i=3
		while not nearest_prior_obj:
			if choice(style) == 1:
				nearest_prior_obj = AICommands().getClosestObject(AICommands().calcAvgCrds(units),economy,position,i)
				if not nearest_prior_obj:
					nearest_prior_obj = AICommands().getClosestObject(AICommands().calcAvgCrds(units),military,position,i)
					if not nearest_prior_obj:
						nearest_prior_obj = AICommands().getClosestObject(AICommands().calcAvgCrds(units),infrastructure,position,i)
			else:
				nearest_prior_obj = AICommands().getClosestObject(AICommands().calcAvgCrds(units),military,position,i)
				if not nearest_prior_obj:
					nearest_prior_obj = AICommands().getClosestObject(AICommands().calcAvgCrds(units),economy,position,i)
					if not nearest_prior_obj:
						nearest_prior_obj = AICommands().getClosestObject(AICommands().calcAvgCrds(units),infrastructure,position,i)
			i+=3

		return nearest_prior_obj[1]

	def selectAction(self,position,turn,side,p1_resources,p2_resources,training_queue):

		#training_queue = [order1,...,ordern], order = [unit (int),amount (int),crds of the producing blding ([0,0]),training process (float)]
		if side == 1:
			resources = p1_resources
			enemy_resources = p2_resources
		elif side == -1:
			resources = p2_resources
			enemy_resources = p1_resources
		action = 1
		selected_objects = []
		produce_unit = [0,[0,0],0] #[object, from coordinates of the structure, the defined amount]
		build_info = [0,[0,0],[0,0]] #[bld_id, crds of bld, crds of workers] ATTENTION! crds of workers is a list of the coordinates of all builders!
		destination = [0,0]






		pref_actions = [[0,1,[]]]
		#[evaluation,action,related data]
		#1 - skip turn
		#-2 - movement
		#	1 - basic movement
		#	2 - gather resources
		#	3 - construct a building
		#	4 - repair a building
		#	5 - attack on enemy unit
		#	6 - chop a tree
		#3 - train units
		#4 - build a structure

		#self.goldminers = []
		builders = []
		repairers = []
		idle_workers = []
		idle_warriors = []
		enemy_goldminers = []
		enemy_builders = []
		enemy_repairers = []
		enemy_idle_workers = []



		j=0
		for line in position:
			i=0
			for node in line:
				if node != 0:
					if node[0] == 1*side:
						self.workers.append(list(node))
						if node[2] == 0:
							idle_workers.append(list(node))
						elif node[2][0] == 2:
							self.goldminers.append(list(node))
						elif node[2][0] == 3:
							builders.append(list(node))
						elif node[2][0] == 4:
							repairers.append(list(node))
					elif node[0] == 1*-1*side:
						self.enemy_workers.append(list(node))
						if node[2] == 0:
							enemy_idle_workers.append(list(node))
						elif node[2][0] == 2:
							enemy_goldminers.append(list(node))
						elif node[2][0] == 3:
							enemy_builders.append(list(node))
						elif node[2][0] == 4:
							enemy_repairers.append(list(node))

					elif node[0] == 2*side:
						self.swordmen.append(list(node))
						if node[2] == 0:
							idle_warriors.append(list(node))
					elif node[0] == 2*-1*side:
						self.enemy_swordmen.append(list(node))

					elif node[0] == 3*side:
						self.eliteswordmen.append(list(node))
						if node[2] == 0:
							idle_warriors.append(list(node))
					elif node[0] == 3*-1*side:
						self.enemy_eliteswordmen.append(list(node))

					elif node[0] == 4*side:
						self.archers.append(list(node))
						if node[2] == 0:
							idle_warriors.append(list(node))
					elif node[0] == 4*-1*side:
						self.enemy_archers.append(list(node))

					elif node[0] == 5*side:
						self.knights.append(list(node))
						if node[2] == 0:
							idle_warriors.append(list(node))
					elif node[0] == 5*-1*side:
						self.enemy_knights.append(list(node))

					elif node[0] == 6*side:
						self.catapults.append(list(node))
						if node[2] == 0:
							idle_warriors.append(list(node))
					elif node[0] == 6*-1*side:
						self.enemy_catapults.append(list(node))

					elif node[0] == 7*side:
						self.hqs.append(list(node))
					elif node[0] == 7*-1*side:
						self.enemy_hqs.append(list(node))

					elif node[0] == 8*side:
						self.resourcecamps.append(list(node))
					elif node[0] == 8*-1*side:
						self.enemy_resourcecamps.append(list(node))

					elif node[0] == 9*side:
						self.barracks.append(list(node))
					elif node[0] == 9*-1*side:
						self.enemy_barracks.append(list(node))

					elif node[0] == 10*side:
						self.archeryranges.append(list(node))
					elif node[0] == 10*-1*side:
						self.enemy_archeryranges.append(list(node))

					elif node[0] == 11*side:
						self.stables.append(list(node))
					elif node[0] == 11*-1*side:
						self.enemy_stables.append(list(node))

					elif node[0] == 17*side:
						self.siegeworkshops.append(list(node))
					elif node[0] == 17*-1*side:
						self.enemy_siegeworkshops.append(list(node))

					elif node[0] == 12*side:
						self.watchtowers.append(list(node))
					elif node[0] == 12*-1*side:
						self.enemy_watchtowers.append(list(node))

					elif node[0] == 13*side:
						self.castles.append(list(node))
					elif node[0] == 13*-1*side:
						self.enemy_castles.append(list(node))

					elif node[0] in [14,15]:
						self.all_resourceplants.append(list(node))

					elif node[0] == 16:
						self.blocks.append(list(node))
				else:
					self.empty_sqs.append([int(i),int(j)])
				self.squares += 1
				i+=1
			j+=1

		mother_base = []
		for hq in self.hqs:
			j=0
			for line in position:
				i=0
				for node in line:
					if abs(i-hq[1][0]) + abs(j-hq[1][1]) <= 6:
						mother_base.append([int(i),int(j)])
					i+=1
				j+=1


		worker_production = False
		swordman_production = False
		archer_production = False
		knight_production = False
		catapult_production = False

		for order in training_queue:
			if order[0] == 1*side and (order[1] > 1 or order[3]+Features().workerTrainingSpeed()<1.0):
				worker_production = True
			elif (order[0] == 2*side and (order[1] > 1 or order[3]+Features().swordmanTrainingSpeed()<1.0)) or (order[0] == 3*side and (order[1] > 1 or order[3]+Features().eliteswordmanTrainingSpeed()<1.0)):
				swordman_production = True
			elif order[0] == 4*side and (order[1] > 1 or order[3]+Features().archerTrainingSpeed()<1.0):
				archer_production = True
			elif order[0] == 5*side and (order[1] > 1 or order[3]+Features().knightTrainingSpeed()<1.0):
				knight_production = True
			elif order[0] == 6*side and (order[1] > 1 or order[3]+Features().catapultTrainingSpeed()<1.0):
				catapult_production = True





		if not worker_production and resources >= Features().workerPrice() and len(self.hqs) > 0:
			for hq in self.hqs:
				possibility, prod_instructs = AICommands().launchUnitProductionAI(1,1,hq,turn) #villager,amount=1,from random HQ
				if possibility:
					pref_actions.append([0,3,list(prod_instructs)]) #evaluation=0,train units,production instructions list (if chosen, becomes the produce_unit list)
		castle_built = False
		for castle in self.castles:
			if castle[9] != -1:
				castle_built = True
		if not swordman_production and resources >= Features().eliteswordmanPrice() and len(self.barracks) > 0 and castle_built:
			for one_barracks in self.barracks:
				possibility, prod_instructs = AICommands().launchUnitProductionAI(3,1,one_barracks,turn)
				if possibility:
					pref_actions.append([0,3,list(prod_instructs)])
		elif not swordman_production and resources >= Features().swordmanPrice() and len(self.barracks) > 0:
			for one_barracks in self.barracks:
				possibility, prod_instructs = AICommands().launchUnitProductionAI(2,1,one_barracks,turn)
				if possibility:
					pref_actions.append([0,3,list(prod_instructs)])
		if not archer_production and resources >= Features().archerPrice() and len(self.archeryranges) > 0:
			for archeryrange in self.archeryranges:
				possibility, prod_instructs = AICommands().launchUnitProductionAI(4,1,archeryrange,turn)
				if possibility:
					pref_actions.append([0,3,list(prod_instructs)])
		if not knight_production and resources >= Features().knightPrice() and len(self.stables) > 0:
			for stable in self.stables:
				possibility, prod_instructs = AICommands().launchUnitProductionAI(5,1,stable,turn)
				if possibility:
					pref_actions.append([0,3,list(prod_instructs)])
		if not catapult_production and resources >= Features().catapultPrice() and len(self.siegeworkshops) > 0:
			for siegeworkshop in self.siegeworkshops:
				possibility, prod_instructs = AICommands().launchUnitProductionAI(6,1,siegeworkshop,turn)
				if possibility:
					pref_actions.append([0,3,list(prod_instructs)])
		if resources >= Features().HQPrice() and self.workers:
			pref_actions.append([0,4,7])
		if resources >= Features().resourcecampPrice() and self.workers:
			pref_actions.append([0,4,8])
		if resources >= Features().barracksPrice() and self.workers:
			pref_actions.append([0,4,9])
		if resources >= Features().archeryrangePrice() and self.workers:
			pref_actions.append([0,4,10])
		if resources >= Features().stablePrice() and self.workers:
			pref_actions.append([0,4,11])
		if resources >= Features().siegeworkshopPrice() and self.workers:
			pref_actions.append([0,4,17])
		if resources >= Features().watchtowerPrice() and self.workers:
			pref_actions.append([0,4,12])
		if resources >= Features().castlePrice() and self.workers:
			pref_actions.append([0,4,13])
		if idle_workers and self.all_resourceplants:
			resourceplants = []
			for r in self.all_resourceplants:
				n=0
				for g in self.goldminers:
					if abs(r[1][0] - g[1][0]) <= 1 or abs(r[1][1] - g[1][1]) <= 1:
						n+=1
				if n < 3:
					resourceplants.append(list(r))
			if resourceplants:
				use_resourceplants = resourceplants
			else:
				use_resourceplants = self.all_resourceplants
			nearest_r_plant = AICommands().getClosestOfObjects(AICommands().calcAvgCrds(idle_workers),use_resourceplants)
			pref_actions.append([0,-2,idle_workers,nearest_r_plant[1],str(AIFlags().task(1))]) #evaluation=0,move units,list of units,destination (if pref_action is chosen)
		if idle_warriors:
			pref_actions.append([0,-2,idle_warriors,[],str(AIFlags().task(2))])
		for builder in enemy_builders:
			if builder[1] in mother_base:
				workers = list(self.workers)
				worker1 = list(AICommands().getClosestOfObjects(builder[1],workers))
				if worker1:
					workers.remove(worker1)
				worker2 = list(AICommands().getClosestOfObjects(builder[1],workers))
				if worker1 and worker2:
					pref_actions.append([0,-2,[worker1,worker2],list(builder[1]),str(AIFlags().task(3))])






		shuffle(pref_actions)
		pref_actions_temp = list(pref_actions)

		obst_ratio = (1.0*(len(self.all_resourceplants)+len(self.blocks)))/(1.0*self.squares)

		for goldminer in self.goldminers:
			dest_crds = AICommands().getClosestObject(goldminer[1],[7*side,8*side],position)[1]
			dist = abs(goldminer[1][0]-dest_crds[0])+abs(goldminer[1][1]-dest_crds[1])
			if dist > 3:
				self.far_goldminers.append(list(goldminer))

		mil_blds = [self.barracks+self.archeryranges+self.stables+self.siegeworkshops][0]
		enemy_mil_blds = [self.enemy_barracks+self.enemy_archeryranges+self.enemy_stables+self.enemy_siegeworkshops][0]
		mil_units = [self.swordmen,self.eliteswordmen,self.archers,self.knights,self.catapults]

		mil_units_amount = 0
		for unit_type in mil_units:
			for unit in unit_type:
				mil_units_amount += 1

		hq_low_hp = False
		if len(self.hqs) == 1:
			if (self.hqs[0][4][0]*1.0)/(self.hqs[0][4][1]*1.0) < 0.4:
				hq_low_hp = True

		rush = int(choice([2])) #How early on military buildings are constructed, 0 = relaxed, 10 = asap
		aggro = [-1] #Possible aggressiveness levels (-10...10)
		style = [0,1] #Possible play style indexes 0 = straightforward, 1 = harass, 2 = side bases
		conserve = 0
		pending_constr_plans = False

		i=0
		for p_action in pref_actions:

			if p_action[1] == -2:
				if p_action[4] == str(AIFlags().task(1)):
					pref_actions_temp[i][0] += 3
					pref_actions_temp[i][0] += len(idle_workers)
				elif p_action[4] == str(AIFlags().task(2)):
					pref_actions_temp[i][0] += 5
					pref_actions_temp[i][0] += len(idle_warriors)
				elif p_action[4] == str(AIFlags().task(3)):
					pref_actions_temp[i][0] += (300-290*len(AICommands().getChasers(p_action[3],position)))

			if p_action[1] == 3:
				pref_actions_temp[i][0] += 140 #Raise the evaluation of a good action
				if p_action[2][0] == 1:
					pref_actions_temp[i][0] -= 17*len(self.workers)+40

				if p_action[2][0] == 2:
					pref_actions_temp[i][0] += len(self.enemy_swordmen)+4*len(self.enemy_knights)

				if p_action[2][0] == 3:
					pref_actions_temp[i][0] += 2*len(self.enemy_swordmen)+len(self.enemy_eliteswordmen)+5*len(self.enemy_knights)+2*len(self.enemy_catapults)
					pref_actions_temp[i][0] += 3

				if p_action[2][0] == 4:
					pref_actions_temp[i][0] += 5*len(self.enemy_swordmen)+3*len(self.enemy_eliteswordmen)+len(self.enemy_archers)
					pref_actions_temp[i][0] += 1

				if p_action[2][0] == 5:
					pref_actions_temp[i][0] += 5*len(self.enemy_archers)+len(self.enemy_knights)+4*len(self.enemy_catapults)

				if p_action[2][0] == 6:
					pref_actions_temp[i][0] += mil_units_amount
					pref_actions_temp[i][0] += 2*len(self.enemy_swordmen)+2*len(self.enemy_archers)+6*len(self.enemy_catapults)+4*len(self.enemy_watchtowers)+4*len(self.enemy_castles)



			if p_action[1] == 4:
				pref_actions_temp[i][0] -= 1
				pref_actions_temp[i][0] += int(resources/50)
				if len(self.workers) > 0:
					pref_actions_temp[i][0] -= int(len(mil_blds)/(len(self.workers)/5.0))*4

				if len(enemy_mil_blds) == 1 and len(mil_blds) == 0 and p_action[2] in [9,10,11,12,17]:
					pref_actions_temp[i][0] += 200

				if len(enemy_mil_blds) == 0 and len(mil_blds) == 0 and p_action[2] in [9,10,11,12,13,17]:
					pref_actions_temp[i][0] += (rush - 5)

				if len(mil_blds) < mil_units_amount and p_action[2] in [9,10,11,12,17]:
					pref_actions_temp[i][0] -= ((mil_units_amount - len(mil_blds))*30)

				if p_action[2] == 7:
					pref_actions_temp[i][0] -= 4
					if hq_low_hp:
						pref_actions_temp[i][0] += 400

				if p_action[2] == 8:
					pref_actions_temp[i][0] -= 3
					if len(self.workers) > 5 and len(self.resourcecamps) == 0 and len(self.hqs) == 1:
						pref_actions_temp[i][0] += 1
					pref_actions_temp[i][0] -= len(self.resourcecamps)
					if len(self.resourcecamps) > 5:
						pref_actions_temp[i][0] -= 200
					pref_actions_temp[i][0] += (len(self.far_goldminers)**2)

				if p_action[2] == 9:
					pref_actions_temp[i][0] += 1
					if len(self.enemy_stables) > len(self.barracks):
						pref_actions_temp[i][0] += 5
					if len(self.castles) > 0:
						pref_actions_temp[i][0] += 5
					if obst_ratio < 0.1:
						pref_actions_temp[i][0] += 1

				if p_action[2] == 10:
					pref_actions_temp[i][0] += 1
					if len(self.enemy_barracks) > len(self.archeryranges):
						pref_actions[i][0] += 4
					if obst_ratio > 0.2:
						pref_actions_temp[i][0] += 1

				if p_action[2] == 11:
					pref_actions_temp[i][0] += 1
					if len(self.enemy_archeryranges) > len(self.stables):
						pref_actions[i][0] += 3
					if len(self.enemy_siegeworkshops) > len(self.stables)*2:
						pref_actions_temp[i][0] += 3
					if obst_ratio < 0.1:
						pref_actions_temp[i][0] += 1

				if p_action[2] == 17:
					pref_actions_temp[i][0] += 2
					if len(self.enemy_siegeworkshops) > len(self.siegeworkshops):
						pref_actions[i][0] += 6
					pref_actions[i][0] += (len(self.enemy_castles)*10+len(self.enemy_watchtowers))*5
					if obst_ratio > 0.2:
						pref_actions_temp[i][0] += 1

				if p_action[2] == 13:
					if len(self.castles) < 1 and resources >= Features().castlePrice()+50:
						pref_actions_temp[i][0] += 5


			i+=1

		pref_actions = list(pref_actions_temp)






		max_eval = -1
		chosen_action = [0,0,[]]
		for p_action in pref_actions:
			if p_action[0] > max_eval:
				chosen_action = list(p_action)
				max_eval = int(p_action[0])

		action = chosen_action[1]
		if action == -2:
			selected_objects = chosen_action[2]
			if selected_objects == idle_warriors:
				destination = self.selectTarget(idle_warriors,side,style,position)
			else:
				destination = chosen_action[3]
		elif action == 3:
			produce_unit = chosen_action[2]
			playSound("button1")
		elif action == 4:
			bld_id = chosen_action[2]
			bld_crds = self.selectBldCrds(bld_id,aggro,style,position)
			if bld_crds:
				workers = []
				for worker in self.workers:
					if (not list(worker) in builders) and (not list(worker) in repairers):
						workers.append(worker)
				worker = AICommands().getClosestOfObjects(bld_crds,workers)
				if not worker:
					worker = AICommands().getClosestOfObjects(bld_crds,self.workers)
				worker_crds = worker[1]
				build_info = [bld_id,bld_crds,[worker_crds]]
			else:
				action = 0
				print "Warning! Tried to build, but couldn't find a place."



		return action,selected_objects,produce_unit,build_info,destination
